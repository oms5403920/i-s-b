<?php

use App\Http\Controllers\API\CompanyParam\GetCompanyParamsController;
use App\Http\Controllers\API\CompanyParam\UpdateCompanyParamValueController;
use Illuminate\Support\Facades\Route;

Route::group(
    [
        'prefix' => '/params',
        'as'     => 'params.',
    ],
    function () {
        Route::get('/company', GetCompanyParamsController::class)
        ->name('params-company')->middleware('can:view_parameters');
        Route::put('/company/update/value', UpdateCompanyParamValueController::class)
        ->name('update-value')->middleware('can:edit_parameters');
    }
);
