<?php

use App\Core\Models\Measure\Measure;
use App\Http\Controllers\API\Measure\Canva\ExportProductionCanvaController;
use App\Http\Controllers\API\Measure\ConsumedEnergyController;
use App\Http\Controllers\API\Measure\Dashboard\ConsummationDashboardChartController;
use App\Http\Controllers\API\Measure\Dashboard\EnergyDashboardTendenceChartController;
use App\Http\Controllers\API\Measure\Details\ConsummationDetailsChartController;
use App\Http\Controllers\API\Measure\Details\DetailsEnergyStatisticsController;
use App\Http\Controllers\API\Measure\Details\EnergyTendenceChartController;
use App\Http\Controllers\API\Measure\ImportController;
use App\Http\Controllers\API\Measure\MeasurementsEnergyData;
use App\Http\Controllers\API\Measure\MeasurementsEnergyDataInsertion;
use App\Http\Controllers\API\Measure\Monitoring\MonitoringStatisticsController;
use App\Http\Controllers\TestController;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

Route::group(
    [
        'prefix' => '/measurements',
        'as'     => 'measurements.',
    ],
    function () {
        Route::get('/global/statistics', ConsumedEnergyController::class)
        ->name('consumed-energy')->middleware('can:view_global_statistics');
        Route::post('/energy/data', MeasurementsEnergyData::class)->name('measurements-data');
        Route::get('/consumation/details/chart/{id}', ConsummationDetailsChartController::class)
        ->name('consummation-chart')->middleware('can:view_details');
        Route::get('/tendances/chart/{ids}/{feature}', EnergyTendenceChartController::class)
        ->name('tendence-chart')->middleware('can:view_details');
        Route::get('/consumation/dashboard/chart/{id?}', ConsummationDashboardChartController::class)
        ->name('consummation-chart-dashboard')->middleware('can:view_dashboard');
        Route::get('/details/statistics/{ids}', DetailsEnergyStatisticsController::class)
        ->name('details-statistics')->middleware('can:view_details_statistics');
        Route::get('dashboard/tendances/chart/{id?}', EnergyDashboardTendenceChartController::class)
        ->name('dashboard-tendances')->middleware('can:view_dashboard');
        Route::get('/monitoring/statistics/{id?}', MonitoringStatisticsController::class)
        ->name('monitoring-statistics')->middleware('can:view_monitoring');
        Route::post('/production/import', ImportController::class)
        ->name('production-import');
        Route::post('/energy/data/insert', MeasurementsEnergyDataInsertion::class)->name('measurements-data-insert');
        Route::get('/production/canva', ExportProductionCanvaController::class)->name('production-canva');
        Route::get('/test', function(){
            // try {
               
            //         list($hour, $minute) = explode(':', $calculHour);
            //         $startDate = Carbon::now();
            //         $endDate = Carbon::now()->hour($hour)->minute($minute);
            //         switch ($per) {
            //             case 'day':
            //                 $now = Carbon::now();
            //                 $startDateValue = $now->copy()->hour($hour)->minute($minute)->second(0);
            //                 if ($now->hour < $hour || ($now->hour == $hour && $now->minute < $minute)) {
            //                     $startDateValue->subDay();
            //                 }
            //                 $startDate = $startDateValue->toDateTimeString();
            //                 $endDate = $startDateValue->copy()->addDay()->toDateTimeString();
            //                 break;
                
            //             case 'week':
            //                 $startDate->subDays(7)->hour($hour)->minute($minute)->toDateTimeString();
            //                 break;
                
            //             case 'month':
            //                 $startDate->startOfMonth()->hour($hour)->minute($minute)->toDateTimeString();
            //                 $endDate->endOfMonth()->addDay(1)->hour($hour)->minute($minute)->toDateTimeString();
            //                 break;
                
            //             case 'year':
            //                 $startDate->startOfYear()->hour($hour)->minute($minute)->toDateTimeString();
            //                 $endDate->endOfYear()->addDay(1)->hour($hour)->minute($minute)->toDateTimeString();
            //                 break;
            //         }
            //         $minQueryOp = FeatureMeasure::select('feature_measurement.measure_id', 'feature_measurement.created_at', 'feature_measurement.value')
            //         ->join('measurements as m', 'feature_measurement.measure_id', '=', 'm.id')
            //         ->join('measurements as sub', function ($join) use ($entityId, $startDate, $endDate, $featureId) {
            //             $join->on('sub.entity_id', '=', 'm.entity_id')
            //                 ->whereNull('sub.point_id')
            //                 ->whereNull('sub.generic_asset_id')
            //                 ->whereBetween('sub.created_at', [$startDate, $endDate])
            //                 ->whereExists(function ($subquery) use ($featureId) {
            //                     $subquery->selectRaw(1)
            //                         ->from('feature_measurement as sub_fm')
            //                         ->whereColumn('sub_fm.measure_id', 'sub.id')
            //                         ->where('sub_fm.feature_id', $featureId);
            //                 });
            //         })
            //         ->where('m.entity_id', $entityId)
            //         ->whereNull('m.generic_asset_id')
            //         ->whereNull('m.point_id')
            //         ->whereBetween('m.created_at', [$startDate, $endDate])
            //         ->where('feature_id', $featureId)
            //         ->orderBy('value', 'asc')
            //         ->limit(1)
            //         ->first();
                    
            //         $maxQueryOp = FeatureMeasure::select('feature_measurement.measure_id', 'feature_measurement.created_at', 'feature_measurement.value')
            //         ->join('measurements as m', 'feature_measurement.measure_id', '=', 'm.id')
            //         ->join('measurements as sub', function ($join) use ($entityId, $startDate, $endDate, $featureId) {
            //             $join->on('sub.entity_id', '=', 'm.entity_id')
            //                 ->whereNull('sub.point_id')
            //                 ->whereNull('sub.generic_asset_id')
            //                 ->whereBetween('sub.created_at', [$startDate, $endDate])
            //                 ->whereExists(function ($subquery) use ($featureId) {
            //                     $subquery->selectRaw(1)
            //                         ->from('feature_measurement as sub_fm')
            //                         ->whereColumn('sub_fm.measure_id', 'sub.id')
            //                         ->where('sub_fm.feature_id', $featureId);
            //                 });
            //         })
            //         ->where('m.entity_id', $entityId)
            //         ->whereNull('m.generic_asset_id')
            //         ->whereNull('m.point_id')
            //         ->whereBetween('m.created_at', [$startDate, $endDate])
            //         ->where('feature_id', $featureId)
            //         ->orderByDesc('value')
            //         ->limit(1)
            //         ->first();
        
            //         return [$maxQueryOp, $maxQueryOp];
            // } catch (Exception $e) {
            //     Log::info('exception',['myexceptionTest'=>$e]);
            // }

        })->name('test');

    }
);
