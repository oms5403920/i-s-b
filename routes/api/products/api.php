<?php

use App\Http\Controllers\API\Products\DeleteProductController;
use App\Http\Controllers\API\Products\ListProductsController;
use App\Http\Controllers\API\Products\ShowProductController;
use App\Http\Controllers\API\Products\UpdateProductController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\Products\StoreProductController;

Route::group(
    [
        'prefix'     => 'products',
        'as'         => 'products.',
    ],
    function () {
        Route::get('/', ListProductsController::class)->name('index');
        Route::post('/', StoreProductController::class)->name('store');
        Route::get('/{id}', ShowProductController::class)->name('show');
        Route::delete('/{id}', DeleteProductController::class)->name('delete');
        Route::post('/{id}', UpdateProductController::class)->name('update');
    }
);
