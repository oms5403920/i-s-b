<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\Entity\GetUserEntitiesController;
use App\Http\Controllers\API\Entity\GetEntityGenericAssetsController;
use App\Http\Controllers\API\Entity\GetUserTreeController;

Route::group(
    [
        'prefix' => '/entities',
        'as'     => 'entities.',
    ],
    function () {
        Route::get('/tree', GetUserEntitiesController::class)->name('tree');
        Route::get('/children/genericassets/tree/{id}', GetEntityGenericAssetsController::class)->name('entityassets');
        Route::get('/tenantTree',GetUserTreeController::class)->name('tenantTree');
    }
);
