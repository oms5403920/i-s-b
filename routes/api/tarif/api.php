<?php

use App\Http\Controllers\API\Tarif\CreateTarifController;
use App\Http\Controllers\API\Tarif\DeleteTarifController;
use Illuminate\Support\Facades\Route;

Route::group(
    [
        'prefix' => '/tarif',
        'as'     => 'tarif.',
    ],
    function () {
        Route::post('/create', CreateTarifController::class)
        ->name('create-tarif');
        Route::delete('/delete/{id}', DeleteTarifController::class)
        ->name('delete-tarif');
    }
);
