<?php


use App\Http\Controllers\API\Point\CreatePointController;
use Illuminate\Support\Facades\Route;

Route::group(
    [
        'prefix' => '/point',
        'as'     => 'point.',
    ],
    function () {
        Route::post('/create', CreatePointController::class)
        ->name('point-creation');
    }
);
