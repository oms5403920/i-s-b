<?php

use App\Http\Controllers\API\GenericAsset\CreateGenericAssetController;
use App\Http\Controllers\API\GenericAsset\DataNeededForCreationController;
use App\Http\Controllers\API\GenericAsset\GetAllController;
use Illuminate\Support\Facades\Route;

Route::group(
    [
        'prefix' => '/genericAsset',
        'as' => 'genericAsset.',
    ],
    function () {
        Route::post('/create', CreateGenericAssetController::class)->name('generic-asset-creation')->middleware('can:create-generic-asset');
        Route::get('/getDataForCreation', DataNeededForCreationController::class)->name('generic-asset-data-creation');
        Route::get('/get/all', GetAllController::class)->name('all-generic-assets')->middleware('can:view-generic-asset');
    }
);
