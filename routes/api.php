<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Core\Services\Auth\API\AuthService;
use App\Http\Controllers\API\Measure\MediaController;
use App\Http\Controllers\API\Point\GetPointsByIdController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::name('api.')
    ->group(
        function () {
            Route::get('/media/download/{path}', [MediaController::class, '__invoke'])
            ->name('media-download');
            Route::get('/points/{id}', GetPointsByIdController::class)->name('get-points');
            require __DIR__ . '/api/auth/api.php';
            Route::group([
                'middleware' => [AuthService::AUTH_MIDDLEWARE_NAME],
            ], function () {
                require __DIR__ . '/api/company/api.php';
                require __DIR__ . '/api/entity/api.php';
                require __DIR__ . '/api/Measure/api.php';
                require __DIR__ . '/api/tarif/api.php';
                require __DIR__ . '/api/GenericAsset/api.php';
                require __DIR__ . '/api/Point/api.php';
                // require __DIR__ . '/api/products/api.php';
            });
        }
    );
