ARG PHP_EXTENSIONS="apcu bcmath pdo_mysql igbinary redis gd soap memcached msgpack"

FROM thecodingmachine/php:8.1-v4-slim-apache

ENV TEMPLATE_PHP_INI=production \
    APACHE_DOCUMENT_ROOT=/var/www/html/public \
    PHP_INI_SHORT_OPEN_TAG=1 \
    PHP_INI_ALLOW_URL_FOPEN=1 \
    PHP_INI_EXPOSE_PHP=1 \
    PHP_INI_DISPLAY_STARTUP_ERRORS=1 \
    PHP_INI_MEMORY_LIMIT=2g


COPY . /var/www/html

RUN composer install --ignore-platform-reqs --optimize-autoloader
