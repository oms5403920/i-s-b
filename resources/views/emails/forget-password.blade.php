<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Confirmation</title>

    <style>
        *{
            font-family:Arial,Helvetica,sans-serif;
        }
        html,
        body {
            padding: 0 !important;
            margin: 0 !important;
            font-family:Arial,Helvetica,sans-serif;
        }

        a:hover {
            color: #009ef7 !important;
        }
    </style>
</head>

<body style="background-color: rgb(213, 217, 226);">

    <div style="background-color: rgb(213, 217, 226); --kt-scrollbar-color: #d9d0cc; --kt-scrollbar-hover-color: #d9d0cc; height: 561px;">
        <div id="#kt_app_body_content"
            style="background-color:#D5D9E2; font-family:Arial,Helvetica,sans-serif; line-height: 1.5; min-height: 100%; font-weight: normal; font-size: 15px; color: #2F3044; margin:0; padding:0; width:100%;">
            <div
                style="background-color:#ffffff; padding: 45px 0 34px 0; border-radius: 24px; margin:40px auto; max-width: 600px;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" height="auto"
                    style="border-collapse:collapse">
                    <tbody>
                        <tr>
                            <td align="center" valign="center" style="text-align:center; padding-bottom: 10px">
                                <div style="text-align:center; margin:0 60px 34px 60px">
                                    <div style="margin-bottom:40px">
                                        <img alt="Logo" src="{{ public_path('assets/media/logos/login.png') }}" style="height: 70px">
                                    </div>
                                    <div style="font-size: 14px; font-weight: 500; margin-bottom: 30px; font-family:Arial,Helvetica,sans-serif;">
                                        <p style="margin-bottom:9px; color:#181C32; font-size: 22px; font-weight:700">
                                            Réinitialiser le mot de passe</p>
                                        <p style="margin-bottom:2px; color:#7E8299">
                                            vous recevez cet e-mail car nous avons reçu une demande de réinitialisation du mot de passe pour votre compte.
                                        </p>
                                    </div>
                                    <div style="text-align:center; font-size: 13px; font-weight: 500; margin-bottom: 27px; font-family:Arial,Helvetica,sans-serif;">
                                        <a href="https://v3dev.i-sense.io/auth/forgot-password/new-password?token={{ $token }}" target="_blank" style="background-color:#50cd89; border-radius:6px;display:inline-block; padding:11px 19px; color: #FFFFFF; font-size: 14px; font-weight:500; font-family:Arial,Helvetica,sans-serif;">
                                            Changer le mot de passe
                                        </a>                                        
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td align="center" valign="center"
                                style="font-size: 13px; padding:0 15px; text-align:center; font-weight: 500; color: #A1A5B7;font-family:Arial,Helvetica,sans-serif">
                                <p> I-sense
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</body>

</html>
