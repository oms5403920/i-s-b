<?php

namespace App\Energy\Managers\MeasureTarif;

use App\Core\Managers\Manager;
use App\Energy\Models\Params\Params;
use App\Energy\Repositories\MeasureTarif\MeasureTarifRepository;
use App\Energy\Repositories\Params\ParamsRepository;
use Carbon\Carbon;

class MeasureTarifManager extends Manager
{
    public function __construct(
        private MeasureTarifRepository $measureTarifRepository
    ) {
    }

    public function create(int $tarifId, int $measureId, int $genericAssetId, Carbon $date, float $tarif_energy): void
    {
        $this->measureTarifRepository->create($tarifId, $measureId, $genericAssetId, $date, $tarif_energy);
    }

    public function isTarifExist(int $tarifId, int $genericAssetId, bool $isGreater): bool
    {
        return $this->measureTarifRepository->isTarifExist($tarifId, $genericAssetId, $isGreater);
    }

    public function getTarifsCosts(array $genericAssetId, 
    ?string $startDate = null, ?string $endDate = null, ?string $per = "month") : ?array
    {
        return $this->measureTarifRepository->getTarifsCosts($genericAssetId, $startDate, $endDate, $per);
    }
}
