<?php

namespace App\Energy\Managers\CompanyParams;

use App\Core\Managers\Manager;
use App\Energy\Models\CompanyParams\CompanyParams;
use App\Energy\Repositories\CompanyParams\CompanyParamsRepository;
use Illuminate\Database\Eloquent\Collection;

class CompanyParamsManager extends Manager
{
    public function __construct(
        private CompanyParamsRepository $companyParamsRepository
    ) {
    }

    public function getByCompanyANDParam(int $companyId, int $paramId): ?CompanyParams
    {
        return $this->companyParamsRepository->getByCompanyANDParam($companyId, $paramId);
    }

    public function getCompanyParams(int $companyId) : ?Collection
    {
        return $this->companyParamsRepository->getCompanyParams($companyId);
    }

    public function getByParamName(int $id, string $paramName) : ?CompanyParams
    {
        return $this->companyParamsRepository->getByParamName($id, $paramName);
    }
}
