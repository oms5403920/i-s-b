<?php

namespace App\Energy\Managers\Params;

use App\Core\Managers\Manager;
use App\Energy\Models\Params\Params;
use App\Energy\Repositories\Params\ParamsRepository;

class ParamsManager extends Manager
{
    public function __construct(
        private ParamsRepository $paramsRepository
    ) {
    }

    public function getByName(string $name): ?Params
    {
        return $this->paramsRepository->getByName($name);
    }
}
