<?php

namespace App\Energy\Managers\Tarif;

use App\Core\Managers\Manager;
use App\Energy\Models\Tarif\Tarif;
use App\Energy\Repositories\Tarif\TarifRepository;

class TarifManager extends Manager
{
    public function __construct(
        private TarifRepository $tarifRepository
    ) {
    }

    public function create(array $attributes) : Tarif
    {
        return $this->tarifRepository->create($attributes);
    }

    public function update(int $id, array $attributes) : bool
    {
        return $this->tarifRepository->update($id, $attributes);
    }

    public function delete(int $id) : bool
    {
        return $this->tarifRepository->delete($id);
    }
}
