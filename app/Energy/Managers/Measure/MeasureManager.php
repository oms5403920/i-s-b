<?php

namespace App\Energy\Managers\Measure;

use App\Core\Managers\Manager;
use App\Core\Models\Measure\Measure;
use App\Energy\Repositories\Measure\MeasureRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class MeasureManager extends Manager
{
    public function __construct(
        private MeasureRepository $measureRepository
    ) {
    }

    public function isProductionMeasureExist(int $entityId, string $date) : ?Measure
    {
        return $this->measureRepository->isProductionMeasureExist($entityId, $date);
    }

    public function isProductionMeasureExistPer(int $entityId, ?string $per) : ?Measure
    {
        return $this->measureRepository->isProductionMeasureExistPer($entityId, $per);
    }
}
