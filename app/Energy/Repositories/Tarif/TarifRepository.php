<?php

namespace App\Energy\Repositories\Tarif;

use App\Core\Repositories\Repository;
use App\Energy\Models\Tarif\Tarif;
use Illuminate\Support\Arr;

class TarifRepository extends Repository
{
    public function create(array $attributes) : Tarif
    {
        return Tarif::query()->create([
            Tarif::IDENTIFIANT => Arr::get($attributes, Tarif::IDENTIFIANT),
            Tarif::TYPE => Arr::get($attributes, Tarif::TYPE),
            Tarif::HEURE_DEBUT => Arr::get($attributes, Tarif::HEURE_DEBUT),
            Tarif::HEURE_FIN => Arr::get($attributes, Tarif::HEURE_FIN),
            Tarif::COST => Arr::get($attributes, Tarif::COST),
            Tarif::TYPE_COST => Arr::get($attributes, Tarif::TYPE_COST),
            Tarif::COMPANY_ID => Arr::get($attributes, Tarif::COMPANY_ID)
        ]);
    }

    public function update(int $id, array $attributes) : bool
    {
        $attributes = Arr::only(
            $attributes,
            [
                Tarif::IDENTIFIANT,
                Tarif::TYPE,
                Tarif::HEURE_DEBUT,
                Tarif::HEURE_FIN,
                Tarif::COST,
                Tarif::TYPE_COST
            ]
            );
        return Tarif::query()->where(Tarif::ID_COLUMN, $id)
                ->update($attributes) > 0;
    }

    public function delete(int $id) : bool
    {
        return Tarif::query()
            ->where(Tarif::ID_COLUMN, $id)
            ->delete() === 1;
    }
}
