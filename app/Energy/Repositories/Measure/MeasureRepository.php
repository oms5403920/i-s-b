<?php

namespace App\Energy\Repositories\Measure;

use App\Core\Models\FeatureMeasure\FeatureMeasure;
use App\Core\Models\Features\Features;
use App\Core\Models\Measure\Measure;
use App\Core\Repositories\Repository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class MeasureRepository extends Repository
{
    public function isProductionMeasureExist(int $entityId, string $date) : ?Measure
    {   
        return Measure::join('feature_measurement', 'feature_measurement.measure_id', '=', 'measurements.id')
        ->join('features', 'features.id', '=', 'feature_measurement.feature_id')
        ->where('measurements.entity_id', $entityId)
        ->whereNull('measurements.deleted_at')
        ->whereDate('measurements.created_at', $date)
        ->where('features.name', Features::PRODUCTION)
        ->select('measurements.id')
        ->first();
    }

    public function isProductionMeasureExistPer(int $entityId, ?string $per) : ?Measure
    {   
        $WEEK = 'week';
        $MONTH = 'month';
        $YEAR = 'year';
        $DAY = 'day';
        if ($per) {
            switch ($per) {
                case $WEEK:
                    $startDate = Carbon::now()->startOfWeek()->toDateString();
                    $endDate = Carbon::now()->endOfWeek()->toDateString();
                    break;
                case $MONTH:
                    $startDate = Carbon::now()->startOfMonth()->toDateString();
                    $endDate = Carbon::now()->endOfMonth()->toDateString();
                    break;
                case $YEAR:
                    $startDate = Carbon::now()->startOfYear()->toDateString();
                    $endDate = Carbon::now()->endOfYear()->toDateString();
                    break;
                case $DAY:
                    $startDate = Carbon::now()->startOfDay()->toDateTimeString();
                    $endDate = Carbon::now()->endOfDay()->toDateTimeString();
                    break;
            }
        }
        return Measure::join('feature_measurement', 'feature_measurement.measure_id', '=', 'measurements.id')
        ->join('features', 'features.id', '=', 'feature_measurement.feature_id')
        ->where('measurements.entity_id', $entityId)
        ->whereNull('measurements.deleted_at')
        ->whereBetween('measurements.created_at', [$startDate, $endDate])
        ->where('features.name', Features::PRODUCTION)
        ->select('measurements.id')
        ->first();
    }
}
