<?php

namespace App\Energy\Repositories\CompanyParams;

use App\Core\Repositories\Repository;
use App\Energy\Models\CompanyParams\CompanyParams;
use COM;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

class CompanyParamsRepository extends Repository
{
    public function getByCompanyANDParam(int $companyId, int $paramId): ?CompanyParams
    {
        return CompanyParams::where(CompanyParams::COMPANY_ID, $companyId)
            ->where(CompanyParams::PARAM_ID, $paramId)
            ->first();
    }

    public function getCompanyParams(int $companyId) : ?Collection
    {
        return CompanyParams::where('company_id', $companyId)
        ->with('param')
        ->get();
    }

    public function getByParamName(int $companyId, string $paramName) : ?CompanyParams
    {
        return CompanyParams::where('company_id', $companyId)
        ->whereHas('param', function ($query) use ($paramName) {
            $query->where('name', $paramName);
        })
        ->first();
    }
    
    
}