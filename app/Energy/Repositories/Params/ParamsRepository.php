<?php

namespace App\Energy\Repositories\Params;

use App\Core\Repositories\Repository;
use App\Energy\Models\Params\Params;

class ParamsRepository extends Repository
{
    public function getByName(string $name): ?Params
    {
        return Params::where(Params::NAME, $name)->first();
    }
}
