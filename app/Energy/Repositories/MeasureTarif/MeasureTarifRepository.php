<?php

namespace App\Energy\Repositories\MeasureTarif;

use App\Core\Repositories\Repository;
use App\Energy\Models\MeasureTarif\MeasureTarif;
use App\Energy\Models\Tarif\Tarif;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MeasureTarifRepository extends Repository
{
    public function create(int $tarifId, int $measureId, int $genericAssetId, Carbon $date, float $tarif_energy): void
    {
        MeasureTarif::create(
            [
                "tarif_id" => $tarifId,
                "measure_id" => $measureId,
                'generic_asset_id' => $genericAssetId,
                'created_at' => $date,
                'updated_at' => $date,
                'date' => $date,
                'tarif_energy' => $tarif_energy
            ]
            );
    }

    public function isTarifExist(int $tarifId, int $genericAssetId, bool $isGreater): bool
    {
        if($isGreater)
        {
            $today = now()->toDateString();
        }
        else{
            $today = now()->subDay()->toDateString();
        }
        return MeasureTarif::where(MeasureTarif::TARIF_ID, $tarifId)
            ->where(MeasureTarif::GENERIC_ASSET_ID, $genericAssetId)
            ->whereDate('created_at', $today)
            ->exists();
    }

    public function getTarifsCosts(array $genericAssetId,
    ?string $startDate = null, ?string $endDate = null, ?string $per = "month") : ?array
    {
        $WEEK = 'week';
        $MONTH = 'month';
        $DAY = 'day';
        if (!$startDate || !$endDate) {
            if ($per) {
                switch ($per) {
                    case $WEEK:
                        $startDate = Carbon::now()->subWeeks(10)->startOfWeek()->toDateString();
                        $endDate = Carbon::now()->endOfWeek()->toDateString();
                        break;
                    case $MONTH:
                        $startDate = Carbon::now()->subMonths(12)->startOfMonth()->toDateString();
                        $endDate = Carbon::now()->endOfMonth()->toDateString();
                        break;
                    case $DAY:
                        $startDate = Carbon::now()->subDays(7)->toDateString();
                        $endDate = Carbon::now()->toDateString();
                        break;
                }
            }
        }
    
        $results = DB::table('generic_assets AS ga')
                ->select(DB::raw("CASE 
                WHEN '$per' = 'day' THEN DATE(T.date)
                WHEN '$per' = 'week' THEN CONCAT(YEAR(T.date), '/', WEEK(T.date))
                WHEN '$per' = 'month' THEN CONCAT(YEAR(T.date), '-',MONTH(T.date))
                ELSE NULL 
                END AS period"), 'T.type', 'T.identifiant', DB::raw('SUM(T.cost) as total_cost'), 'T.energy')
                ->from(function ($query) use ($genericAssetId, $startDate, $endDate) {
                    $query->select(
                            'ga.id AS asset_id', 
                            'fm.value AS cost', 
                            'tm.date AS date', 
                            't.type_cost AS type', 
                            't.identifiant AS identifiant',
                            'tm.tarif_energy as energy'
                        )
                        ->from('generic_assets AS ga')
                        ->leftJoin('measurements AS m', 'm.generic_asset_id', 'ga.id')
                        ->leftJoin('feature_measurement AS fm', 'fm.measure_id', 'm.id')
                        ->leftJoin('tarif_measures AS tm', 'tm.measure_id', 'm.id')
                        ->leftJoin('tarifs AS t', 't.id', 'tm.tarif_id')
                        ->whereIn('ga.id', $genericAssetId)
                        ->where('fm.feature_id', 50)
                        ->whereBetween('tm.date', [$startDate, $endDate]);
                }, 'T')
                ->groupBy('period', 'T.type', 'T.identifiant', 'T.energy')
                ->get();
        return $results->toArray();
    }
}
