<?php

namespace App\Energy\Models\MeasureTarif;

use App\Core\Models\AbstractModel;

class MeasureTarif extends AbstractModel
{
    public const TABLE = 'tarif_measures';

    protected $table = self::TABLE;

    public const TARIF_ID = 'tarif_id';
    public const MEASURE_ID = 'measure_id';
    public const GENERIC_ASSET_ID = 'generic_asset_id';
    public const DATE = 'date';
    public const TARIF_ENERGY = 'tarif_energy';

    protected $guarded = [];

    public function getTarifId(): ?string
    {
        return $this->getAttribute(self::TARIF_ID);
    }

    public function getMeasureId(): string
    {
        return $this->getAttribute(self::MEASURE_ID);
    }

    public function getGenericAssetId(): string
    {
        return $this->getAttribute(self::GENERIC_ASSET_ID);
    }
    
    public function getDate()
    {
        return $this->getAttribute(self::DATE);
    }
    
    public function getTarifEnergy() : float
    {
        return $this->getAttribute(self::TARIF_ENERGY);
    }
}
