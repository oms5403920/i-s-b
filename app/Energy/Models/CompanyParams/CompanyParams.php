<?php

namespace App\Energy\Models\CompanyParams;

use App\Core\Models\AbstractModel;
use App\Core\Models\Company\Company;
use App\Energy\Models\Params\Params;

class CompanyParams extends AbstractModel
{
    public const TABLE = 'company_params';

    protected $table = self::TABLE;

    public const PARAM_ID = 'param_id';
    public const COMPANY_ID = 'company_id';
    public const VALUE = 'value';

    protected $guarded = [];

    public function getParamID(): int
    {
        return $this->getAttribute(self::PARAM_ID);
    }

    public function getCompanyID(): int
    {
        return $this->getAttribute(self::COMPANY_ID);
    }

    public function getValue(): string
    {
        return $this->getAttribute(self::VALUE);
    }

    public function param()
    {
        return $this->belongsTo(Params::class, 'param_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
    
}
