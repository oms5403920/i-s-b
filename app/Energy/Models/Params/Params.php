<?php

namespace App\Energy\Models\Params;

use App\Core\Models\AbstractModel;

class Params extends AbstractModel
{
    public const TABLE = 'params';

    protected $table = self::TABLE;

    public const NAME = 'name';
    public const VIEW = 'view';

    protected $guarded = [];

    public function getName(): string
    {
        return $this->getAttribute(self::NAME);
    }

    public function getView(): string
    {
        return $this->getAttribute(self::VIEW);
    }

}
