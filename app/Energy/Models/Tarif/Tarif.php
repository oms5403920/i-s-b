<?php

namespace App\Energy\Models\Tarif;

use App\Core\Models\AbstractModel;

class Tarif extends AbstractModel
{
    public const TABLE = 'tarifs';

    protected $table = self::TABLE;

    public const IDENTIFIANT = 'identifiant';
    public const TYPE = 'type';
    public const HEURE_DEBUT = 'heure_debut';
    public const HEURE_FIN = 'heure_fin';
    public const COST = 'cost';
    public const TYPE_COST = 'type_cost';
    public const COMPANY_ID = 'company_id';

    public const VENTE = 'vente';
    public const ACHAT = 'achat';

    protected $guarded = [];

    public function getIdentifiant(): ?string
    {
        return $this->getAttribute(self::IDENTIFIANT);
    }

    public function getType(): string
    {
        return $this->getAttribute(self::TYPE);
    }

    public function getHeureDebut()
    {
        return $this->getAttribute(self::HEURE_DEBUT);
    }

    public function getHeureFin()
    {
        return $this->getAttribute(self::HEURE_FIN);
    }

    public function getCost() :?float
    {
        return $this->getAttribute(self::COST);
    }

    public function getTypeCost(): ?string
    {
        return $this->getAttribute(self::TYPE_COST);
    }

    public function getCompanyId() : ?int 
    {
        return $this->getAttribute(self::COMPANY_ID);
    }
}
