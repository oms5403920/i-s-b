<?php

namespace App\Energy\Services\Tarif;

use App\Core\Services\Service;
use App\Energy\Managers\Tarif\TarifManager;
use App\Energy\Models\Tarif\Tarif;

class TarifService extends Service
{
    public function __construct(
        private TarifManager $tarifManager
    ) {
        parent::__construct();
    }

    public function create(array $attributes) : Tarif
    {
        return $this->tarifManager->create($attributes);
    }

    public function update(int $id, array $attributes) : bool
    {
        return $this->tarifManager->update($id, $attributes);
    }

    public function delete(int $id)
    {
        return $this->tarifManager->delete($id);
    }
}
