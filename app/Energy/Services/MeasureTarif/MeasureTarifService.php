<?php

namespace App\Energy\Services\MeasureTarif;

use App\Core\Services\Service;
use App\Energy\Managers\MeasureTarif\MeasureTarifManager;
use Carbon\Carbon;

class MeasureTarifService extends Service
{
    public function __construct(
        private MeasureTarifManager $measureTarifManager
    ) {
        parent::__construct();
    }

    public function create(int $tarifId, int $measureId, int $genericAssetId, Carbon $date, float $tarif_energy): void
    {
        $this->measureTarifManager->create($tarifId, $measureId, $genericAssetId, $date, $tarif_energy);
    }

    public function isTarifExist(int $tarifId, int $genericAssetId, bool $isGreater): bool
    {
        return $this->measureTarifManager->isTarifExist($tarifId, $genericAssetId, $isGreater);
    }

    public function getTarifsCosts(array $genericAssetId, 
    ?string $startDate = null, ?string $endDate = null, ?string $per = "month") : ?array
    {
        return $this->measureTarifManager->getTarifsCosts($genericAssetId, $startDate, $endDate, $per);
    }
}
