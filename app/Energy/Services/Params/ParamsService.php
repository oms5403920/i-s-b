<?php

namespace App\Energy\Services\Params;

use App\Core\Services\Service;
use App\Energy\Managers\Params\ParamsManager;
use App\Energy\Models\Params\Params;

class ParamsService extends Service
{
    public function __construct(
        private ParamsManager $paramsManager
    ) {
        parent::__construct();
    }

    public function getByName(string $name): ?Params
    {
        return $this->paramsManager->getByName($name);
    }
}
