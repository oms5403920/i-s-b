<?php

namespace App\Energy\Services\CompanyParams;

use App\Core\Services\Service;
use App\Energy\Managers\CompanyParams\CompanyParamsManager;
use App\Energy\Models\CompanyParams\CompanyParams;

class CompanyParamsService extends Service
{
    public function __construct(
        private CompanyParamsManager $companyParamsManager
    ) {
        parent::__construct();
    }

    public function getByCompanyANDParam(int $companyId, int $paramId): ?CompanyParams
    {
        return $this->companyParamsManager->getByCompanyANDParam($companyId, $paramId);
    }

    public function getCompanyParams(int $companyId) : ?array
    {
        $companyParams = $this->companyParamsManager->getCompanyParams($companyId);
        if ($companyParams->count() > 0) {
            $paramsForCompany = [];
            foreach ($companyParams as $companyParam) {
                $paramName = $companyParam->param->name;
                $paramValue = $companyParam->value;
                $paramsForCompany[$paramName] = $paramValue;
            }
        }
            return $paramsForCompany;
    }

    public function getByParamName(int $id, string $paramName) : ?CompanyParams
    {
        return $this->companyParamsManager->getByParamName($id, $paramName);
    }
    
}
