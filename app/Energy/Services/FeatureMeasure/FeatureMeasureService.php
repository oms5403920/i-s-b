<?php

namespace App\Energy\Services\FeatureMeasure;

use App\Core\Managers\FeatureMeasure\FeatureMeasureManager;
use App\Core\Models\Entity\Entity;
use App\Core\Models\FeatureMeasure\FeatureMeasure;
use App\Core\Services\Entity\EntityService;
use App\Core\Services\ExtraField\ExtraFieldService;
use App\Core\Services\GenericAssetsExtraField\GenericAssetsExtraFieldService;
use App\Core\Services\Service;
use DateTime;
use Illuminate\Database\Eloquent\Collection;

class FeatureMeasureService extends Service
{
    public function __construct(
        private FeatureMeasureManager $featureMeasureManager,
        private EntityService $entityService,
        private GenericAssetsExtraFieldService $genericAssetsExtraFieldService,
        private ExtraFieldService $extraFieldService
    ) {
        parent::__construct();
    }

    public function getByEntityId(int $entityId, string $per, string $calculHour): ?FeatureMeasure
    {
        return $this->featureMeasureManager->getByEntityId($entityId, $per, $calculHour);
    }

    public function energyCalculation(array $assets): array {
        $consumedEnergy = 0.0;
        $energyProduced = 0.0;
        $production = 0.0;
        foreach ($assets as $asset) {
            foreach ($asset['consummationMeasures'] as $measureData) {
                if($measureData['firstConsummationMeasure'] && $measureData['lastConsummationMeasure'])
                {
                    $initialValue = $measureData['firstConsummationMeasure']['value'];
                    $lastValue = $measureData['lastConsummationMeasure']['value'];
                    if ($measureData['firstConsummationMeasure']['point_id'] == $measureData['lastConsummationMeasure']['point_id']) {
                        if ($measureData['firstConsummationMeasure']['type'] != 0 && $measureData['lastConsummationMeasure']['type'] != 0) {
                            $energyProduced += $lastValue - $initialValue;
                        } elseif ($measureData['firstConsummationMeasure']['type'] == 0 && $measureData['lastConsummationMeasure']['type'] == 0) {
                            $consumedEnergy += $lastValue - $initialValue;
                        }
                    }
                    // if($measureData['productionMeasure'])
                    // {
                    //     $production += $measureData['productionMeasure']['value'];
                    // }
                }
            }
        }
    
        return [
            'consumedEnergy' => $consumedEnergy,
            'energyProduced' => $energyProduced,
            // 'production' => $production
        ];
    }
    
    

    public function consummationDetailChart(array $firstMeasureids, array $lastMeasureIds, Entity $entity): array
    {
        $entityGenericAssets = $entity->genericAssets;
        $consumedEnergyByAsset = [];
        $entityConsummation = 0;
        $genericAssets = [];
    
        foreach ($firstMeasureids as $firstMeasure) {
            foreach ($lastMeasureIds as $lastMeasure) {
                if ($lastMeasure['point_id'] === $firstMeasure['point_id']) {
                    $initialValue = $firstMeasure['value'];
                    $lastValue = $lastMeasure['value'];
    
                    $assetName = $firstMeasure['name'];
                    if (!in_array($assetName, $genericAssets)) {
                        $genericAssets[] = $assetName;
                        $consumedEnergyByAsset['assets'][] = [
                            'name'  => $assetName,
                            'value' => (float) number_format(($consumedEnergyByAsset[$assetName]['value'] ?? 0) + ($lastValue - $initialValue), 2, '.', ''),
                        ];
                        $entityConsummation += ($lastValue - $initialValue);
                    }
                }
            }
        }

        foreach ($entityGenericAssets as $genericAsset) {
            $genericAssetName = $genericAsset->getName();
            if (!in_array($genericAssetName, $genericAssets)) {
                $consumedEnergyByAsset['assets'][] = [
                    'name'  => $genericAssetName,
                    'value' => 0,
                ];
            }
        }
    
        $consumedEnergyByAsset['entity'] = [
            'name'  => $entity->getName(),
            'value' => (float) number_format($entityConsummation, 2, '.', ''),
        ];
    
        return $consumedEnergyByAsset;
    }

    public function powerTrendancesChart(array $measures, array &$consumedEnergy, string $featureName, bool $isRunning)
    {
        $unit = null;
        $assetsData = [];
        $puissanceNominale = $this->extraFieldService->findByName('puissance_nominale');
        foreach ($measures as $measure) {
            if (isset($measure['id'])) {
                $assetName = $measure['name'];
    
                if (!isset($assetsData['assets'][$assetName])) {
                    $assetsData['assets'][$assetName] = [];
                }
    
                $measureEnergy = $measure['value'];
                $unit = $measure['unit'];
    
                $yValue = $isRunning && $measureEnergy === 0.0 ? -1 : $measureEnergy;
                if(!$isRunning)
                {  
                    if (!isset($assetsData['zValues'][$assetName])) {
                        $zValue = $this->genericAssetsExtraFieldService->getByGenericAssetIdAndFieldId($measure['assetId'], $puissanceNominale->getId());
                        if($zValue)
                        {
                            $z = $zValue->getvalue();
                            $assetsData['zValues'][$assetName] = $z;
                        }
                    } else {
                        $z = $assetsData['zValues'][$assetName];
                    }
                    $assetsData['assets'][$assetName][] = [
                        'y' => $yValue,
                        'x' => $measure['date'] ? (new DateTime($measure['date']))->format('Y-m-d H:i:s') : '00-00-00 00:00:00',
                        'z' => $z ?? 0,
                    ];
                }
                else{
                    $assetsData['assets'][$assetName][] = [
                        'y' => $yValue,
                        'x' => $measure['date'] ? (new DateTime($measure['date']))->format('Y-m-d H:i:s') : '00-00-00 00:00:00'
                    ];
                }
                
            }
        }
        unset($assetsData['zValues']);
        $consumedEnergy = $assetsData;
        $consumedEnergy['unit'] = $unit;
    }
    
    
    public function consummationDashBoardChart(array $assets, ?Entity $parentEntity = null): array
    {
        $consumedEnergyByEntity = [];
    
        foreach ($assets['assets'] as $asset) {
            $initialValue = $asset->first_measure_value ?? null;
            $lastValue = $asset->last_measure_value ?? null;
    
            $entity = $this->entityService->getTopLevelParent($asset->entity, $parentEntity?->getId())->getName();
    
            $consumedEnergy = &$consumedEnergyByEntity[$entity] ?? 0;
            $consumedEnergy += ($lastValue - $initialValue);
        }
    
        $response = [];
    
        foreach ($consumedEnergyByEntity as $entityName => $value) {
            $response['entities'][$entityName] = [
                'name' => $entityName,
                'value' => (float)number_format($value, 2, '.', ''),
            ];
        }
    
        $response['total'] = [
            'name' => $parentEntity ? $parentEntity->getName() : 'Total',
            'value' => (float)number_format(array_sum($consumedEnergyByEntity), 2, '.', ''),
        ];
    
        return $response;
    }
    
    
    

    public function DetailsStatisticsCalculation(array $firstMeasureids, array $lastMeasureIds): array
    {
        $puissanceValue = null;
        $energy = null;
        foreach ($firstMeasureids as $firstMeasure) {
            foreach ($lastMeasureIds as $lastMeasure) {
                if ($lastMeasure['point_id'] === $firstMeasure['point_id']) {
                    $initialValue = $firstMeasure['value'];
                    $lastValue = $lastMeasure['value'];
                        if ($lastMeasure['type'] === 0 && $firstMeasure['type'] === 0) {
                            $energy += $lastValue - $initialValue;
                        } elseif ($lastMeasure['type'] !== 0 && $firstMeasure['type'] !== 0) {
                            $energy += $lastValue + $initialValue;
                        }
                }
            }
        }

        $lastAssetsMeasueres = [];
        $dates = [];
        foreach ($lastMeasureIds as $element) {
            $name = $element['name'];
            $measure = [
                'name'       => $name,
                'measure_id' => $element['measure_id'],
                'date' => $element['date']
            ];
            $lastAssetsMeasueres[$name] = $measure;
        }
        foreach ($lastAssetsMeasueres as $measure) {
            $lastPuissance = $this->featureMeasureManager->getValueByFeature(
                $measure['measure_id'],
                FeatureMeasure::POWER
            );
            $dates[] = $measure['date'] ;
            $puissanceValue += $lastPuissance ? $lastPuissance->getValue() : 0;
        }
        if($dates) {
            $latestRecord = max($dates);
            $latestDate = $latestRecord->toDateTimeString();
        }
        else {
            $latestDate = "00-00-00 00:00:00";
        }
        return [
            'Energy'    => $energy,
            'Puissance' => $puissanceValue,
            'last_record' => $latestDate
        ];
    }

    public function energyTrendancesDashboardChart(array $measures, string $featureName) : array
    {
        $consumedEnergy = [];
        foreach ($measures as $measure) {
                $xValue = $measure['date'] ? (new DateTime($measure['date']))->format('Y-m-d') : '00-00-00';
                $consumedEnergy[] = [
                    'y' => (float)number_format($measure['value'], 2, '.', ''),
                    'x' => $xValue,
                ];
        }
        return $consumedEnergy;
    }

    
    public function energyTrendancesChart(array $firstMeasures, array $lastMeasures, string $feature)
    {
        $previousLastMeasure = null;
        $lDates = [];
        $processedWeeks = [];
        $unit = null;
        $totals = [];
        foreach ($firstMeasures as $firstMeasure) {
            foreach ($lastMeasures as $lastMeasure) {
                if (
                    $lastMeasure['point_id'] === $firstMeasure['point_id'] &&
                    $lastMeasure['name'] === $firstMeasure['name']
                ) {
                    $lastMeasureDate = new DateTime($lastMeasure['date']);
                    $firstMeasureDate = new DateTime($firstMeasure['date']);
                    $assetName = $firstMeasure['name'];
    
                    if (!isset($totals[$assetName])) {
                        $totals[$assetName] = [];
                    }
                    
                    // $initialMeasureEnergy = $this->featureMeasureManager->getValueByFeature(
                    //     $firstMeasure['measure_id'],
                    //     $feature
                    // );

                    $initialValue = $firstMeasure['energyValue'];
                    
                    // $lastMeasureEnergy = $this->featureMeasureManager->getValueByFeature(
                    //     $lastMeasure['measure_id'],
                    //     $feature
                    // );
                    $lastValue = $lastMeasure['energyValue'];
                    
                    if ($unit === null) {
                        $unit = $lastMeasure['energyUnit'];
                    } elseif ($unit !== $lastMeasure['energyUnit']) {
                        $unit = null;
                    }
                    $per = $firstMeasure['per'];
    
                    if ($per === 'day' && $lastMeasure['date']->day === $firstMeasure['date']->day) {
                        $energyDifference = $lastValue - $initialValue;
                        $totals[$assetName][] = [
                            'y' => (float)number_format($energyDifference, 2, '.', ''),
                            'x' => $lastMeasure['date']->format('Y-m-d'),
                        ];
                    } elseif ($per === 'month' && $lastMeasureDate->format('m') === $firstMeasureDate->format('m')) {
                        $energyDifference = $lastValue - $initialValue;
                        $totals[$assetName][] = [
                            'y' => (float)number_format($energyDifference, 2, '.', ''),
                            'x' => $lastMeasure['date']->format('Y-m'),
                        ];
                    } elseif ($per === 'week' && $lastMeasureDate->format('W') == $firstMeasureDate->format('W')) {
                        
                        
                        $initialWeekNumber = $firstMeasureDate->format('W');
                        $lastWeekNumber = $lastMeasureDate->format('W');
                        $weekKey = $assetName . '_' . $initialWeekNumber;
                        if (!in_array($weekKey, $processedWeeks)) {
                            $energyDifference = $lastValue - $initialValue;
                            $totals[$assetName][] = [
                                'y' => (float)number_format($energyDifference, 2, '.', ''),
                                'x' =>  $lastMeasureDate->format('Y') . '/' . $lastWeekNumber,
                            ];
                            $processedWeeks[] = $weekKey;
                        }
                    } elseif ($per === 'year' && $firstMeasureDate->format('Y') === $lastMeasureDate->format('Y')) {
                        $energyDifference = $lastValue - $initialValue;
                        $totals[$assetName][] = [
                            'y' => (float)number_format($energyDifference, 2, '.', ''),
                            'x' => $lastMeasure['date']->format('Y'),
                        ];
                    }
                }
            }
        }
        return ['assets' => $totals, 'unit' => $unit ];
    }
    
    public function getWorkingHour(array $genericAssetIds, int $featureId,
            int $entityId, int $genericAssetsCount, int $entityGenerticAssetsCount, string $calculHour, string $per) : ?float
    {
        $workingHourData = 0.0;
        if($genericAssetsCount == 1)
        {
            [$maxValue, $minValue] = $this->featureMeasureManager->getWorkingHourOfGnericAssets($genericAssetIds, $featureId, $calculHour, $per);
            if($maxValue && $minValue)
            {
                $workingHourData = $maxValue->getValue() - $minValue->getValue();
            }
        }
        else {
           [$maxValue, $minValue] = $this->featureMeasureManager->getWorkingHourOfEntity($entityId, $featureId, $calculHour, $per);
            if($maxValue && $minValue)
            {
                $workingHourData = $maxValue->getValue() - $minValue->getValue();
            }
        }
        if ($workingHourData !== null) {
            return (float)number_format($workingHourData, 2, '.', '');
        }
        return 0.0;
    }

    public function getProductionByEntityIds(array $entityIds, string $per, string $calculHour): ?Collection
    {
        $descendantIds = $this->entityService->getDescendantIds($entityIds);
        return $this->featureMeasureManager->getProductionByEntityIds($descendantIds, $per, $calculHour);
    }

    public function getTarifTendencesChart($tarifsData, $type){
        $tendances = [];
        foreach ($tarifsData as $result) {
            $identifiant = $result->identifiant;
            $date = $result->period;
            $totalCost = $result->total_cost; 
            $energy = $result->energy; 
            if($type == "cost")
            {
                $tendances['assets'][$identifiant][] = [
                    'y' => (float)number_format($totalCost, 2, '.', ''),
                    'x' => $date,
                ];
                $tendances['unit'] = "DH";
            }
            else if($type == "kwh") {
                $tendances['assets'][$identifiant][] = [
                    'y' => (float)number_format($energy, 2, '.', ''),
                    'x' => $date,
                ];
                $tendances['unit'] = "Kwh";
            }
            
        }
        return $tendances;
    }
}
