<?php

namespace App\Energy\Services\Measure;

use App\Core\Models\GenericAsset\GenericAsset;
use App\Core\Models\Measure\Measure;
use App\Core\Services\Service;
use App\Energy\Managers\Measure\MeasureManager;
use Carbon\Carbon;

class MeasureService extends Service
{
    public function __construct(
        private MeasureManager $measureManager,
    ) {
        parent::__construct();
    }

    public function isProductionMeasureExist(int $entityId, string $date) : ?Measure
    {
        return $this->measureManager->isProductionMeasureExist($entityId, $date);
    }

    public function isProductionMeasureExistPer(int $entityId, ?string $per) : ?Measure
    {
        return $this->measureManager->isProductionMeasureExistPer($entityId, $per);
    }
}
