<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ProductionCanvaExport implements FromCollection, WithHeadings, WithMapping, WithStyles
{
   
    protected $entities;

    public function __construct($entities)
    {
        $this->entities = $entities;
    }

    public function collection()
    {
        if ($this->entities === null) {
            return collect([]);
        }
        return $this->entities;
    }

    public function headings(): array
    {
        return ['Entity ID', 'Entity Name', 'Production', 'Date'];
    }

    public function map($entity): array
    {
        return [
            'id' => $entity->id,
            'name' => $entity->name,
            'production' => null,
            'date' => null, 
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1:D1')->applyFromArray([
            'font' => [
                'bold' => true,
                'color' => ['rgb' => 'FFFFFF'],
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'startColor' => ['rgb' => '0070C0'],
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['rgb' => '000000'],
                ],
            ],
        ]);
        $row = 2;
        foreach ($this->entities as $entity) {
            $sheet->getStyle('A' . $row . ':D' . $row)->applyFromArray([
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'startColor' => ['rgb' => $this->getAlternateColor($row)],
                ],
            ]);
            $row++;
        }
        $sheet->getColumnDimension('A')->setWidth(20);
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->getColumnDimension('C')->setWidth(20);
        $sheet->getStyle('D')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);
        $sheet->getColumnDimension('D')->setWidth(25);
        $sheet->getStyle('A2:D' . ($sheet->getHighestRow()))->applyFromArray([
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['rgb' => '000000'],
                ],
            ],
        ]);
    }

    private function getAlternateColor($row)
    {
        return ($row % 2 === 0) ? 'E0E0E0' : 'FFFFFF';
    }
}
