<?php

namespace App\Console\Commands;

use App\Core\Managers\FeatureMeasure\FeatureMeasureManager;
use App\Core\Models\ExtraField\ExtraField;
use App\Core\Models\FeatureMeasure\FeatureMeasure;
use App\Core\Models\Features\Features;
use App\Core\Models\GenericAsset\GenericAsset;
use App\Core\Services\Company\CompanyService;
use App\Core\Services\Entity\EntityService;
use App\Core\Services\EntityUser\EntityUserService;
use App\Core\Services\ExtraField\ExtraFieldService;
use App\Core\Services\FeatureMeasure\FeatureMeasureService;
use App\Core\Services\Features\FeaturesService;
use App\Core\Services\GenericAsset\GenericAssetService;
use App\Core\Services\GenericAssetsExtraField\GenericAssetsExtraFieldService;
use App\Core\Services\Measure\MeasureService;
use App\Core\Services\User\UserService;
use App\Energy\Models\Tarif\Tarif;
use App\Energy\Services\MeasureTarif\MeasureTarifService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class TarifMeasurementCreation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:tarif-measurement';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $companyService;
    private $userService;
    private $entityUserService;
    private $genericAssetService;
    private $entityService;
    private $extraFieldService;
    private $genericAssetsExtraFieldService;
    private $measureService;
    private $featureMeasureManager;
    private $featureMeasureService;
    private $featuresService;
    private $measureTarifService;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if (!$this->companyService || !$this->userService || !$this->entityUserService
            || !$this->genericAssetService || !$this->entityService || !$this->extraFieldService
            || !$this->genericAssetsExtraFieldService || !$this->measureService
            || !$this->featureMeasureManager || !$this->featureMeasureService
            || !$this->featuresService || !$this->measureTarifService) {
            $this->companyService = $this->companyService ?? app(CompanyService::class);
            $this->userService = $this->userService ?? app(UserService::class);
            $this->entityUserService = $this->entityUserService ?? app(EntityUserService::class);
            $this->genericAssetService = $this->genericAssetService ?? app(GenericAssetService::class);
            $this->entityService = $this->entityService ?? app(EntityService::class);
            $this->extraFieldService = $this->extraFieldService ?? app(ExtraFieldService::class);
            $this->genericAssetsExtraFieldService = $this->genericAssetsExtraFieldService ?? app(GenericAssetsExtraFieldService::class);
            $this->measureService = $this->measureService ?? app(MeasureService::class);
            $this->featureMeasureManager = $this->featureMeasureManager ?? app(FeatureMeasureManager::class);
            $this->featureMeasureService = $this->featureMeasureService ?? app(FeatureMeasureService::class);
            $this->featuresService = $this->featuresService ?? app(FeaturesService::class);
            $this->measureTarifService = $this->measureTarifService ?? app(MeasureTarifService::class);
        }
       
        $tarifs = Tarif::all();
        foreach($tarifs as $tarif)
        {
            $hourFin = Carbon::parse($tarif->getHeureFin());
            if(Carbon::now() > $hourFin)
            {
            $company = $this->companyService->findById($tarif->getCompanyId());
            if($company)
            {
                $user = $this->userService->findByCompanyId($company->getId());
            }
            $userEntities = $user->entities;
                foreach($userEntities as $entity)
                {
                    if($tarif->getTypeCost() === Tarif::ACHAT)
                    {
                        $consumedAssets = $this->genericAssetService->getAllByEntityIdAndEnergyType($entity->getId(), GenericAsset::CONSUMED);
                        if($consumedAssets->isEmpty())
                        {
                            $this->processChildEntities($entity, $tarif, GenericAsset::CONSUMED);
                        }
                    }
                    else if($tarif->getTypeCost() === Tarif::VENTE) {
                        $producedAssets = $this->genericAssetService->getAllByEntityIdAndEnergyType($entity->getId(), GenericAsset::PRODUCED);
                        if($producedAssets->isEmpty())
                        {
                            $this->processChildEntities($entity, $tarif, GenericAsset::PRODUCED);
                        }
                    }
                }
            }
        }
    }
    //function to loop through childs of entity till it find generic assets
    private function processChildEntities($entity, $tarif, $typeEnergy)
    {
        if($tarif->getHeureFin() > $tarif->getHeureDebut())
        {
            $measureCreatedAt = Carbon::parse($tarif->getHeureFin())->subMinute(5);
        } else{
            $measureCreatedAt = Carbon::parse($tarif->getHeureFin())->subDay()->addMinute(5);
        }
        $entityChildren = $this->entityService->getSubEntitiesById($entity->getId());
        foreach ($entityChildren as $child) {
            $genericAssets = $this->genericAssetService->getAllByEntityIdAndEnergyType($child->getId(), $typeEnergy);
            if ($genericAssets->isEmpty()) {
                $this->processChildEntities($child, $tarif, $typeEnergy);
            } else {
                foreach ($genericAssets as $asset) {
                    $isGreater = $tarif->getHeureFin() > $tarif->getHeureDebut();

                    $facturableField = $this->extraFieldService->findByName(ExtraField::FACTURABLE);
                    $genericAssetField = $this->genericAssetsExtraFieldService->getByGenericAssetIdAndFieldId($asset->getId(), $facturableField->getId());
                    $factureFeature = $this->featuresService->getByName(Features::TARIF);
                    if ($genericAssetField !== null && $genericAssetField->getValue() === "true") {
                        if (!$this->measureService->isTarifMeasureExist($asset->getId(), $tarif->getHeureDebut(), $tarif->getHeureFin()) && !$this->measureTarifService->isTarifExist($tarif->getId(), $asset->getId(), $isGreater)) {
                            $measures = $this->measureService->getFirstAndLastEnergyMeasures($asset->getId(), $tarif->getHeureDebut(), $tarif->getHeureFin());
                       
                            if ($measures) {
                                $firstMeasure = $measures['first_energy_measure'];
                                $lastMeasure = $measures['last_energy_measure'];
                                if($firstMeasure && $lastMeasure)
                                {
                                    $initialMeasureEnergy = $this->featureMeasureManager->getValueByFeature($firstMeasure->getId(), FeatureMeasure::ENERGY);
                                    $lastMeasureEnergy = $this->featureMeasureManager->getValueByFeature($lastMeasure->getId(), FeatureMeasure::ENERGY);
                                    if ($initialMeasureEnergy !== null && $lastMeasureEnergy !== null) {
                                        $tarif_energy = ($lastMeasureEnergy->getValue() - $initialMeasureEnergy->getValue());
                                        $factureCost = $tarif_energy * $tarif->getCost();
                                        $newMeasure = $this->measureService->createForGenericAsset($asset, $measureCreatedAt);
                                        Log::info('new measure added : '. $newMeasure->getId());
                                        echo 'new measure added => ' . $newMeasure->getId() . "\n";
                                        if ($newMeasure !== null) {
                                            $this->featureMeasureService->create($newMeasure->getId(), $factureCost, $factureFeature->getId(), $measureCreatedAt);
                                            $this->measureTarifService->create($tarif->getId(), $newMeasure->getId(), $asset->getId(), $newMeasure->getCreatedAt(), $tarif_energy);
                                        }
                                    }
                                }
                            }
                        }
                        else{
                            $now = Carbon::now();
                            Log::info("Current time: {$now->format('Y-m-d H:i:s')}");
                            Log::info('Tarif already Created...');
                        }
                    }
                }
            }
        }
    }
    

}
