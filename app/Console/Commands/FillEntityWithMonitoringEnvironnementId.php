<?php

namespace App\Console\Commands;

use App\Core\Models\Entity\Entity;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FillEntityWithMonitoringEnvironnementId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:monitoring-entitiescl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $monitoring = DB::table('envirenements')->where('env_name', 'monitoring')->first();
        Entity::orderBy('id')->chunk(100, function ($entities) use ($monitoring) {
            foreach ($entities as $entity) {
                $entityModel = Entity::where('id', $entity->id)->whereNull('env_id')->first();
                $entityModel->env_id = $monitoring->id;
                $entityModel->save();
            }
        });
    }
}
