<?php

namespace App\Console\Commands;

use App\Core\Models\Measure\Measure;
use App\Core\Services\FeatureMeasure\FeatureMeasureService;
use App\Core\Services\Measure\MeasureService;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class InjectMeasures extends Command
{

    public function __construct(
        private MeasureService $measureService,
        private FeatureMeasureService $featureMeasureService
    ) {
        parent::__construct();
    }
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:inject-measures';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {

        $startTimestamp = Carbon::now()->subHours(5);
        $endTimestamp = Carbon::now();
        $interval = 5;

        $points = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $lastEnergyValues = array_fill_keys($points, 0);

        $currentTimestamp = clone $startTimestamp;
        $totalMinutes = $endTimestamp->diffInMinutes($startTimestamp);
        $currentMinutes = 0;

        while ($currentTimestamp <= $endTimestamp) {
            foreach ($points as $point) {
                $lastEnergy = $lastEnergyValues[$point];
                if ($currentTimestamp->hour >= 18 || $currentTimestamp->hour < 8) {
                    $newPuissance = -0.2;
                    $randomValue = mt_rand(1, 9) / 10000;
                    $newEnergie = $lastEnergy + $randomValue;
                } else {
                    $randomValue = mt_rand(1, 9) / 1000;
                    $newPuissance = rand(0, 10);
                    $newEnergie = $lastEnergy + $randomValue;
                }

                $data = [
                    "Puissance_Active_Actuelle" => $newPuissance,
                    "Energie_Active_Actuelle" => $newEnergie,
                    "point_id" => $point,
                    "timestamp" => $currentTimestamp,
                ];
                $dataFormatted = collect($data);

                $measure = $this->measureService->create($data['point_id'], $dataFormatted->all());
                if ($measure instanceof Measure) {
                    $this->featureMeasureService->createFeatureMeasure($measure->getId(), $dataFormatted->all(), $measure->getCreatedAt());

                    // Update the last energy value for the current point
                    $lastEnergyValues[$point] = $newEnergie;
                }
            }

            $currentTimestamp->addMinutes($interval);
            $currentMinutes += $interval;

            // Calculate progress and display the progress bar
            $progress = ($currentMinutes / $totalMinutes) * 100;
            $progress = min(max($progress, 0), 100);
            echo "\rProgress: [" . str_repeat("#", $progress) . str_repeat(" ", 100 - $progress) . "] $progress%";
        }
        echo "DONE";
    }
}
