<?php

namespace App\Console\Commands;

use App\Core\Models\Entity\Entity;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FillEntityWithEnergyEnvironnementId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:energy-entities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $energy = DB::table('envirenements')->where('env_name', 'energy')->first();
        Entity::orderBy('id')->chunk(100, function ($entities) use ($energy) {
            foreach ($entities as $entity) {
                $entityModel = Entity::where('id', $entity->id)->whereNull('env_id')->first();
                $entityModel->env_id = $energy->id;
                $entityModel->save();
            }
        });
    }
}
