<?php

namespace App\Http\Controllers\API\GenericAsset;

use App\Core\Log\LogParametersList;
use App\Core\Models\Entity\Entity;
use App\Core\Models\GenericAsset\GenericAsset;
use App\Core\Services\Entity\EntityService;
use App\Core\Services\Feature\FeatureList;
use App\Core\Services\GenericAsset\GenericAssetService;
use App\Http\Controllers\API\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class GetAllController extends Controller
{
    public function __construct(
        private EntityService $entityService,
        private GenericAssetService $genericAssetService
    ) {
        parent::__construct();
    }

    public function __invoke()
    {
        try {
            $user = $this->getCurrentUser();
            $entities = $user->entities;

            $genericAssetsList = collect();

            foreach ($entities as $entity) {
                $genericAssetsList = $genericAssetsList->merge(
                    $this->processEntityGenericAssets($entity)
                );
            }

            $perPage = 10; 
            $currentPage = request()->input('page', 1);
            $totalAssets = $genericAssetsList->count();
            $lastPage = ceil($totalAssets / $perPage);
            $totalPages = $lastPage;
            $prevPageUrl = ($currentPage > 1) ? url()->current() . '?page=' . ($currentPage - 1) : null;
            $nextPageUrl = ($currentPage < $lastPage) ? url()->current() . '?page=' . ($currentPage + 1) : null;
            $pagedData = $genericAssetsList->forPage($currentPage, $perPage);

            return $this->arrayResponse(
                [
                    'genericAssets' => $pagedData->values(),
                    'pagination' => [
                        'total' => $totalAssets,
                        'per_page' => $perPage,
                        'current_page' => $currentPage,
                        'last_page' => $lastPage,
                        'prev_page_url' => $prevPageUrl,
                        'next_page_url' => $nextPageUrl,
                        'total_pages' => $totalPages,
                    ],
                ],
                JsonResponse::HTTP_OK
            );
        } catch (Exception $e) {
            $this->logger->error(
                'An error occurred while processing the request',
                [
                    LogParametersList::FEATURE => FeatureList::ENTITY,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE => getExceptionTraceAsString($e),
                ]
            );

            return $this->errorResponse('An error occurred while processing your request. Please try again later.');
        }
    }

    private function processEntityGenericAssets(Entity $entity): array
    {
        $genericAssetsList = [];
        $entityChildrens = $this->entityService->getSubEntitiesById($entity->getId());
        $genericAssets = $this->genericAssetService->getAllByEntityId($entity->getId());

        foreach ($genericAssets as $genericAsset) {
            if ($genericAsset instanceof GenericAsset) {
                $genericAssetsList[] = $this->formatGenericAsset($genericAsset);
            }
        }

        foreach ($entityChildrens as $child) {
            $childGenericAssets = $this->genericAssetService->getAllByEntityId($child->getId());

            foreach ($childGenericAssets as $genericAsset) {
                if ($genericAsset instanceof GenericAsset) {
                    $genericAssetsList[] = $this->formatGenericAsset($genericAsset);
                }
            }
        }

        return $genericAssetsList;
    }

    private function formatGenericAsset($genericAsset): array
    {
        return [
            'id' => $genericAsset->getId(),
            'name' => $genericAsset->getName(),
            'ref' => $genericAsset->getRef(),
            'parent' => $genericAsset->parent ? $genericAsset->parent->name : null,
            'class' => $genericAsset->getClass(),
            'entity' => $genericAsset->entity ? $genericAsset->entity->name : null,
            'group' => $genericAsset->group ? preg_replace('/\p{C}+/u', '', $genericAsset->group->name) : null,
            'family' => $genericAsset->family ? $genericAsset->family->name : null,
        ];
    }
}
