<?php

namespace App\Http\Controllers\API\GenericAsset;

use App\Core\Log\LogParametersList;
use App\Core\Models\ExtraField\ExtraField;
use App\Core\Models\GenericAsset\GenericAsset;
use App\Core\Models\GenericAssetsExtraField\GenericAssetsExtraField;
use App\Core\Services\ExtraField\ExtraFieldService;
use App\Core\Services\Feature\FeatureList;
use App\Core\Services\GenericAsset\GenericAssetService;
use App\Core\Services\GenericAssetsExtraField\GenericAssetsExtraFieldService;
use App\Core\Utilities\FileStorageUtility;
use App\Http\Controllers\API\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class CreateGenericAssetController extends Controller
{
    public function __construct(
        private GenericAssetService $genericAssetService,
        private FileStorageUtility $fileStorageUtility,
        private ExtraFieldService $extraFieldService,
        private GenericAssetsExtraFieldService $genericAssetsExtraFieldService
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request)
    {
        $rules = [
            GenericAsset::NAME_COLUMN => 'required|string|max:255|unique:generic_assets,name',
            GenericAsset::REF_COLUMN => 'required|string|max:255|unique:generic_assets,ref',
            GenericAsset::ENTITY_ID_COLUMN => 'required|numeric|exists:entities,id',
            GenericAsset::FAMILY_ID_COLUMN => 'required|numeric|exists:families,id',
            GenericAsset::GROUP_ID_COLUMN => 'required|numeric|exists:groups,id',
            GenericAsset::DIAGRAM_ID_COLUMN => 'nullable|numeric|exists:diagrams,id',
            GenericAsset::CLASS_COLUMN => 'required|in:AA,A,B,C',
            GenericAsset::PHOTO_COLUMN => 'sometimes|mimes:jpeg,png,jpg,gif',
            GenericAsset::ENERGY_TYPE_COLUMN => 'required',
            'puissance_nominal' => 'required|numeric'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
        $imagePath = "";
        $data = $request->all();
        $facturableExtraFeild = $this->extraFieldService->findByName(ExtraField::FACTURABLE);
        $puissanceNominaleExtraFeild = $this->extraFieldService->findByName(ExtraField::PUISSANCE_NOMINALE);
        $isFacturable = (bool) $data["facturable"];
        $puissanceNominal = $data["puissance_nominal"];
        try {
                $asset[GenericAsset::NAME_COLUMN] = $data[GenericAsset::NAME_COLUMN];
                $asset[GenericAsset::REF_COLUMN] =  $data[GenericAsset::REF_COLUMN];
                $asset[GenericAsset::ENTITY_ID_COLUMN] = $data[GenericAsset::ENTITY_ID_COLUMN];
                $asset[GenericAsset::FAMILY_ID_COLUMN] = $data[GenericAsset::FAMILY_ID_COLUMN];
                $asset[GenericAsset::GROUP_ID_COLUMN] = $data[GenericAsset::GROUP_ID_COLUMN];
                $asset[GenericAsset::DIAGRAM_ID_COLUMN] = $data[GenericAsset::DIAGRAM_ID_COLUMN] ?? null;
                $asset[GenericAsset::CLASS_COLUMN] = $data[GenericAsset::CLASS_COLUMN];
                $asset[GenericAsset::ENERGY_TYPE_COLUMN] = $data[GenericAsset::ENERGY_TYPE_COLUMN];
                $asset[GenericAsset::PHOTO_COLUMN] = $data[GenericAsset::PHOTO_COLUMN] ?? null;
                $genericAsset = $this->genericAssetService->create($asset);
                if($genericAsset instanceof GenericAsset)
                {
                    $assetId = $genericAsset->getId();
                    $file_name = 'generic_asset_' . $assetId;
                    if($facturableExtraFeild instanceof ExtraField && $isFacturable)
                    {
                        $genericAssetsExtraField[GenericAssetsExtraField::EXTRA_FIELD_COLUMN] = $puissanceNominaleExtraFeild->getId();
                        $genericAssetsExtraField[GenericAssetsExtraField::GENERIC_ASSETS_ID_COLUMN] = $assetId;
                        $genericAssetsExtraField[GenericAssetsExtraField::VALUE_COLUMN] = $isFacturable;
                        $facturablegenericAssetsExtraField = $this->genericAssetsExtraFieldService->create($genericAssetsExtraField);
                    }
                    if($puissanceNominaleExtraFeild instanceof ExtraField)
                    {
                        $genericAssetsExtraField[GenericAssetsExtraField::EXTRA_FIELD_COLUMN] = $facturableExtraFeild->getId();
                        $genericAssetsExtraField[GenericAssetsExtraField::GENERIC_ASSETS_ID_COLUMN] = $assetId;
                        $genericAssetsExtraField[GenericAssetsExtraField::VALUE_COLUMN] = $puissanceNominal;
                        $puissanceNominalegenericAssetsExtraField = $this->genericAssetsExtraFieldService->create($genericAssetsExtraField);
                    }
                    if (!empty($data[GenericAsset::PHOTO_COLUMN])) {
                        $file_path = $file_name . '.' . $data[GenericAsset::PHOTO_COLUMN]->extension();
                        if (!file_exists($file_path)) {
                            $this->fileStorageUtility->save($file_path, file_get_contents($data[GenericAsset::PHOTO_COLUMN]));
                        }
                        $imagePath = \Storage::disk('s3')->url($file_path);
                        $genericAsset->update([

                            GenericAsset::PHOTO_COLUMN => $imagePath,
                        ]);
                    } else {
                        $genericAsset->update([
                            GenericAsset::PHOTO_COLUMN => null,
                        ]);
                    }
                    Log::info("new generic asset created successfully with ID : {$genericAsset->getId()}");
                    return $this->arrayResponse([
                        'message' => 'Generic Asset Created Successfully.',
                        'data' => $genericAsset], 200);
                }
        } catch (Exception $e) {
            $this->logger->error(
                'error while processing data',
                [
                    LogParametersList::FEATURE       => FeatureList::GENERIC_ASSET,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE   => getExceptionTraceAsString($e),
                ]
            );
            return $this->errorResponse('An error occurred.');
        }
    }
}
