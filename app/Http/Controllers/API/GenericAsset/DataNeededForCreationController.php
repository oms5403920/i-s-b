<?php

namespace App\Http\Controllers\API\GenericAsset;

use App\Core\Log\LogParametersList;
use App\Core\Services\Feature\FeatureList;
use App\Core\Services\GenericAsset\GenericAssetService;
use App\Http\Controllers\API\Controller;
use Exception;
use Illuminate\Http\Request;

class DataNeededForCreationController extends Controller
{
    public function __construct(
        private GenericAssetService $genericAssetService,
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request)
    {

        try {
            $res = $this->genericAssetService->getRelatedData();
            return $this->arrayResponse(
                [
                    'data' => $res,
                ]
            );
        } catch (Exception $e) {
            $this->logger->error(
                'error while processing data',
                [
                    LogParametersList::FEATURE       => FeatureList::GENERIC_ASSET,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE   => getExceptionTraceAsString($e),
                ]
            );
            return $this->errorResponse('An error occurred.');
        }
    }
}
