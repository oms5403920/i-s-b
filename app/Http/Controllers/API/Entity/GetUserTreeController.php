<?php

namespace App\Http\Controllers\API\Entity;

use App\Core\Log\LogParametersList;
use App\Core\Services\Entity\EntityService;
use App\Core\Services\Feature\FeatureList;
use App\Exceptions\Auth\UserNotLoggedInException;
use App\Http\Controllers\API\Controller;
use Illuminate\Http\Response;

class GetUserTreeController extends Controller
{
    public function __construct(
        private EntityService $entityService
    ) {
        parent::__construct();
    }

    public function __invoke()
    {
        try {
            $user = $this->getCurrentUser();
            $result = $this->entityService->getUserTree($user);
            if ($result) {

                return $this->arrayResponse(
                    [
                        'tree' => $result,
                    ]
                );
            } else {
                return $this->errorResponse(
                    'No entity found',
                    Response::HTTP_NOT_FOUND
                );
            }
        } catch (UserNotLoggedInException $e) {
            $this->logger->error(
                'error user is not logged in',
                [
                    LogParametersList::FEATURE => FeatureList::API_AUTH,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE => getExceptionTraceAsString($e),
                ]
            );

            return $this->errorResponse('Error user is not logged in. Please try again later.');
        }
    }
}
