<?php

namespace App\Http\Controllers\API\Entity;

use App\Core\Log\LogParametersList;
use App\Core\Models\Entity\Entity;
use App\Core\Services\Entity\EntityService;
use App\Core\Services\Feature\FeatureList;
use App\Core\Services\GenericAsset\GenericAssetService;
use App\Http\Controllers\API\Controller;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class GetEntityGenericAssetsController extends Controller
{
    public function __construct(
        private EntityService $entityService,
        private GenericAssetService $genericAssetService
    ) {
        parent::__construct();
    }

    public function __invoke(int $entityId)
    {
        try {
            $entity = $this->entityService->getById($entityId);
            if (!$entity instanceof Entity) {
                return $this->errorResponse('Entity not found in our records.', Response::HTTP_BAD_REQUEST);
            }

            $entityChildrens = $this->entityService->getSubEntitiesById($entity->getId());
            // $genericAssets = $this->genericAssetService->getAllByEntityId($entity->getId());
            $genericAssets = $entity->genericAssets;
            foreach ($entityChildrens as $child) {
                // $childGenericAssets = $this->genericAssetService->getAllByEntityId($child->getId());
                $childGenericAssets = $child->genericAssets;
                $child->hasGenericAssets = !$childGenericAssets->isEmpty();
            }

            return $this->arrayResponse(
                [
                    'childrens'     => $entityChildrens,
                    'genericAssets' => $genericAssets,
                ]
            );
        } catch (Exception $e) {
            $this->logger->error(
                'An error occurred while processing the request',
                [
                    LogParametersList::FEATURE       => FeatureList::ENTITY,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE   => getExceptionTraceAsString($e),
                ]
            );

            return $this->errorResponse('An error occurred while processing your request. Please try again later.');
        }
    }
}
