<?php

namespace App\Http\Controllers\API\CompanyParam;

use App\Core\Log\LogParametersList;
use App\Core\Services\Feature\FeatureList;
use App\Energy\Models\CompanyParams\CompanyParams;
use App\Energy\Services\CompanyParams\CompanyParamsService;
use App\Http\Controllers\API\Controller;
use Exception;
use Illuminate\Http\Request;

class UpdateCompanyParamValueController extends Controller
{
    public function __construct(
        private CompanyParamsService $companyParamsService,
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request)
    {
        $data = $request->all();
        $user = $this->getCurrentUser();
        $company_id = $user->getCompanyId();
        $paramKeys = ['calcul_hour', 'indecateurs_cles', 'energy_consume', 'facture', 'energy_produit',
        'production', 'consummation_specifique', 'trier_par', 'consumation_total', 'tendances', 'period_reinitialisation'];
        $isUpdated = false;
        foreach ($paramKeys as $paramKey) {
            try {
                $companyParamsData = $this->getCompanyParamsByKeyName($company_id, $data, $paramKey);
    
                if ($companyParamsData) {
                    $companyParam = $companyParamsData['companyParam'];
                    $value = $companyParamsData['value'];
    
                    if ($companyParam) {
                        $companyParam->update([CompanyParams::VALUE => $value]);
                        $isUpdated = true;
                    }
                }
            } catch (Exception $e) {
                $this->logger->error(
                    'Error while updating company parameters',
                    [
                        LogParametersList::FEATURE => FeatureList::CompanyParams,
                        LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                        LogParametersList::ERROR_TRACE => getExceptionTraceAsString($e),
                    ]
                );
            }
        }
    
        if ($isUpdated) {
            return $this->arrayResponse([
                'message' => 'Company Parameters Updated Successfully',
            ]);
        } else {
            return $this->arrayResponse([
                'message' => 'No Valid Parameters Found To Update',
            ]);
        }
    }

    private function getCompanyParamsByKeyName($company_id, $data, $paramKey) {
        foreach ($data as $d) {
            if (array_key_exists($paramKey, $d)) {
                $companyParam = $this->companyParamsService->getByParamName($company_id, $paramKey);
                return ['companyParam' => $companyParam,
                        'value' => $d[$paramKey]];
            }
        }
        return null;
    }
}
