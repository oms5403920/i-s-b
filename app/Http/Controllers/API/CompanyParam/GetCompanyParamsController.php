<?php

namespace App\Http\Controllers\API\CompanyParam;

use App\Core\Log\LogParametersList;
use App\Core\Services\Company\CompanyService;
use App\Core\Services\Feature\FeatureList;
use App\Energy\Services\CompanyParams\CompanyParamsService;
use App\Http\Controllers\API\Controller;
use Exception;

class GetCompanyParamsController extends Controller
{
    public function __construct(
        private CompanyParamsService $companyParamsService,
        private CompanyService $companyService
    ) {
        parent::__construct();
    }

    public function __invoke()
    {
        try {
            $user = $this->getCurrentUser();
            $company_id = $user->getCompanyId();
            $userCompany = $this->companyService->findById($company_id);
            $companyParams = $this->companyParamsService->getCompanyParams($company_id);
            $tarifs = $userCompany->tarifs;
            return $this->arrayResponse(
                [
                    'params' => $companyParams,
                    'tarifs' => $tarifs
                ]
            );
        } catch (Exception $e) {
            $this->logger->error(
                'error no params found',
                [
                    LogParametersList::FEATURE => FeatureList::CompanyParams,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE => getExceptionTraceAsString($e),
                ]
            );

            return $this->errorResponse('Error no params found');
        }
    }
}
