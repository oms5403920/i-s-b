<?php

namespace App\Http\Controllers\API\Tarif;

use App\Core\Log\LogParametersList;
use App\Core\Services\Feature\FeatureList;
use App\Energy\Models\Tarif\Tarif;
use App\Energy\Services\Tarif\TarifService;
use App\Http\Controllers\API\Controller;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DeleteTarifController extends Controller
{
    public function __construct(
        private TarifService $tarifService,
    ) {
        parent::__construct();
    }

    public function __invoke(int $id)
    {
            try {
                $isDeleted = $this->tarifService->delete($id);
                if($isDeleted)
                {
                    return $this->arrayResponse([
                        'message' => 'Tarif deleted Successfully'
                    ]);
                }
            } catch (Exception $e) {
                $this->logger->error(
                    'error while deleting tarif',
                    [
                        LogParametersList::FEATURE       => FeatureList::TARIF,
                        LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                        LogParametersList::ERROR_TRACE   => getExceptionTraceAsString($e),
                    ]
                );
                return $this->errorResponse('An error occurred.');
            }
    }
}
