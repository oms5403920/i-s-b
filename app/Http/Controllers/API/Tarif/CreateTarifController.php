<?php

namespace App\Http\Controllers\API\Tarif;

use App\Core\Log\LogParametersList;
use App\Core\Services\Feature\FeatureList;
use App\Energy\Models\Tarif\Tarif;
use App\Energy\Services\Tarif\TarifService;
use App\Http\Controllers\API\Controller;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CreateTarifController extends Controller
{
    public function __construct(
        private TarifService $tarifService,
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request)
    {
        $data = $request->all();
        $user = $this->getCurrentUser();
        $company_id = $user->getCompanyId();
        foreach ($data as $d) {
            try {
                if (!array_key_exists('id', $d)) {
                    $d['heure_debut'] = Carbon::parse($d['heure_debut']);
                    $d['heure_fin'] = Carbon::parse($d['heure_fin']);
                    $d['company_id'] = $company_id;
                    $tarif = $this->tarifService->create($d);
                    if($tarif instanceof Tarif)
                    {

                       Log::info("new tarif created successfully with ID : {$tarif->getId()}");
                    }
                } else {
                    $isUpdated = $this->updateTarif($d['id'], $d);
                    if ($isUpdated) {
                       Log::info("Tarif Updated successfully ID : {$d['id']}");
                    }
                }
            } catch (Exception $e) {
                $this->logger->error(
                    'error while processing data',
                    [
                        LogParametersList::FEATURE       => FeatureList::TARIF,
                        LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                        LogParametersList::ERROR_TRACE   => getExceptionTraceAsString($e),
                    ]
                );
                return $this->errorResponse('An error occurred.');
            }
        }
    
        return $this->arrayResponse(['message' => 'Data processed successfully.'], 200);
    }
    
    private function updateTarif(int $id, array $data)
    {
        try {
            return $this->tarifService->update($id, $data);
        } catch (Exception $e) {
            $this->logger->error(
                'error no tarif found',
                [
                    LogParametersList::FEATURE       => FeatureList::TARIF,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE   => getExceptionTraceAsString($e),
                ]
            );
            return false;
        }
    }
}
