<?php

namespace App\Http\Controllers\API\Point;

use App\Core\Log\LogParametersList;
use App\Core\Models\GenericAsset\GenericAsset;
use App\Core\Models\Point\Point;
use App\Core\Services\Feature\FeatureList;
use App\Core\Services\GenericAsset\GenericAssetService;
use App\Core\Services\Point\PointService;
use App\Energy\Models\Tarif\Tarif;
use App\Energy\Services\Tarif\TarifService;
use App\Http\Controllers\API\Controller;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class CreatePointController extends Controller
{
    public function __construct(
        private PointService $pointService,
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request)
    {
        $rules = [
            Point::NAME_COLUMN => 'required|string|max:255|unique:measurement_points,name',
            Point::TYPE_COLUMN => 'required|string|max:255',
            Point::GENERIC_ASSET_ID_COLUMN => 'required|numeric|exists:generic_assets,id',
            Point::DEVICE_ID_COLUMN => 'required|numeric',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
        $data = $request->all();
            try {
                    $point[Point::NAME_COLUMN] = $data[Point::NAME_COLUMN];
                    $point[Point::TYPE_COLUMN] =  $data[Point::TYPE_COLUMN];
                    $point[Point::GENERIC_ASSET_ID_COLUMN] = $data[Point::GENERIC_ASSET_ID_COLUMN];
                    $point[Point::DEVICE_ID_COLUMN] = $data[Point::DEVICE_ID_COLUMN];
                    $newPoint = $this->pointService->create($point);
                    if($newPoint instanceof Point)
                    {
                       Log::info("new point created successfully with ID : {$newPoint->getId()}");
                       return $this->arrayResponse(['message' => 'Generic Asset Point Created Successfully.'], 200);
                    }
            } catch (Exception $e) {
                $this->logger->error(
                    'error while processing data',
                    [
                        LogParametersList::FEATURE       => FeatureList::POINT,
                        LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                        LogParametersList::ERROR_TRACE   => getExceptionTraceAsString($e),
                    ]
                );
                return $this->errorResponse('An error occurred.');
            }
    }
}
