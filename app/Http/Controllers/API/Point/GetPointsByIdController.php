<?php

namespace App\Http\Controllers\API\Point;

use App\Core\Log\LogParametersList;
use App\Core\Services\Feature\FeatureList;
use App\Core\Services\GenericAsset\GenericAssetService;
use App\Core\Services\Point\PointService;
use App\Http\Controllers\API\Controller;
use Exception;
use Illuminate\Http\Request;

class GetPointsByIdController extends Controller
{
    public function __construct(
        private PointService $pointService,
    ) {
        parent::__construct();
    }

    public function __invoke(int $deviceId)
    {

        try {
            $points = $this->pointService->getAllByDeviceId($deviceId);
            return $this->arrayResponse(
                [
                    'points' => $points,
                ]
            );
        } catch (Exception $e) {
            $this->logger->error(
                'error while processing data',
                [
                    LogParametersList::FEATURE       => FeatureList::POINT,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE   => getExceptionTraceAsString($e),
                ]
            );
            return $this->errorResponse('An error occurred.');
        }
    }
}
