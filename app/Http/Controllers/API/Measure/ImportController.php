<?php

namespace App\Http\Controllers\API\Measure;

use App\Core\Log\LogParametersList;
use App\Core\Models\FeatureMeasure\FeatureMeasure;
use App\Core\Models\Features\Features;
use App\Core\Models\Measure\Measure;
use App\Core\Services\Feature\FeatureList;
use App\Core\Services\FeatureMeasure\FeatureMeasureService;
use App\Core\Services\Features\FeaturesService;
use App\Core\Services\Measure\MeasureService as CoreMeasureService;
use App\Core\Utilities\FileStorageUtility;
use App\Energy\Services\CompanyParams\CompanyParamsService;
use App\Energy\Services\Measure\MeasureService;
use App\Energy\Services\Params\ParamsService;
use App\Http\Controllers\API\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ImportController extends Controller
{
    public function __construct(
        private MeasureService $measureService,
        private CoreMeasureService $coreMeasureService,
        private FeatureMeasureService $featureMeasureService,
        private ParamsService $paramService,
        private CompanyParamsService $companyParamsService, 
        private FileStorageUtility $fileStorageUtility,
        private FeaturesService $featuresService
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request)
    {
        $request->validate([
            'import_file' => 'required|mimes:csv,xlsx,xlsm',
        ]);
        $productionFeature = $this->featuresService->getByName(Features::PRODUCTION);        
        $user = $this->getCurrentUser();
        $company_id = $user->getCompanyId();
        $param = $this->paramService->getByName('calcul_hour');
        $companyParams = $this->companyParamsService->getByCompanyANDParam($company_id, $param->getId());
        $calculHour =  $companyParams->getValue();
        list($hour, $minute) = explode(':', $calculHour);
        $file = $request->file('import_file');

        
        try {
            $isValid = false;
            $fileExtension = $file->getClientOriginalExtension();
            $userName = $user->getName() .'_'. $user->getLastName();
            $file_name = $userName .'_'. Carbon::now()->format('Y_m_d_H_i_s');
            $file_path = $file_name . '.' . $file->extension();  
            if ($fileExtension === 'csv') {
                $delimiterChar = ',';
                $content = file_get_contents($file->getRealPath());
                $lines = explode("\n", $content);
                $this->fileStorageUtility->save($file_path, file_get_contents($file));
                foreach ($lines as $line) {
                    $line = trim($line);
                    if (!empty($line)) {
                        $fields = explode($delimiterChar, $line);

                        if (count($fields) >= 2) {
                            $entityId = trim($fields[0]);
                            $production = trim($fields[1]);
                            $date = trim($fields[2]);
                            $importedData[] = [
                                'entityId' => $entityId,
                                'production' => $production,
                                'date' => $date,
                            ];
                        }
                    }
                }
            } elseif ($fileExtension === 'xlsx' || $fileExtension === 'xlsm') {
                
                $data = Excel::toCollection(null, $file)->get(0);
                $importedData = [];

                if($data)
                {
                    $headersData = $data[0];
                        for ($i = 0; $i < count($headersData); $i++) {
                            $head = $headersData[$i];
                            if (!is_null($head)) {
                                $headers[] = $head;
                            }
                        }
                    $fileForamt = array("Entity ID", "Entity Name", "Production", "Date");
                    //here am checking if the file respect the requested format
                    if($fileForamt === $headers)
                    {
                        foreach ($data->skip(1) as $row) {
                            $nullCount = 0;
                        
                            for ($i = 0; $i < count($row); $i++) {
                                if (is_null($row[$i])) {
                                    $nullCount++;
                                }
                            }
                        
                            // Skip the row if all values are null
                            if ($nullCount === 4) {
                                continue;
                            }
                            $date = null;
                            if (!empty($row[3])) {
                                $date = Carbon::createFromTimestamp(($row[3] - 25569) * 86400);
                            }
                            $importedData[] = [
                                'entityId' => $row[0] ?? null,
                                'production' => $row[2] ?? null,
                                'date' => $date,
                            ];
                        }
                        foreach ($importedData as $data) {
                            if (isset($data['entityId'], $data['production'], $data['date'])) {
                                $dateString = $data['date'];
                                $date = Carbon::parse($dateString)->hour($hour)->minute($minute)->addHour();
                                $measure = $this->measureService->isProductionMeasureExist($data['entityId'], $date->toDateString());
                                if (!$measure instanceof Measure) {
                                    $newMeasure = $this->coreMeasureService->createForEntity($data['entityId'], $date);
                                    log::info('created : '. $newMeasure->getId());
                                    if ($newMeasure instanceof Measure) {
                                         $this->featureMeasureService->create($newMeasure->getId(), $data['production'], $productionFeature->getId(), $date);
                                    }
                                } else {
                                    $this->featureMeasureService->updateProductionValue($measure->getId(), [FeatureMeasure::VALUE_COLUMN => $data['production']]);
                                    log::info('updated:  ' . $measure->getId());
                                }
                            } else {
                                $nullValues = [];
                                foreach ($data as $key => $value) {
                                    if ($value === null) {
                                        $nullValues[$key] = $value;
                                    }
                                }
                                log::info('Required data is missing : ' . json_encode($nullValues));
                                continue;
                            }
                        }
                        $isValid = true;
                    }
                   
                }
            }
            if($isValid)
            {
                $this->fileStorageUtility->save($file_path, file_get_contents($file));
                return $this->arrayResponse(
                    [
                        'message' => "The file has been processed successfully.",
                    ]);
            }
            else{
                return $this->errorResponse("Format is not valid");
            }
        }catch (Exception $e){
        $this->logger->error(
            'Error while importing production measures',
            [
                LogParametersList::FEATURE => FeatureList::CompanyParams,
                LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                LogParametersList::ERROR_TRACE => getExceptionTraceAsString($e),
            ]
        );
    }
    }
}