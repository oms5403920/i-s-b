<?php

namespace App\Http\Controllers\API\Measure\Dashboard;

use App\Core\Log\LogParametersList;
use App\Core\Services\Entity\EntityService;
use App\Core\Services\Feature\FeatureList;
use App\Core\Services\GenericAsset\GenericAssetService;
use App\Core\Services\Measure\MeasureService;
use App\Energy\Services\CompanyParams\CompanyParamsService;
use App\Energy\Services\FeatureMeasure\FeatureMeasureService as FeatureMeasureFeatureMeasureService;
use App\Energy\Services\Params\ParamsService;
use App\Http\Controllers\API\Controller;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class EnergyDashboardTendenceChartController extends Controller
{
    public function __construct(
        private MeasureService $measureService,
        private FeatureMeasureFeatureMeasureService $featureMeasureService,
        private GenericAssetService $genericAssetService,
        private EntityService $entityService,
        private CompanyParamsService $companyParamsService,
        private ParamsService $paramService,
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request, int $entityId = null)
    {
        try {
            $feature = 'Energy';
            $startDate = $request->startDate ?? null;
            $endDate = $request->endDate ?? null;
            $assetIds = [];
            $optAssetIds = [];
            $consumedEnergy = [];
            $user = $this->getCurrentUser();
            $company_id = $user->getCompanyId();
            $param = $this->paramService->getByName('calcul_hour');
            $companyParams = $this->companyParamsService->getByCompanyANDParam($company_id, $param->getId());
            $calculHour = (int) $companyParams->getValue();
            if ($entityId === null) {
                $user = $this->getCurrentUser();
                $userEntities = $user->entities;
            } else {
                $userEntity = $this->entityService->getById($entityId);
                $userEntities = new Collection([$userEntity]);
            }
            if($userEntities)
            {
                $this->genericAssetService->getGenericAssetsOfUserEntitiesOptimize($userEntities, $assetIds);
                // $this->genericAssetService->getGenericAssetsOfUserEntities($userEntities, $assetIds);
                if ($assetIds) {
                    $idArray = array_map(function($item) {
                        return $item['id'];
                    }, $assetIds);
                    //    $measures = $this->measureService->getGenericAssetMeasurements($idArray, $calculHour, $startDate, $endDate);
                       $measures = $this->measureService->getGenericAssetMeasurementsOfEachDay($idArray, $calculHour, $startDate, $endDate);
                   
                        $consumedEnergy = $this->featureMeasureService->energyTrendancesDashboardChart($measures, $feature);
                }
            }
            $sortedConsumedEnergy = collect($consumedEnergy)->sortBy('x')->values()->all();
            return $this->arrayResponse(
                [
                    'tendances' => $sortedConsumedEnergy,
                ]
            );
        } catch (Exception $e) {
            $this->logger->error(
                'error no measure found',
                [
                    LogParametersList::FEATURE => FeatureList::ENERGY_TENDANCES,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE => getExceptionTraceAsString($e),
                ]
            );

            return $this->errorResponse('Error no measures found to display');
        }
    }
}
