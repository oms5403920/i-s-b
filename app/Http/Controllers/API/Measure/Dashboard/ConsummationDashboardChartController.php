<?php

namespace App\Http\Controllers\API\Measure\Dashboard;

use App\Core\Log\LogParametersList;
use App\Core\Services\Entity\EntityService;
use App\Core\Services\Feature\FeatureList;
use App\Core\Services\GenericAsset\GenericAssetService;
use App\Core\Services\Measure\MeasureService;
use App\Core\Services\Point\PointService;
use App\Energy\Services\CompanyParams\CompanyParamsService;
use App\Energy\Services\FeatureMeasure\FeatureMeasureService as FeatureMeasureFeatureMeasureService;
use App\Energy\Services\Params\ParamsService;
use App\Http\Controllers\API\Controller;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ConsummationDashboardChartController extends Controller
{
    public function __construct(
        private EntityService $entityService,
        private GenericAssetService $genericAssetService,
        private PointService $pointService,
        private MeasureService $measureService,
        private FeatureMeasureFeatureMeasureService $featureMeasureService,
        private CompanyParamsService $companyParamsService,
        private ParamsService $paramService,
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request, int $id = null)
    {
        try {
            $startDate = $request->startDate ?? null;
            $endDate = $request->endDate ?? null;
            $assetIds = [];
            $consumption = [];
            $user = $this->getCurrentUser();
            $company_id = $user->getCompanyId();
            $param = $this->paramService->getByName('calcul_hour');
            $companyParams = $this->companyParamsService->getByCompanyANDParam($company_id, $param->getId());
            $calculHour =  $companyParams->getValue();
            $paramPer = $this->paramService->getByName('trier_par');
            $companyParamsPer = $this->companyParamsService->getByCompanyANDParam($company_id, $paramPer->getId());
            $per = $companyParamsPer->getValue();
            if ($id === null) {
                $userEntities = $user->entities;
            } else {
                $userEntity = $this->entityService->getById($id);
                $userEntities = new Collection([$userEntity]);
                $parentEntity = $userEntity;
            }
            if ($userEntities) {
                $entityIds = [];

                foreach ($userEntities as $entity) {
                    $entityIds[] = $entity->getId();
                }
               // $this->genericAssetService->getGenericAssetsOfUserEntitiesWithConsummation($userEntities, $assetIds, $calculHour, $startDate, $endDate,$per);
                $assetIds = $this->genericAssetService->getFirstAndLastConsumptionMeasures($entityIds, $assetIds, $calculHour, $startDate, $endDate, $per);
                // $pointIds = $this->pointService->getGenericAssetsPoints($assetIds);
                // $lastMeasures = $this->measureService->getLastOrFirstConsummationMeasureForEachPoint($pointIds, $calculHour, $startDate, $endDate, $per, true);
                // $firstMeasures = $this->measureService->getLastOrFirstConsummationMeasureForEachPoint($pointIds, $calculHour, $startDate, $endDate, $per, false);
                // $consumption = $this->featureMeasureService->consummationDashBoardChart(
                //     $firstMeasures,
                //     $lastMeasures,
                //     $parentEntity ?? null
                // );
                // $consumption = $this->featureMeasureService->consummationDashBoardChart(
                //   $assetIds,
                //     $parentEntity ?? null
                // );
                $consumption = $this->featureMeasureService->consummationDashBoardChart(
                    $assetIds,
                      $parentEntity ?? null
                  );
            }
            $consumedEnergyByAssets = $consumption;
            usort($consumedEnergyByAssets['entities'], function ($a, $b) {
                return $b['value'] <=> $a['value'];
            });
            return $this->arrayResponse(
                [
                    'Dashboard' => $consumedEnergyByAssets,
                ]
            );
        } catch (Exception $e) {
            $this->logger->error(
                'error no measure found',
                [
                    LogParametersList::FEATURE => FeatureList::ENERGY_CONSUMMATION_CHART,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE => getExceptionTraceAsString($e),
                ]
            );

            return $this->errorResponse('Error no data to display for the chart');
        }
    }
}
