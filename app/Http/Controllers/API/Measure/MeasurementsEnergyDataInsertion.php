<?php

namespace App\Http\Controllers\API\Measure;

use App\Core\Log\LogParametersList;
use App\Core\Models\Measure\Measure;
use App\Core\Services\Feature\FeatureList;
use App\Core\Services\FeatureMeasure\FeatureMeasureService;
use App\Core\Services\Measure\MeasureService;
use App\Http\Controllers\API\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class MeasurementsEnergyDataInsertion extends Controller
{
    public function __construct(
        private MeasureService $measureService,
        private FeatureMeasureService $featureMeasureService
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request)
    {
        try {
            $data = $request->all();
            $rules = [
                'point_id' => 'required|integer|exists:measurement_points,id',
                'timestamp' => 'required|date_format:Y-m-d\TH:i:s.v\Z',
                'Name' => 'required|string',
                'measure_values' => 'required|array',
                'measure_values.Power' => 'required|numeric',
                'measure_values.Energy' => 'required|numeric',
            ];
        
            $validator = Validator::make($data, $rules);
        
            if ($validator->fails()) {
                Log::info('Error messages', ['Values validation' => $validator->errors()]);
                return response()->json(['error' => $validator->errors()], 400);
            }
            $pointId = $data['point_id'];
            $timestamp = $data['timestamp'];
            $name = $data['Name'];
            $puissanceActive = $data['measure_values']['Power'];
            $energieActive = $data['measure_values']['Energy'];
            Log::info('measureData',['measureDataLog'=>$data]);
            $energyCompteur = $energieActive;
            $data['energyCompteur'] = $energyCompteur;
            $lastMeasure = $this->measureService->getLastMeasure($pointId);
            Log::info('Last Measure', ['LastMesaureLog' => $lastMeasure]);
            if($lastMeasure){
                Log::info('Last Measure Exist');
                $featureMeasurement = $lastMeasure->featureMeasurement;
                if ($featureMeasurement->isNotEmpty()) {
                    Log::info('FeatureMesurements is not empty');
                    $lastPuissance = null;
                    $lastEnergie = null;
                    $newPuissance = $puissanceActive;
                    $newMeasureDate = Carbon::parse($timestamp);
                    $lastMeasureDate = Carbon::parse($lastMeasure->created_at);
                    $diffBetweenMeasuresDatesInSec = $newMeasureDate->diffInSeconds($lastMeasureDate);
                    $diffBetweenMeasuresDatesInHours = $diffBetweenMeasuresDatesInSec / 3600;
                    foreach ($featureMeasurement as $featureMeasure) {
                        if($featureMeasure->feature->name == 'Power(kW)')
                        {
                            $lastPuissance = $featureMeasure->value;
                        }
                        if($featureMeasure->feature->name == 'Energy')
                        {
                            $lastEnergie = $featureMeasure->value;
                        }
                    }
                    $calucalatedEnergy = ($lastPuissance + $newPuissance) / 2 * $diffBetweenMeasuresDatesInHours + $lastEnergie;

                    $data['Energie_Active_Actuelle'] = $calucalatedEnergy;
                    $data['Puissance_Active_Actuelle'] = $puissanceActive;
                    Log::info($calucalatedEnergy);
                    $measure = $this->measureService->create($pointId, $data);
                    if ($measure instanceof Measure) {
                        $this->featureMeasureService->createFeatureMeasure($measure->getId(), $data,$measure->getCreatedAt());
                        $this->logger->info(
                            'measure and featureMeasure created successfully',
                            [
                                LogParametersList::FEATURE    => FeatureList::MEASUREMENT,
                                LogParametersList::MEASURE_ID => $measure->getId(),
                            ]
                        );
                    }
                }
            }else{
                Log::info('Last Measure Is not exist');
                $data['Energie_Active_Actuelle'] = 0;
                $measure = $this->measureService->create($pointId, $data);
                if ($measure instanceof Measure) {
                    $this->featureMeasureService->createFeatureMeasure($measure->getId(), $data, $measure->getCreatedAt());
                    $this->logger->info(
                        'measure and featureMeasure created successfully',
                        [
                            LogParametersList::FEATURE    => FeatureList::MEASUREMENT,
                            LogParametersList::MEASURE_ID => $measure->getId(),
                        ]
                    );
                }
            }
        } catch (Exception $e) {
            $this->logger->error(
                'error no measure found',
                [
                    LogParametersList::FEATURE       => FeatureList::MEASUREMENT,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE   => getExceptionTraceAsString($e),
                ]
            );

            return $this->errorResponse('Error no data to calcul');
        }
    }
}

