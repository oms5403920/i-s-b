<?php

namespace App\Http\Controllers\API\Measure;

use App\Core\Log\LogParametersList;
use App\Core\Models\Measure\Measure;
use App\Core\Services\Feature\FeatureList;
use App\Core\Services\FeatureMeasure\FeatureMeasureService;
use App\Core\Services\Measure\MeasureService;
use App\Http\Controllers\API\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Log;

class MeasurementsEnergyData extends Controller
{
    public function __construct(
        private MeasureService $measureService,
        private FeatureMeasureService $featureMeasureService
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request)
    {
        try {
            Log::info('measureData',['measureDataLog'=>$request->all()]);
            $energyCompteur = $request->Energie_Active_Actuelle;
            $request['energyCompteur'] = $energyCompteur;
            $lastMeasure = $this->measureService->getLastMeasure($request->point_id);
            Log::info('Last Measure', ['LastMesaureLog' => $lastMeasure]);
            if($lastMeasure){
                Log::info('Last Measure Exist');
                $featureMeasurement = $lastMeasure->featureMeasurement;
                if ($featureMeasurement->isNotEmpty()) {
                    Log::info('FeatureMesurements is not empty');
                    $lastPuissance = null;
                    $lastEnergie = null;
                    $newPuissance = $request->Puissance_Active_Actuelle;
                    $newMeasureDate = Carbon::parse($request->timestamp);
                    $lastMeasureDate = Carbon::parse($lastMeasure->created_at);
                    $diffBetweenMeasuresDatesInSec = $newMeasureDate->diffInSeconds($lastMeasureDate);
                    $diffBetweenMeasuresDatesInHours = $diffBetweenMeasuresDatesInSec / 3600;
                    foreach ($featureMeasurement as $featureMeasure) {
                        if($featureMeasure->feature->name == 'Power(kW)')
                        {
                            $lastPuissance = $featureMeasure->value;
                        }
                        if($featureMeasure->feature->name == 'Energy')
                        {
                            $lastEnergie = $featureMeasure->value;
                        }
                    }
                    $calucalatedEnergy = ($lastPuissance + $newPuissance) / 2 * $diffBetweenMeasuresDatesInHours + $lastEnergie;
                    $request['Energie_Active_Actuelle'] = $calucalatedEnergy;
                    Log::info($calucalatedEnergy);
                    $measure = $this->measureService->create($request->point_id, $request->all());
                    if ($measure instanceof Measure) {
                        $this->featureMeasureService->createFeatureMeasure($measure->getId(), $request->all(),$measure->getCreatedAt());
                        $this->logger->info(
                            'measure and featureMeasure created successfully',
                            [
                                LogParametersList::FEATURE    => FeatureList::MEASUREMENT,
                                LogParametersList::MEASURE_ID => $measure->getId(),
                            ]
                        );
                    }
                }
            }else{
                Log::info('Last Measure Is not exist');
                $request['Energie_Active_Actuelle'] = 0;
                $measure = $this->measureService->create($request->point_id, $request->all());
                if ($measure instanceof Measure) {
                    $this->featureMeasureService->createFeatureMeasure($measure->getId(), $request->all(),$measure->getCreatedAt());
                    $this->logger->info(
                        'measure and featureMeasure created successfully',
                        [
                            LogParametersList::FEATURE    => FeatureList::MEASUREMENT,
                            LogParametersList::MEASURE_ID => $measure->getId(),
                        ]
                    );
                }
            }
        } catch (Exception $e) {
            $this->logger->error(
                'error no measure found',
                [
                    LogParametersList::FEATURE       => FeatureList::MEASUREMENT,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE   => getExceptionTraceAsString($e),
                ]
            );

            return $this->errorResponse('Error no data to calcul');
        }
    }
}

