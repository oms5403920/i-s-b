<?php

namespace App\Http\Controllers\API\Measure\Details;

use App\Core\Log\LogParametersList;
use App\Core\Models\FeatureMeasure\FeatureMeasure;
use App\Core\Services\Feature\FeatureList;
use App\Core\Services\Features\FeaturesService;
use App\Core\Services\GenericAsset\GenericAssetService;
use App\Core\Services\Measure\MeasureService;
use App\Core\Services\Point\PointService;
use App\Energy\Models\CompanyParams\CompanyParams;
use App\Energy\Services\CompanyParams\CompanyParamsService;
use App\Energy\Services\FeatureMeasure\FeatureMeasureService as FeatureMeasureFeatureMeasureService;
use App\Energy\Services\Params\ParamsService;
use App\Http\Controllers\API\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DetailsEnergyStatisticsController extends Controller
{
    public function __construct(
        private MeasureService $measureService,
        private FeatureMeasureFeatureMeasureService $featureMeasureService,
        private PointService $pointService,
        private GenericAssetService $genericAssetService,
        private CompanyParamsService $companyParamsService,
        private ParamsService $paramService,
        private FeaturesService $featuresService
    ) {
        parent::__construct();
    }

    public function __invoke(string $assetIds, Request $request)
    {
        try {
            $startDate = null;
            $endDate = null;
            $production = null;
            $assetNames = '';
            $entityName = '';
            $assetIdsArray = explode(',', $assetIds);
            $featureId = $this->featuresService->getByName(FeatureMeasure::RUNNING_TIME)->getId();
            $assetIdsArray = array_map(function ($id) {
                return ['id' => $id];
            }, $assetIdsArray);
            $energy = null;
            $puissance = null;
            $user = $this->getCurrentUser();
            $company_id = $user->getCompanyId();
            $param = $this->paramService->getByName('calcul_hour');
            $companyParams = $this->companyParamsService->getByCompanyANDParam($company_id, $param->getId());
            $paramPer = $this->paramService->getByName('trier_par');
            $companyParamsPer = $this->companyParamsService->getByCompanyANDParam($company_id, $paramPer->getId());
            $per = $companyParamsPer->getValue();
            // $per =$request->per ?? "day";
            $calculHour = $companyParams->getValue();
            if ($assetIdsArray) {
                foreach ($assetIdsArray as $asset) {
                    $gAsset = $this->genericAssetService->getById($asset['id']);
                    if($gAsset)
                    {
                        $assetName = $gAsset->getName();
                        $entity = $gAsset->entity;
                        if ($entity) {
                            $entityName = $entity->getName();
                            $productionMeasure = $this->featureMeasureService->getByEntityId($entity->getId(), $per, $calculHour);
                            $productionValue = $productionMeasure ? $productionMeasure->getValue() : null;
                            $production = $productionValue;
                        }
                    }
                    $assetNames .= $assetName . ', ';
                }
                $entityGenericAssetsCount = $entity->genericAssets->count();
                $workingHours = $this->featureMeasureService->getWorkingHour($assetIdsArray, $featureId, $entity->getId(), count($assetIdsArray), $entityGenericAssetsCount, $calculHour, $per);
                $pointIds = $this->pointService->getGenericAssetsPointsStatistics($assetIdsArray);
                $lastMeasures = $this->measureService->getLastOrFirstConsummationMeasureStatistics($pointIds, $calculHour, $startDate, $endDate, $per, true);
                $firstMeasures = $this->measureService->getLastOrFirstConsummationMeasureStatistics($pointIds, $calculHour, $startDate, $endDate, $per, false);
                $result = $this->featureMeasureService->DetailsStatisticsCalculation($firstMeasures, $lastMeasures);
                $energy = $result['Energy'];
                $puissance = $result['Puissance'];
                $lastestRecordDate = $result['last_record'];
                $consummationSpecific = ($energy != 0 && $production > 0) ? (float)number_format($energy / $production, 2, '.', '') : 0;
            }
            return $this->arrayResponse([
                'Energy' =>  is_null($energy) ? '-'  : (float)number_format($energy, 2, '.', ''),
                'Puissance' =>  is_null($puissance) ? '-' : (float)number_format($puissance, 2, '.', ''),
                'consummation_specific' =>  is_null($consummationSpecific) ? '-' : $consummationSpecific,
                'assetsName' => rtrim($assetNames, ', '),
                'entity' => $entityName,
                'Production' =>  is_null($production) ? '-' : (float)number_format($production, 2, '.', ''),
                'Hours' => (float)$workingHours,
                'last_record' => $lastestRecordDate,
            ]);
        } catch (Exception $e) {
            $this->logger->error(
                'error no measure found',
                [
                    LogParametersList::FEATURE       => FeatureList::ENERGY_TENDANCES,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE   => getExceptionTraceAsString($e),
                ]
            );

            return $this->errorResponse('Error no measures found to display');
        }
    }
}
