<?php

namespace App\Http\Controllers\API\Measure\Details;

use App\Core\Log\LogParametersList;
use App\Core\Models\FeatureMeasure\FeatureMeasure;
use App\Core\Services\Feature\FeatureList;
use App\Core\Services\Measure\MeasureService;
use App\Core\Services\Point\PointService;
use App\Energy\Services\CompanyParams\CompanyParamsService;
use App\Energy\Services\FeatureMeasure\FeatureMeasureService as FeatureMeasureFeatureMeasureService;
use App\Energy\Services\MeasureTarif\MeasureTarifService;
use App\Energy\Services\Params\ParamsService;
use App\Http\Controllers\API\Controller;
use Exception;
use Illuminate\Http\Request;

class EnergyTendenceChartController extends Controller
{
    public function __construct(
        private MeasureService $measureService,
        private PointService $pointService,
        private FeatureMeasureFeatureMeasureService $featureMeasureService,
        private CompanyParamsService $companyParamsService,
        private ParamsService $paramService,
        private MeasureTarifService $measureTarifService
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request, string $assetIds, string $feature)
    {
        try {
            $assetIdsArray = explode(',', $assetIds);
            $startDate = $request->startDate ?? null;
            $endDate = $request->endDate ?? null;
            $consumedEnergy = [];
            $user = $this->getCurrentUser();
            $company_id = $user->getCompanyId();
            $param = $this->paramService->getByName('calcul_hour');
            $companyParams = $this->companyParamsService->getByCompanyANDParam($company_id, $param->getId());
            $calculHour = $companyParams->getValue();
            $paramPer = $this->paramService->getByName('trier_par');
            $companyParamsPer = $this->companyParamsService->getByCompanyANDParam($company_id, $paramPer->getId());
            $per = $request->per ?? $companyParamsPer->getValue();
            $type = $request->type ?? "cost";
            // $per =$request->per ?? "day";
            if ($assetIdsArray) {
                if($feature === FeatureMeasure::ENERGY)
                {
                    $assetIds = array_map(function ($id) {
                        return ['id' => $id];
                    }, $assetIdsArray);
                    $pointIds = $this->pointService->getGenericAssetsPoints($assetIds);
                    
                    $firstMeasures = $this->measureService->firstMeasureForEachPoint($pointIds, $calculHour, $startDate, $endDate,$per);
                    $lastMeasures = $this->measureService->lastMeasureForEachPoint($pointIds, $calculHour, $startDate, $endDate, $per);
                    $consumedEnergy = $this->featureMeasureService->energyTrendancesChart($firstMeasures, $lastMeasures, $feature);
                    
                }
                else if($feature === FeatureMeasure::POWER)
                {
                    $measures = $this->measureService->getGenericAssetMeasurements($assetIdsArray, $calculHour, $startDate, $endDate);
                    $this->featureMeasureService->powerTrendancesChart($measures, $consumedEnergy, $feature, false);
                }
                else if($feature === FeatureMeasure::RUNNING)
                {
                    $measures = $this->measureService->getGenericAssetsRunningMeasurements($assetIdsArray, $calculHour, $startDate, $endDate);
                    $this->featureMeasureService->powerTrendancesChart($measures, $consumedEnergy, $feature, true);
                }
                if($feature === "Counter")
                {
                    $feature = "energyCompteur";
                    $assetIds = array_map(function ($id) {
                        return ['id' => $id];
                    }, $assetIdsArray);
                    $pointIds = $this->pointService->getGenericAssetsPoints($assetIds);
                    $firstMeasures = $this->measureService->firstMeasureForEachPoint($pointIds, $calculHour, $startDate, $endDate,$per, true);
                    $lastMeasures = $this->measureService->lastMeasureForEachPoint($pointIds, $calculHour, $startDate, $endDate, $per, true);
                    $consumedEnergy = $this->featureMeasureService->energyTrendancesChart($firstMeasures, $lastMeasures, $feature);
                }
                if($feature == "Tarif")
                {
                   $tarifsData = $this->measureTarifService->getTarifsCosts($assetIdsArray, $startDate, $endDate, $per);
                    if($tarifsData)
                    {
                        $consumedEnergy = $this->featureMeasureService->getTarifTendencesChart($tarifsData, $type);
                    }
                }
            }

            return $this->arrayResponse(
                [
                    'tendances' => $consumedEnergy,
                ]
            );
        } catch (Exception $e) {
            $this->logger->error(
                'error no measure found',
                [
                    LogParametersList::FEATURE => FeatureList::ENERGY_TENDANCES,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE => getExceptionTraceAsString($e),
                ]
            );

            return $this->errorResponse('Error no measures found to display');
        }
    }
}
