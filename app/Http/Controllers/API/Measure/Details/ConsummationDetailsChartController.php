<?php

namespace App\Http\Controllers\API\Measure\Details;

use App\Core\Log\LogParametersList;
use App\Core\Services\Entity\EntityService;
use App\Core\Services\Feature\FeatureList;
use App\Core\Services\GenericAsset\GenericAssetService;
use App\Core\Services\Measure\MeasureService;
use App\Core\Services\Point\PointService;
use App\Energy\Services\CompanyParams\CompanyParamsService;
use App\Energy\Services\FeatureMeasure\FeatureMeasureService as FeatureMeasureFeatureMeasureService;
use App\Energy\Services\Params\ParamsService;
use App\Http\Controllers\API\Controller;
use Exception;
use Illuminate\Http\Request;

class ConsummationDetailsChartController extends Controller
{
    public function __construct(
        private EntityService $entityService,
        private GenericAssetService $genericAssetService,
        private PointService $pointService,
        private MeasureService $measureService,
        private FeatureMeasureFeatureMeasureService $featureMeasureService,
        private CompanyParamsService $companyParamsService,
        private ParamsService $paramService,
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request, int $entityId)
    {
        try {
            $startDate = $request->startDate ?? null;
            $endDate = $request->endDate ?? null;
            $entity = $this->entityService->getById($entityId);
            $consumedEnergyByAsset = [];
            $user = $this->getCurrentUser();
            $company_id = $user->getCompanyId();
            $param = $this->paramService->getByName('calcul_hour');
            $companyParams = $this->companyParamsService->getByCompanyANDParam($company_id, $param->getId());
            $paramPer = $this->paramService->getByName('trier_par');
            $companyParamsPer = $this->companyParamsService->getByCompanyANDParam($company_id, $paramPer->getId());
            $per = $request->per ?? $companyParamsPer->getValue();
            // $per =$request->per ?? "day";
            $calculHour = $companyParams->getValue();
            $paramPer = $this->paramService->getByName('trier_par');
            $companyParamsPer = $this->companyParamsService->getByCompanyANDParam($company_id, $paramPer->getId());
            $per = $request->per ?? $companyParamsPer->getValue();
            if ($entity) {
                $assetIds = $this->genericAssetService->entityGenericAssets($entity);
                $pointIds = $this->pointService->getGenericAssetsPoints($assetIds);
                $lastMeasures = $this->measureService->getLastOrFirstConsummationMeasureForEachPoint($pointIds, $calculHour, $startDate, $endDate, $per, true);
                $firstMeasures = $this->measureService->getLastOrFirstConsummationMeasureForEachPoint($pointIds, $calculHour, $startDate, $endDate, $per, false);
                $consumedEnergyByAsset = $this->featureMeasureService->consummationDetailChart(
                    $firstMeasures,
                    $lastMeasures,
                    $entity
                );
            }
            if(isset($consumedEnergyByAsset['assets']))
            {
                usort($consumedEnergyByAsset['assets'], function ($a, $b) {
                    return $b['value'] <=> $a['value'];
                });
            }
            
            return $this->arrayResponse(
                [
                    'consumed_energy' => $consumedEnergyByAsset,
                ]
            );
        } catch (Exception $e) {
            $this->logger->error(
                'error no measure found',
                [
                    LogParametersList::FEATURE => FeatureList::ENERGY_CONSUMMATION_CHART,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE => getExceptionTraceAsString($e),
                ]
            );
            return $this->errorResponse('Error no data to display for the chart');
        }
    }
}
