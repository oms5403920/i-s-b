<?php

namespace App\Http\Controllers\API\Measure\Canva;

use App\Core\Log\LogParametersList;
use App\Core\Models\User\User;
use App\Core\Services\Entity\EntityService;
use App\Core\Services\Feature\FeatureList;
use App\Core\Services\Features\FeaturesService;
use App\Exports\ProductionCanvaExport;
use App\Http\Controllers\API\Controller;
use Exception;
use Maatwebsite\Excel\Facades\Excel;

class ExportProductionCanvaController extends Controller
{
    public function __construct(
        private FeaturesService $featuresService,
        private EntityService $entityService

    ) {
        parent::__construct();
    }

    public function __invoke()
    {      
        $user = $this->getCurrentUser();
        $entities = null;
        try {
                $userEntities = $user->entities;
                $ids = [];
                    foreach ($userEntities as $entity) {
                        $entityIds[] = (int) $entity->getId();
                    }
                    $ids = $this->entityService->getDescendantIds($entityIds);
                    $entities = $this->entityService->getAllByIds(array_merge($entityIds, $ids));
                    return Excel::download(new ProductionCanvaExport($entities), 'ProductionCanva.xlsx');
            } catch (Exception $e){
            $this->logger->error(
                'Error while exporting production canva',
                [
                    LogParametersList::FEATURE => FeatureList::CompanyParams,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE => getExceptionTraceAsString($e),
                ]
            );
        }
    }
}