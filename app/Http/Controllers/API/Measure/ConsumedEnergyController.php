<?php

namespace App\Http\Controllers\API\Measure;

use App\Core\Log\LogParametersList;
use App\Core\Services\Entity\EntityService;
use App\Core\Services\Feature\FeatureList;
use App\Core\Services\GenericAsset\GenericAssetService;
use App\Core\Services\Measure\MeasureService;
use App\Core\Services\Point\PointService;
use App\Energy\Services\CompanyParams\CompanyParamsService;
use App\Energy\Services\FeatureMeasure\FeatureMeasureService as FeatureMeasureFeatureMeasureService;
use App\Energy\Services\Measure\MeasureService as MeasureMeasureService;
use App\Energy\Services\Params\ParamsService;
use PhpMqtt\Client\Facades\MQTT;
use App\Http\Controllers\API\Controller;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ConsumedEnergyController extends Controller
{
    public function __construct(
        private EntityService $entityService,
        private GenericAssetService $genericAssetService,
        private PointService $pointService,
        private MeasureService $measureService,
        private FeatureMeasureFeatureMeasureService $featureMeasureService,
        private CompanyParamsService $companyParamsService,
        private ParamsService $paramService,
        private MeasureMeasureService $energyMeasureService
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request)
    {
        try {
            $startDate = $request->startDate ?? null;
            $endDate = $request->endDate ?? null;
            $mqttTopic = 'energy/calculation';
            $consumedEnergy = null;
            $energyProduced = null;
            $production = 0;
            $assetIds = [];
            $consummationSpecific = 0;
            $user = $this->getCurrentUser();
            $company_id = $user->getCompanyId();
            $param = $this->paramService->getByName('calcul_hour');
            $companyParams = $this->companyParamsService->getByCompanyANDParam($company_id, $param->getId());
            $calculHour =  $companyParams->getValue();
            $userEntities = $user->entities;
            $paramPer = $this->paramService->getByName('trier_par');
            $companyParamsPer = $this->companyParamsService->getByCompanyANDParam($company_id, $paramPer->getId());
            $per = $companyParamsPer->getValue();
            $dateToday = Carbon::now()->toDateString();
            if ($userEntities) {
                $entityIds = $userEntities->pluck('id')->toArray();
                foreach($userEntities as $entity)
                {
                    $entityIdAsArray = [];
                    $isEntityProductionExist = $this->energyMeasureService->isProductionMeasureExistPer($entity->getId(), $per);
                    if(is_null($isEntityProductionExist)){
                        $entityIdAsArray[] = $entity->getId();
                        $productionMeasures = $this->featureMeasureService->getProductionByEntityIds($entityIdAsArray, $per, $calculHour);
                        if($productionMeasures)
                        {
                            $production += $productionMeasures->sum->getValue();
                        }
                    }
                    else{
                        $entityProductionMeasure = $this->featureMeasureService->getByEntityId($entity->getId(), $per, $calculHour);
                        $productionValue = $entityProductionMeasure ? $entityProductionMeasure->getValue() : 0;
                        $production += $productionValue;
                    }
                }
                
                
                $consumedGAssets = $this->genericAssetService->getFirstAndLastConsumptionMeasures($entityIds, $assetIds, $calculHour, $startDate, $endDate, $per, false);
                $consumedAssetsCollection = collect($consumedGAssets['assets']);
                $consumedEnergy = $consumedAssetsCollection->sum('consumedEnergy');

                $producedGAssets = $this->genericAssetService->getFirstAndLastProducedMeasures($entityIds, $assetIds, $calculHour, $startDate, $endDate, $per, false);
                $producedAssetsCollection = collect($producedGAssets['assets']);
                $energyProduced = $producedAssetsCollection->sum('consumedEnergy');

                $consummationSpecific = ($consumedEnergy != 0 && $production > 0) ? (float)number_format($consumedEnergy / $production, 2, '.', '') : 0;
            }
            $mqttData = [
                'product_energy'          => is_null($energyProduced) ? '-' : (float)number_format($energyProduced, 2, '.', ''),
                'consume_energy'          => is_null($consumedEnergy) ? '-' : (float)number_format($consumedEnergy, 2, '.', ''),
                'consommation_specifique' => $consummationSpecific == 0 ? '-' : $consummationSpecific,
                'production'              => $production == 0 ? '-' : (float)number_format($production, 2, '.', ''),
            ];            
            //MQTT::connection()->publish($mqttTopic, json_encode($mqttData));




            return $this->arrayResponse(
                $mqttData
            );
        } catch (Exception $e) {
            $this->logger->error(
                'error no measure found',
                [
                    LogParametersList::FEATURE       => FeatureList::ENERGY_CALCULS,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE   => getExceptionTraceAsString($e),
                ]
            );

            return $this->errorResponse('Error no data to calcul');
        }
    }
}
