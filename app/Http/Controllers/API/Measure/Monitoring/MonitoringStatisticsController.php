<?php

namespace App\Http\Controllers\API\Measure\Monitoring;

use App\Core\Log\LogParametersList;
use App\Core\Services\Entity\EntityService;
use App\Core\Services\Feature\FeatureList;
use App\Core\Services\GenericAsset\GenericAssetService;
use App\Core\Services\Measure\MeasureService;
use App\Core\Services\Point\PointService;
use App\Energy\Services\CompanyParams\CompanyParamsService;
use App\Energy\Services\FeatureMeasure\FeatureMeasureService as FeatureMeasureFeatureMeasureService;
use App\Energy\Services\Params\ParamsService;
use App\Http\Controllers\API\Controller;
use Exception;
use Illuminate\Database\Eloquent\Collection;

class MonitoringStatisticsController extends Controller
{
    public function __construct(
        private EntityService $entityService,
        private GenericAssetService $genericAssetService,
        private PointService $pointService,
        private MeasureService $measureService,
        private FeatureMeasureFeatureMeasureService $featureMeasureService,
        private CompanyParamsService $companyParamsService,
        private ParamsService $paramService,
    ) {
        parent::__construct();
    }

    public function __invoke(int $entityId = null)
    {
        try {
            $mqttTopic = 'monitoring/statistics';
            $production = 0;
            $assetIds = [];
            $user = $this->getCurrentUser();
            $data = [];
            if ($entityId === null) {
                $userEntities = $this->entityService->getAllByUserId($user->getId());
            } else {
                $userEntity = $this->entityService->getById($entityId);
                $userEntities = new Collection([$userEntity]);
            }
            $company_id = $user->getCompanyId();
            $param = $this->paramService->getByName('calcul_hour');
            $companyParams = $this->companyParamsService->getByCompanyANDParam($company_id, $param->getId());
            $calculHour = $companyParams->getValue();
            if ($userEntities) {
                foreach($userEntities as $entity)
                {
                    $entityChildrens = $this->entityService->getSubEntitiesById($entity->getId());
                    $hasChildrens = !$entityChildrens->isEmpty();
                    if($hasChildrens)
                    {
                        if(!$entityId)
                        {
                            $data[] = [
                                'name' => $entity->getName(),
                                'id' => $entity->getId(),
                                'Puissance' => '-',
                                'Energy' => '-',
                                'H_Marche' => '-',
                                'Production' => '-',
                                'type' => 'entity'
                            ];
                        } else{
                            foreach($entityChildrens as $child)
                            {
                                $data[] = [
                                    'name' => $child->getName(),
                                    'id' => $child->getId(),
                                    'Puissance' => '-',
                                    'Energy' => '-',
                                    'H_Marche' => '-',
                                    'Production' => '-',
                                    'type' => 'entity'
                                ];
                            }
                        }
                    }
                    else{
                        $genericAssets = $this->genericAssetService->getAllByEntityId($entity->getId());
                        $hasGenericAssits = !$genericAssets->isEmpty();
                        if($hasGenericAssits)
                        {
                            foreach($genericAssets as $gasset)
                            {
                                $assetIds[] = ['id' => $gasset->getId()];
                                $pointIds = $this->pointService->getGenericAssetsPoints($assetIds);
                                $lastMeasures = $this->measureService->lastMeasureForEachPoint($pointIds, $calculHour);
                                $firstMeasures = $this->measureService->firstMeasureForEachPoint($pointIds, $calculHour);
                                $result = $this->featureMeasureService->DetailsStatisticsCalculation($firstMeasures, $lastMeasures);
                                $energy = $result['Energy'];
                                $puissance = $result['Puissance'];
                                
                                
                                                $data[] = [
                                                    'id' => $gasset->getId(),
                                                    'name' => $gasset->getName(),
                                                    'Puissance' => (float)number_format($puissance, 2, '.', ''),
                                                    'Energy' => (float)number_format($energy, 2, '.', ''),
                                                    'H_Marche' => '-',
                                                    'Production' => (float)number_format($production, 2, '.', ''),
                                                    'type' => 'asset'
                                                ];
                            }
                        }
                    }

                }
            }
            // MQTT::connection()->publish($mqttTopic, json_encode($data));

            return $this->arrayResponse(
                $data
            );
        } catch (Exception $e) {
            $this->logger->error(
                'error no measure found',
                [
                    LogParametersList::FEATURE       => FeatureList::MONITORING,
                    LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                    LogParametersList::ERROR_TRACE   => getExceptionTraceAsString($e),
                ]
            );

            return $this->errorResponse('Error no data to calcul');
        }
    }
}
