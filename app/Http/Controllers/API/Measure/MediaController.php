<?php

namespace App\Http\Controllers\API\Measure;

use App\Core\Log\LogParametersList;
use App\Core\Services\Feature\FeatureList;
use App\Http\Controllers\API\Controller;
use Exception;

class MediaController extends Controller
{
    public function __construct(
    ) {
        parent::__construct();
    }

    public function __invoke(string $path)
    {
        try {
            $bucketName = env('AWS_BUCKET');
            $filePath = "s3://$bucketName/$path";
            $fileContent = \Storage::get($path);
            $headers = [
                'Content-Type' => \Storage::mimeType($filePath),
                'Content-Disposition' => 'attachment; filename="' . basename($path) . '"',
            ];
            return response($fileContent, 200, $headers);
        }catch (Exception $e){
        $this->logger->error(
            'Error while importing production measures',
            [
                LogParametersList::FEATURE => FeatureList::CompanyParams,
                LogParametersList::ERROR_MESSAGE => $e->getMessage(),
                LogParametersList::ERROR_TRACE => getExceptionTraceAsString($e),
            ]
        );
    }
    }
}