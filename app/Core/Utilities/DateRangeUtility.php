<?php

namespace App\Core\Utilities;

use Carbon\Carbon;

class DateRangeUtility
{
    public static function calculateRange(?string $startDate, ?string $endDate, ?string $per, string $calculHour)
    {
        $WEEK = 'week';
        $MONTH = 'month';
        $YEAR = 'year';
        $DAY = 'day';
        list($hour, $minute) = explode(':', $calculHour);
        if ($per) {
            switch ($per) {
                case $WEEK:
                    $startDate = Carbon::now()->startOfWeek()->hour($hour)->minute($minute)->toDateTimeString();
                    $endDate = Carbon::now()->endOfWeek()->addDay()->hour($hour)->minute($minute)->toDateTimeString();
                    break;
                case $MONTH:
                    $startDate = Carbon::now()->startOfMonth()->hour($hour)->minute($minute)->toDateTimeString();
                    $endDate = Carbon::now()->endOfMonth()->addDay()->hour($hour)->minute($minute)->toDateTimeString();
                    break;
                case $YEAR:
                    $startDate = Carbon::now()->startOfYear()->hour($hour)->minute($minute)->toDateTimeString();
                    $endDate = Carbon::now()->endOfYear()->addDay()->hour($hour)->minute($minute)->toDateTimeString();
                    break;
                case $DAY:
                    $now = Carbon::now();
                    $startDateValue = $now->copy()->hour(substr($calculHour, 0, 2))->minute(substr($calculHour, 3))->second(0);
                    list($hour, $minute) = explode(":", $calculHour);
                    if ($now->hour < $hour || ($now->hour == $hour && $now->minute < $minute)) {
                        $startDateValue->subDay();
                    }
                    $startDate = $startDateValue->toDateTimeString();
                    $endDate = $startDateValue->copy()->addDay()->toDateTimeString();
                    break;
            }
        }

        return [$startDate, $endDate];
    }
}
?>