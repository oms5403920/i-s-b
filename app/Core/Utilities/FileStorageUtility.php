<?php

namespace App\Core\Utilities;

class FileStorageUtility
{
    public function __construct()
    {
        //
    }

    public function save($path, $content)
    {
        try {
            \Storage::put($path,$content);
        } catch (Throwable $e) {
            report($e);

            return false;
        }
    }
}