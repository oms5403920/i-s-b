<?php

namespace App\Core\Managers\Entity;

use App\Core\Managers\Manager;
use App\Core\Models\Entity\Entity;
use App\Core\Models\Measure\Measure;
use App\Core\Models\User\User;
use App\Core\Repositories\Entity\EntityRepository;
use Illuminate\Database\Eloquent\Collection;

class EntityManager extends Manager
{
    public function __construct(
        private EntityRepository $entityRepository
    ) {
    }

    public function getById(int $id): ?Entity
    {
        return $this->entityRepository->getById($id);
    }

    public function getAllByIds(array $ids): ?Collection
    {
        return $this->entityRepository->getAllByIds($ids);
    }

    public function getSubEntitiesById(int $id): ?Collection
    {
        return $this->entityRepository->getSubEntitiesById($id);
    }

    public function update(Entity $entity, array $attributes): bool
    {
        return $this->entityRepository->update($entity, $attributes);
    }

    public function LastMeasure(int $entityId) : ?Measure
    {
       return $this->entityRepository->LastMeasure($entityId);
    }

    public function getLastRunningTimeEntity(int $entityId) : ?float
    {
       return $this->entityRepository->getLastRunningTimeEntity($entityId);
    }

    public function getTopLevelParent(int $entityId, ?int $parentEntityId = null) : ?Entity
    {
        return $this->entityRepository->getTopLevelParent($entityId, $parentEntityId);
    }

    public function getChildIds(int $entityId) : ?array
    {
        return $this->entityRepository->getChildIds($entityId);
    }

    public function getUserTree(User $user) : array
    {
        return $this->entityRepository->getUserTree($user);
    }
}
