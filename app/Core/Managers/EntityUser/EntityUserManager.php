<?php

namespace App\Core\Managers\EntityUser;

use App\Core\Managers\Manager;
use App\Core\Repositories\EntityUser\EntityUserRepository;
use Illuminate\Database\Eloquent\Collection;

class EntityUserManager extends Manager
{
    public function __construct(
        private EntityUserRepository $entityUserRepository
    ) {
    }

    public function getByUserId(int $userId): ?Collection
    {
        return $this->entityUserRepository->getByUserId($userId);
    }
}
