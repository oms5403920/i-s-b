<?php

namespace App\Core\Managers\Point;

use App\Core\Managers\Manager;
use App\Core\Models\Point\Point;
use App\Core\Repositories\Point\PointRepository;
use Illuminate\Database\Eloquent\Collection;

class PointManager extends Manager
{
    public function __construct(
        private PointRepository $pointRepository
    ) {
    }

    public function getAllByGenericAsset(int $genericAssetId): ?Collection
    {
        return $this->pointRepository->getAllByGenericAsset($genericAssetId);
    }

    public function getById(int $id): ?Point
    {
        return $this->pointRepository->getById($id);
    }

    public function create(array $attributes) : Point
    {
        return $this->pointRepository->create($attributes);
    }

    public function getAllByDeviceId(int $deviceId) : ?Collection
    {
        return $this->pointRepository->getAllByDeviceId($deviceId);
    }
}
