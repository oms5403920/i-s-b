<?php

namespace App\Core\Managers\Measure;

use App\Core\Managers\Manager;
use App\Core\Models\Measure\Measure;
use App\Core\Repositories\Measure\MeasureRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class MeasureManager extends Manager
{
    public function __construct(
        private MeasureRepository $measureRepository
    ) {
    }

    public function firstMeasureForEachPoint(
        int $pointId,
        string $calculHour,
        ?string $startDate = null,
        ?string $endDate = null,
        ?string $per = 'month',
        bool $energyCompteur = false
    ): ?Collection {
        return $this->measureRepository->firstMeasureForEachPoint($pointId, $calculHour, $startDate, $endDate, $per, $energyCompteur);
    }

    public function lastMeasureForEachPoint(
        int $pointId,
        string $calculHour,
        ?string $startDate = null,
        ?string $endDate = null,
        ?string $per = 'month', 
        bool $energyCompteur = false
    ): ?Collection {
        return $this->measureRepository->lastMeasureForEachPoint($pointId, $calculHour, $startDate, $endDate, $per, $energyCompteur);
    }

    public function getGenericAssetMeasurements(
        array $assetId,
        string $calculHour,
        ?string $startDate = null,
        ?string $endDate = null,
    ): ?Collection {
        return $this->measureRepository->getGenericAssetMeasurements($assetId, $calculHour, $startDate, $endDate);
    }

    public function create(int $pointId, int $genericAssetId, int $entityId,string $dateMeasure): ?Measure
    {
        return $this->measureRepository->create($pointId, $genericAssetId, $entityId,$dateMeasure);
    }

    public function getById(int $id): Measure
    {
        return $this->measureRepository->getById($id);
    }

    public function getBeforeLastMeasure(int $gAssetId): ?Measure
    {
        return $this->measureRepository->getBeforeLastMeasure($gAssetId);
    }

    public function firstConsummationMeasureForEachPoint(int $pointId,
    string $calculHour,
    ?string $startDate = null,
    ?string $endDate = null,
    ?string $per = null,
    bool $subDays = false) : ?Measure
    {
        return $this->measureRepository->firstConsummationMeasureForEachPoint($pointId, $calculHour, $startDate, $endDate, $per, $subDays);
    }

    public function lastConsummationMeasureForEachPoint(int $pointId,
    string $calculHour,
    ?string $startDate = null,
    ?string $endDate = null,
    ?string $per = null,
    bool $subDays = false) : ?Measure
    {
        return $this->measureRepository->lastConsummationMeasureForEachPoint($pointId, $calculHour,  $startDate, $endDate, $per, $subDays);
    }


    public function createForGenericAsset(int $gAssetId,int $entityId, Carbon $measure_created_at): ?Measure
    {
        return $this->measureRepository->createForGenericAsset($gAssetId, $entityId, $measure_created_at);
    }

    public function createForEntity(int $entityId, Carbon $measure_created_at): ?Measure
    {
        return $this->measureRepository->createForEntity($entityId, $measure_created_at);
    }

    public function getGenericAssetsRunningMeasurements(array $genericAssetId, string $calculHour,
    ?string $startDate = null, ?string $endDate = null) : ?Collection
    {
        return $this->measureRepository->getGenericAssetsRunningMeasurements($genericAssetId, $calculHour, $startDate, $endDate);
    }

    public function isTarifMeasureExist(int $genericAssetId, string $startHour, string $endHour) : bool
    {
        return $this->measureRepository->isTarifMeasureExist($genericAssetId, $startHour, $endHour);
    }

    public function getFirstAndLastEnergyMeasures(int $genericAssetId, string $heureDebut, string $heureFin) : ?array
    {
        return $this->measureRepository->getFirstAndLastEnergyMeasures($genericAssetId, $heureDebut, $heureFin);
    }

    public function getGenericAssetMeasurementsOfEachDay(
        array $gAssetsIds,
        string $calculHour,
        ?string $startDate = null,
        ?string $endDate = null,
    ): ?array {
        return $this->measureRepository->getGenericAssetMeasurementsOfEachDay($gAssetsIds, $calculHour, $startDate, $endDate);
    }

    public function getLastMeasure(int $pointId) : ?object
    {
        return $this->measureRepository->getLastMeasure($pointId);
    }
}
