<?php

namespace App\Core\Managers\Features;

use App\Core\Managers\Manager;
use App\Core\Models\Features\Features;
use App\Core\Repositories\Features\FeaturesRepository;

class FeaturesManager extends Manager
{
    public function __construct(
        private FeaturesRepository $featuresRepository
    ) {
    }

    public function getByName(string $name): ?Features
    {
        return $this->featuresRepository->getByName($name);
    }
}
