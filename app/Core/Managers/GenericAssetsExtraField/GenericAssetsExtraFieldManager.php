<?php

namespace App\Core\Managers\GenericAssetsExtraField;

use App\Core\Managers\Manager;
use App\Core\Models\GenericAssetsExtraField\GenericAssetsExtraField;
use App\Core\Repositories\GenericAssetsExtraField\GenericAssetsExtraFieldRepository;

class GenericAssetsExtraFieldManager extends Manager
{
    public function __construct(
        private GenericAssetsExtraFieldRepository $genericAssetsExtraFieldRepository
    ) {
    }

    public function getByGenericAssetIdAndFieldId(int $genericAssetId, int $fieldId) : ?GenericAssetsExtraField
    {
        return $this->genericAssetsExtraFieldRepository->getByGenericAssetIdAndFieldId($genericAssetId, $fieldId);
    }

    public function create(array $attributes) : GenericAssetsExtraField
    {
        return $this->genericAssetsExtraFieldRepository->create($attributes);
    }
}
