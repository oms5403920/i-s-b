<?php

namespace App\Core\Managers\FeatureMeasure;

use App\Core\Managers\Manager;
use App\Core\Models\FeatureMeasure\FeatureMeasure;
use App\Core\Repositories\FeatureMeasure\FeatureMeasureRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class FeatureMeasureManager extends Manager
{
    public function __construct(
        private FeatureMeasureRepository $featureMeasureRepository
    ) {
    }

    public function getValueByFeature(int $measureId, string $featureName): ?FeatureMeasure
    {
        return $this->featureMeasureRepository->getValueByFeature($measureId, $featureName);
    }

    public function create(int $measureId, float $featureValue, int $featureId,Carbon $measure_created_at): void
    {
        $this->featureMeasureRepository->create($measureId, $featureValue, $featureId,$measure_created_at);
    }

    public function getByEntityId(int $entityId, string $per, string $calculHour): ?FeatureMeasure
    {
        return $this->featureMeasureRepository->getByEntityId($entityId, $per, $calculHour);
    }

    public function getFeatureMeasure(int $measureId, int $featureId): ?FeatureMeasure
    {
        return $this->featureMeasureRepository->getFeatureMeasure($measureId, $featureId);
    }

    public function getWorkingHourOfGnericAssets(array $genericAssetIds, int $featureId, string $calculHour, string $per) : ?array
    {
        return $this->featureMeasureRepository->getWorkingHourOfGnericAssets($genericAssetIds, $featureId, $calculHour, $per);
    }
    public function getWorkingHourOfEntity(int $entityId, int $featureId, string $calculHour, string $per) : ?array
    {
        return $this->featureMeasureRepository->getWorkingHourOfEntity($entityId, $featureId, $calculHour, $per);
    }

    public function getProductionByEntityIds(array $entityIds, string $per, string $calculHour): ?Collection
    {
        return $this->featureMeasureRepository->getProductionByEntityIds($entityIds, $per, $calculHour);
    }

    public function updateProductionValue(string $measureId, array $attributes): bool
    {
        return $this->featureMeasureRepository->updateProductionValue($measureId, $attributes);
    }
}
