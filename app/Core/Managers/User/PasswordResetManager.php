<?php

namespace App\Core\Managers\User;

use App\Core\Models\User\PasswordResetToken;
use App\Core\Repositories\User\PasswordResetRepository;

class PasswordResetManager
{
    public function __construct(
        private PasswordResetRepository $passwordResetRepository
    ) {
    }

    public function findByToken(string $token): ?PasswordResetToken
    {
        return $this->passwordResetRepository->findByToken($token);
    }
}
