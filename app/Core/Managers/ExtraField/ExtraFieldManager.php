<?php

namespace App\Core\Managers\ExtraField;

use App\Core\Managers\Manager;
use App\Core\Models\ExtraField\ExtraField;
use App\Core\Repositories\ExtraField\ExtraFieldRepository;

class ExtraFieldManager extends Manager
{
    public function __construct(
        private ExtraFieldRepository $extraFieldRepository
    ) {
    }

    public function findByName(string $name) : ?ExtraField
    {
        return $this->extraFieldRepository->findByName($name);
    }
}
