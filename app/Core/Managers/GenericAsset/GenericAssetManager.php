<?php

namespace App\Core\Managers\GenericAsset;

use App\Core\Managers\Manager;
use App\Core\Models\GenericAsset\GenericAsset;
use App\Core\Models\Measure\Measure;
use App\Core\Repositories\GenericAsset\GenericAssetRepository;
use Illuminate\Database\Eloquent\Collection;

class GenericAssetManager extends Manager
{
    public function __construct(
        private GenericAssetRepository $genericAssetRepository
    ) {
    }

    public function getAllByEntityId(int $entityId): ?Collection
    {
        return $this->genericAssetRepository->getAllByEntityId($entityId);
    }

    public function getById(int $id): ?GenericAsset
    {
        return $this->genericAssetRepository->getById($id);
    }

    public function update(GenericAsset $gAsset, array $attributes): bool
    {
        return $this->genericAssetRepository->update($gAsset->getId(), $attributes);
    }

    public function getLastRunningTimeAsset(int $gAssetId) : ?float
    {
        return $this->genericAssetRepository->getLastRunningTimeAsset($gAssetId);
    }

    public function checkRunningStatus(array $genericAssetIds) : ?bool
    {
        return $this->genericAssetRepository->checkRunningStatus($genericAssetIds);
    }

    public function LastMeasure(int $gAssetId) : ?Measure
    {
       return $this->genericAssetRepository->LastMeasure($gAssetId);
    }

    public function getAllByEntityIdAndEnergyType(int $entityId, int $energyType): ?Collection
    {
        return $this->genericAssetRepository->getAllByEntityIdAndEnergyType($entityId, $energyType);
    }

    public function getByIds(array $ids): Collection
    {
        return $this->genericAssetRepository->getByIds($ids);
    }

    public function getAllByEntityIdWithConsummation(int $entityId, string $calculHour, ?string $startDate, ?string $endDate, ?string $per): ?Collection
    {
        return $this->genericAssetRepository->getAllByEntityIdWithConsummation($entityId, $calculHour, $startDate, $endDate, $per);
    }

    public function getAllByEntityIds(array $entityIds): ?Collection
    {
        return $this->genericAssetRepository->getAllByEntityIds($entityIds);
    }

    public function getFirstAndLastConsumptionMeasures(array $entityIds,
    string $calculHour,
    ?string $startDate = null,
    ?string $endDate = null,
    ?string $per = null) : ?array
    {
        return $this->genericAssetRepository->getFirstAndLastConsumptionMeasures($entityIds, $calculHour, $startDate, $endDate, $per);
    }

    public function getFirstAndLastProducedMeasures(array $entityIds,
    string $calculHour,
    ?string $startDate = null,
    ?string $endDate = null,
    ?string $per = null) : ?array
    {
        return $this->genericAssetRepository->getFirstAndLastProducedMeasures($entityIds, $calculHour, $startDate, $endDate, $per);
    }

    public function getAllByEntityIdsWithConsummation(array $entityIds, string $calculHour, ?string $startDate, ?string $endDate, ?string $per): ?Collection
    {
        return $this->genericAssetRepository->getAllByEntityIdsWithConsummation($entityIds, $calculHour, $startDate, $endDate, $per);
    }

    public function create(array $attributes) : GenericAsset
    {
        return $this->genericAssetRepository->create($attributes);
    }
    public function getRelatedData() : array
    {
        return $this->genericAssetRepository->getRelatedData();
    }

    public function getAll() : ?Collection
    {
        return $this->genericAssetRepository->getAll();
    }
}
