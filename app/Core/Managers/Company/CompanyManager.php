<?php

namespace App\Core\Managers\Company;

use App\Core\Managers\Manager;
use App\Core\Models\Company\Company;
use App\Core\Repositories\Company\CompanyRepository;

class CompanyManager extends Manager
{
    public function __construct(
        private CompanyRepository $companyRepository
    ) {
    }

    public function findById(int $id) : ?Company
    {
        return $this->companyRepository->findById($id);
    }
}
