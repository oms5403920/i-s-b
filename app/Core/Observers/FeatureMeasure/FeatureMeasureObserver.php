<?php

namespace App\Core\Observers\FeatureMeasure;

use App\Http\Controllers\API\Controller;
use App\Core\Services\Measure\MeasureService;
use App\Energy\Services\Params\ParamsService;
use App\Exceptions\Auth\UserNotLoggedInException;
use App\Core\Models\FeatureMeasure\FeatureMeasure;
use App\Core\Models\User\User;
use App\Core\Services\GenericAsset\GenericAssetService;
use App\Core\Services\Entity\EntityService;
use App\Core\Services\FeatureMeasure\FeatureMeasureService;
use App\Core\Services\Features\FeaturesService;
use App\Energy\Services\CompanyParams\CompanyParamsService;

class FeatureMeasureObserver extends Controller
{
    public function __construct(
        private MeasureService $measureService,
        private FeatureMeasureService $featureMeasureService,
        private GenericAssetService $genericAssetService,
        private CompanyParamsService $companyParamsService,
        private ParamsService $paramService,
        private EntityService $entityService,
        private FeaturesService $featuresService
    ) {
        parent::__construct();
    }

    /**
     * @throws UserNotLoggedInException
     */
    public function creating(FeatureMeasure $featureMeasure): void
    {
        $powerFeature = $this->featuresService->getByName('Power(kW)');
        if ($featureMeasure->getFeatureId() == $powerFeature->getId()) {
            $user = $this->getCurrentUser();
            // $user = User::find(4);
            $company_id = $user->getCompanyId();
            $param = $this->paramService->getByName('seuil_puissance');
            $companyParams = $this->companyParamsService->getByCompanyANDParam($company_id, $param->getId());
            $measure = $this->measureService->getById($featureMeasure->getMeasureId());
            $genericAsset = $this->genericAssetService->getById($measure->getGenericAssetId());
            // $entity = $this->entityService->getById($genericAsset->getEntityId());
            $runningTimeFeature = $this->featuresService->getByName('runningTime');
            $runningFeature = $this->featuresService->getByName('Running');
            if ($featureMeasure->getValue() > $companyParams->getValue()) {
                // create measure for the asset
                $measureGenericAsset = $this->measureService->createForGenericAsset($genericAsset, $featureMeasure->getCreatedAt());

                // create new featureMeasure running for the asset
                $this->featureMeasureService->create($measureGenericAsset->getId(), 1, $runningFeature->getId(), $featureMeasure->getCreatedAt());
                // get last running time for the asset that have runningTime
                $LastMeasureAsset = $this->genericAssetService->LastMeasure($genericAsset->getId());
                if ($LastMeasureAsset) {
                    // Check if $measureGenericAsset->getCreatedAt() is greater than $LastMeasureAsset->getCreatedAt()
                    if ($measureGenericAsset->getCreatedAt()->greaterThan($LastMeasureAsset->getCreatedAt())) {
                        // last running time for the asset
                        $runnigTimeGenericAsset = $this->genericAssetService->getLastRunningTimeAsset($genericAsset->getId());
                        $timeDifferenceInSeconds = $measureGenericAsset->getCreatedAt()->diffInSeconds($LastMeasureAsset->getCreatedAt());
                        $timeDifferenceInHours = $timeDifferenceInSeconds / 3600.0;
                        $timeDifferenceInHours = $timeDifferenceInHours + $runnigTimeGenericAsset;

                        // create new featureMeasure runningTime for the asset with the difference between now and last measure.
                        $this->featureMeasureService->create($measureGenericAsset->getId(), $timeDifferenceInHours, $runningTimeFeature->getId(), $featureMeasure->getCreatedAt());
                    }
                } else {
                    // create new featureMeasure runningTime for asset of 0 because of (first time)
                    $this->featureMeasureService->create($measureGenericAsset->getId(), 0, $runningTimeFeature->getId(), $featureMeasure->getCreatedAt());
                }

                // create Measure for entity
                $measureEntity = $this->measureService->createForEntity($genericAsset->getEntityId(), $featureMeasure->getCreatedAt());
                // create new featureMeasure running for the entity
                $this->featureMeasureService->create($measureEntity->getId(), 1, $runningFeature->getId(), $featureMeasure->getCreatedAt());
                // get last measure entity that have running time
                $LastMeasureEntity = $this->entityService->LastMeasure($genericAsset->getEntityId());
                if ($LastMeasureEntity) {
                    if ($measureEntity->getCreatedAt()->greaterThan($LastMeasureEntity->getCreatedAt())) {
                        // last running time for the entity
                        $runnigTimeEntity = $this->entityService->getLastRunningTimeEntity($genericAsset->getEntityId());
                        // take diffrence between now and last measure for the entity and convert it to hours and add it with lastRunningTime
                        $timeDifferenceInSeconds = $LastMeasureEntity->getCreatedAt()->diffInSeconds($measureEntity->getCreatedAt());
                        $timeDifferenceInHours = $timeDifferenceInSeconds / 3600.0;
                        $timeDifferenceInHours = $timeDifferenceInHours + $runnigTimeEntity;
                        // create new featureMeasure runningTime for the entity with the same last runningTime
                        $this->featureMeasureService->create($measureEntity->getId(), $timeDifferenceInHours, $runningTimeFeature->getId(), $featureMeasure->getCreatedAt());
                    }
                } else {
                    // create new featureMeasure runningTime for entity of 0 because of (first time)
                    $this->featureMeasureService->create($measureEntity->getId(), 0, $runningTimeFeature->getId(), $featureMeasure->getCreatedAt());
                }
            } else {
                // create measure for the asset
                $measureGenericAsset = $this->measureService->createForGenericAsset($genericAsset, $featureMeasure->getCreatedAt());
                // create new featureMeasure running for the asset
                $this->featureMeasureService->create($measureGenericAsset->getId(), 0.00, $runningFeature->getId(), $featureMeasure->getCreatedAt());
                // get last running time for the asset
                $runnigTimeGenericAsset = $this->genericAssetService->getLastRunningTimeAsset($genericAsset->getId());
                if ($runnigTimeGenericAsset) {
                    // create new featureMeasure runningTime for the asset with the same last runningTime
                    $this->featureMeasureService->create($measureGenericAsset->getId(), $runnigTimeGenericAsset, $runningTimeFeature->getId(), $featureMeasure->getCreatedAt());
                } else {
                    // create new featureMeasure runningTime of 0 because of (first time)
                    $this->featureMeasureService->create($measureGenericAsset->getId(), 0, $runningTimeFeature->getId(), $featureMeasure->getCreatedAt());
                }

                // get all generic assets for an entity
                $gAssets = $this->genericAssetService->getAllByEntityId($genericAsset->getEntityId());
                // check if all asset is off return true if not return false.
                $statusOfMeasures = $this->genericAssetService->checkRunningStatus($gAssets->pluck('id')->toArray());


                // if all off create measure for entity
                if ($statusOfMeasures) {
                    // create Measure for entity
                    $measureEntity = $this->measureService->createForEntity($genericAsset->getEntityId(), $featureMeasure->getCreatedAt());
                    // create new featureMeasure running for the entity
                    $this->featureMeasureService->create($measureEntity->getId(), 0, $runningFeature->getId(), $featureMeasure->getCreatedAt());
                    // get last running time for the entity
                    $runnigTimeEntity = $this->entityService->getLastRunningTimeEntity($genericAsset->getEntityId());
                    if ($runnigTimeEntity) {
                        // create new featureMeasure runningTime for the asset with the same last runningTime
                        $this->featureMeasureService->create($measureEntity->getId(), $runnigTimeEntity, $runningTimeFeature->getId(), $featureMeasure->getCreatedAt());
                    } else {
                        // create new featureMeasure runningTime of 0 because of (first time)
                        $this->featureMeasureService->create($measureEntity->getId(), 0, $runningTimeFeature->getId(), $featureMeasure->getCreatedAt());
                    }
                }
            }
        }
    }
}
