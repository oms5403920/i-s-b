<?php

namespace App\Models\GenericPoint;

use App\Core\Models\AbstractModel;
use App\Core\Models\Device\Device;
use App\Core\Models\GenericAsset\GenericAsset;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class GenericPoint extends AbstractModel
{
    public const TABLE = 'generic_points';

    protected $table = self::TABLE;

    public const NAME_COLUMN = 'name';
    public const DEVICE_ID_COLUMN = 'device_id';
    public const GENERIC_ASSET_ID_COLUMN = 'generic_asset_id';
    public const TYPE_COLUMN = 'type';

    public function getName(): string
    {
        return $this->getAttribute(self::NAME_COLUMN);
    }

    public function getDeviceId(): int
    {
        return $this->getAttribute(self::DEVICE_ID_COLUMN);
    }

    public function getGenericAssetId(): ?int
    {
        return $this->getAttribute(self::GENERIC_ASSET_ID_COLUMN);
    }

    public function getType(): string
    {
        return $this->getAttribute(self::TYPE_COLUMN);
    }

    public function genericAsset() : ?BelongsTo
    {
        return $this->belongsTo(GenericAsset::class);
    }

    public function device() : ?BelongsTo
    {
        return $this->belongsTo(Device::class);
    }
}
