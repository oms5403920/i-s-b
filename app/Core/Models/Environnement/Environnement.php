<?php

namespace App\Core\Models\Environnement;

use App\Core\Models\AbstractModel;

class Environnement extends AbstractModel
{
    public const TABLE = 'envirenements';
    protected $table = self::TABLE;

    public const NAME_COLUMN = 'env_name';

    public function getName(): string
    {
        return $this->getAttribute(self::NAME_COLUMN);
    }
}
