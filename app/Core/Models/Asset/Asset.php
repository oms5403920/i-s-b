<?php

namespace App\Core\Models\Asset;

use App\Core\Models\AbstractModel;
use App\Core\Models\Entity\Entity;
use App\Core\Models\Family\Family;
use App\Core\Models\Group\Group;
use App\Core\Models\Point\Point;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Asset extends AbstractModel
{
    public const TABLE = 'assets';
    protected $table = self::TABLE;

    public const NAME_COLUMN = 'name';
    public const REF_COLUMN = 'ref';
    public const ILLUSTRATION_COLUMN = 'illustration';
    public const MODEL_COLUMN = '3d_model';
    public const MTBR_COLUMN = 'mtbr';
    public const MTTR_COLUMN = 'mttr';
    public const RPM_COLUMN = 'rpm';
    public const BRAND_COLUMN = 'brand';
    public const CLASS_COLUMN = 'class';
    public const TYPE_COLUMN = 'type';
    public const STRUCTURE_COLUMN = 'structure';
    public const SPEED_PERIMETER_COLUMN = 'SpeedPerimeter';
    public const POWER_RANGE_COLUMN = 'powerRange';
    public const STATUS_COLUMN = 'status';
    public const ENTITY_ID_COLUMN = 'entity_id';
    public const FAMILY_ID_COLUMN = 'family_id';
    public const PARENT_ID_COLUMN = 'parent_id';
    public const DIAGRAM_ID_COLUMN = 'diagram_id';
    public const GROUP_ID_COLUMN = 'group_id';
    public const LAST_MEASURE_ID = 'last_measure_id';
    public const LAST_MEASURE_CREATED_AT = 'last_measure_created_at';

    public function getName(): string
    {
        return $this->getAttribute(self::NAME_COLUMN);
    }

    public function getRef(): string
    {
        return $this->getAttribute(self::REF_COLUMN);
    }

    public function getStructure(): ?string
    {
        return $this->getAttribute(self::STRUCTURE_COLUMN);
    }

    public function getSpeedPerimeter(): ?string
    {
        return $this->getAttribute(self::SPEED_PERIMETER_COLUMN);
    }

    public function getPowerRange(): ?string
    {
        return $this->getAttribute(self::POWER_RANGE_COLUMN);
    }

    public function getIllustration(): ?string
    {
        return $this->getAttribute(self::ILLUSTRATION_COLUMN);
    }

    public function get3DModel(): ?string
    {
        return $this->getAttribute(self::MODEL_COLUMN);
    }

    public function getMTBR(): ?string
    {
        return $this->getAttribute(self::MTBR_COLUMN);
    }

    public function getMTTR(): ?string
    {
        return $this->getAttribute(self::MTTR_COLUMN);
    }

    public function getRPM(): ?string
    {
        return $this->getAttribute(self::RPM_COLUMN);
    }

    public function getBrand(): ?string
    {
        return $this->getAttribute(self::BRAND_COLUMN);
    }

    public function getEntityId(): ?int
    {
        return $this->getAttribute(self::ENTITY_ID_COLUMN);
    }

    public function getFamilyId(): ?int
    {
        return $this->getAttribute(self::FAMILY_ID_COLUMN);
    }

    public function getDiagramId(): ?int
    {
        return $this->getAttribute(self::DIAGRAM_ID_COLUMN);
    }

    public function getGroupId(): ?int
    {
        return $this->getAttribute(self::GROUP_ID_COLUMN);
    }

    public function getClass(): ?string
    {
        return $this->getAttribute(self::CLASS_COLUMN);
    }

    public function getType(): string
    {
        return $this->getAttribute(self::TYPE_COLUMN);
    }

    public function getStatus(): ?int
    {
        return $this->getAttribute(self::STATUS_COLUMN);
    }

    public function getParentId(): ?int
    {
        return $this->getAttribute(self::PARENT_ID_COLUMN);
    }

    public function getLastMeasureId(): ?int
    {
        return $this->getAttribute(self::LAST_MEASURE_ID);
    }

    public function getLastMeasureCreatedAt()
    {
        return $this->getAttribute(self::LAST_MEASURE_CREATED_AT);
    }

    public function parent(): ?BelongsTo
    {
        return $this->belongsTo(Asset::class);
    }

    public function entity(): ?BelongsTo
    {
        return $this->belongsTo(Entity::class);
    }

    public function group(): ?BelongsTo
    {
        return $this->belongsTo(Group::class);
    }

    public function family(): ?BelongsTo
    {
        return $this->belongsTo(Family::class);
    }

    public function children() : ?HasMany
    {
        return $this->hasMany(Asset::class, self::PARENT_ID_COLUMN)->with('children');
    }

    public function points() : ?HasMany
    {
        return $this->hasMany(Point::class);
    }
}
