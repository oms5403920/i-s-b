<?php

namespace App\Core\Models\Supplier;

use App\Core\Models\AbstractModel;

class Supplier extends AbstractModel
{
    public const TABLE = 'suppliers';

    protected $table = self::TABLE;

    public const NAME_COLUMN = 'name';

    public function getName(): string
    {
        return $this->getAttribute(self::NAME_COLUMN);
    }
}