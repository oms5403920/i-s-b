<?php

namespace App\Core\Models\EntityUser;

use App\Core\Models\Entity\Entity;
use App\Core\Models\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class EntityUser extends Model
{
    public const TABLE = 'entity_user';
    protected $table = self::TABLE;

    public const USER_ID_COLUMN = 'user_id';
    public const ENTITY_ID_COLUMN = 'entity_id';

    public function getUserId(): ?int
    {
        return $this->getAttribute(self::USER_ID_COLUMN);
    }

    public function getEntityId(): ?int
    {
        return $this->getAttribute(self::ENTITY_ID_COLUMN);
    }

    public function entity(): BelongsTo
    {
        return $this->belongsTo(Entity::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
