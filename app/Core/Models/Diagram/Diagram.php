<?php

namespace App\Core\Models\Diagram;

use App\Core\Models\AbstractModel;

class Diagram extends AbstractModel
{
    public const TABLE = 'diagrams';

    protected $table = self::TABLE;

    public const NAME_COLUMN = 'name';
    public const PATH_COLUMN = 'path';
    public const FAMILY_ID_COLUMN = 'family_id';

    public function getName(): string
    {
        return $this->getAttribute(self::NAME_COLUMN);
    }

    public function getPath(): string
    {
        return $this->getAttribute(self::PATH_COLUMN);
    }

    public function getFamilyId(): int
    {
        return $this->getAttribute(self::FAMILY_ID_COLUMN);
    }
}
