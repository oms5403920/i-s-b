<?php

namespace App\Core\Models\ExtraField;

use App\Core\Models\AbstractModel;
use App\Core\Models\Gate\Gate;
use App\Core\Models\GenericAsset\GenericAsset;
use App\Core\Models\Product\Product;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ExtraField extends AbstractModel
{
    public const TABLE = 'extrafields';

    protected $table = self::TABLE;

    public const NAME_COLUMN = 'name';
    public const LABEL_COLUMN = 'label';
    public const TYPE_COLUMN = 'type';
    public const POSSIBLE_VALUES_COLUMN = 'possible_values';

    public const FACTURABLE = 'facturable';
    public const PUISSANCE_NOMINALE = 'puissance_nominale';

    public function getName(): string
    {
        return $this->getAttribute(self::NAME_COLUMN);
    }

    public function getLabel(): string
    {
        return $this->getAttribute(self::LABEL_COLUMN);
    }

    public function getType() : string
    {
        return $this->getAttribute(self::TYPE_COLUMN);
    }

    public function getPossibleValues() : string
    {
        return $this->getAttribute(self::POSSIBLE_VALUES_COLUMN);
    }

    public function genericAssets() : ?BelongsToMany
    {
        return $this->belongsToMany(GenericAsset::class, 'asset_generic_extrafield', 'extrafield_id', 'generic_asset_id');
    }
}
