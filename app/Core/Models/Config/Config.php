<?php

namespace App\Core\Models\Config;

use App\Core\Models\AbstractModel;

class Config extends AbstractModel
{
    public const TABLE = 'acquisition_configurations';

    protected $table = self::TABLE;

    public const NAME_COLUMN = 'name';
    public const DESCRIPTION_COLUMN = 'description';
    public const SAMPLING_COLUMN = 'sampling';
    public const RESOLUTION_COLUMN = 'resolution';
    public const RESOLUTION_ACC_COLUMN = 'resolution_acc';
    public const RESOLUTION_ENV_COLUMN = 'resolution_env';
    public const DURATION_COLUMN = 'duration';
    public const OVERLAP_COLUMN = 'overlap';
    public const MIN_ACCELERATION_COLUMN = 'min_acceleration';
    public const MAX_ACCELERATION_COLUMN = 'max_acceleration';
    public const MIN_VELOCITY_COLUMN = 'min_velocity';
    public const MAX_VELOCITY_COLUMN = 'max_velocity';

    public function getName(): string
    {
        return $this->getAttribute(self::NAME_COLUMN);
    }

    public function getDescription(): ?string
    {
        return $this->getAttribute(self::DESCRIPTION_COLUMN);
    }

    public function getSampling(): float
    {
        return $this->getAttribute(self::SAMPLING_COLUMN);
    }

    public function getResolution(): float
    {
        return $this->getAttribute(self::RESOLUTION_COLUMN);
    }

    public function getResolutionAcc(): ?float
    {
        return $this->getAttribute(self::RESOLUTION_ACC_COLUMN);
    }

    public function getResolutionEnv(): ?float
    {
        return $this->getAttribute(self::RESOLUTION_ENV_COLUMN);
    }

    public function getDuration(): float
    {
        return $this->getAttribute(self::DURATION_COLUMN);
    }

    public function getOverlap(): float
    {
        return $this->getAttribute(self::OVERLAP_COLUMN);
    }

    public function getMinAcceleration(): float
    {
        return $this->getAttribute(self::MIN_ACCELERATION_COLUMN);
    }

    public function getMaxAcceleration(): float
    {
        return $this->getAttribute(self::MAX_ACCELERATION_COLUMN);
    }

    public function getMinVelocity(): float
    {
        return $this->getAttribute(self::MIN_VELOCITY_COLUMN);
    }

    public function getMaxVelocity(): float
    {
        return $this->getAttribute(self::MAX_VELOCITY_COLUMN);
    }
}
