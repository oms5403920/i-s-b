<?php

namespace App\Core\Models\Family;

use App\Core\Models\AbstractModel;
use App\Core\Models\Diagram\Diagram;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Family extends AbstractModel
{
    public const TABLE = 'families';

    protected $table = self::TABLE;

    public const NAME_COLUMN = 'name';

    public function getName(): string
    {
        return $this->getAttribute(self::NAME_COLUMN);
    }

    public function faults() : BelongsToMany
    {
        return $this->belongsToMany(Fault::class);
    }

    public function diagrams() : HasMany
    {
        return $this->hasMany(Diagram::class);
    }
}
