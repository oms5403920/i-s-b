<?php

namespace App\Core\Models\Entity;

use App\Core\Models\AbstractModel;
use App\Core\Models\Asset\Asset;
use App\Core\Models\Environnement\Environnement;
use App\Core\Models\GenericAsset\GenericAsset;
use App\Core\Models\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Entity extends AbstractModel
{
    use HasFactory;

    public const Table = 'entities';
    protected $table = self::Table;

    public const REF_COLUMN = 'ref';
    public const NAME_COLUMN = 'name';
    public const LATITUDE_COLUMN = 'latitude';
    public const LONGITUDE_COLUMN = 'longitude';
    public const TYPE_COLUMN = 'type';
    public const PARENT_ID_COLUMN = 'parent_id';
    public const ENV_ID_COLUMN = 'env_id';
    public const SHAPE_COLUMN = 'shape';
    public const LAST_MEASURE_CREATED_AT = 'last_measure_created_at';

    public function getRef(): string
    {
        return $this->getAttribute(self::REF_COLUMN);
    }

    public function getName(): string
    {
        return $this->getAttribute(self::NAME_COLUMN);
    }

    public function getLatitude(): ?float
    {
        return $this->getAttribute(self::LATITUDE_COLUMN);
    }

    public function getLongitude(): ?float
    {
        return $this->getAttribute(self::LONGITUDE_COLUMN);
    }

    public function getType(): ?int
    {
        return $this->getAttribute(self::TYPE_COLUMN);
    }

    public function getParentId(): ?int
    {
        return $this->getAttribute(self::PARENT_ID_COLUMN);
    }

    public function getEnvId(): ?int
    {
        return $this->getAttribute(self::ENV_ID_COLUMN);
    }

    public function getShape(): ?string
    {
        return $this->getAttribute(self::SHAPE_COLUMN);
    }

    public function getLastMeasureCreatedId()
    {
        return $this->getAttribute(self::LAST_MEASURE_CREATED_AT);
    }

    public function parent(): ?BelongsTo
    {
        return $this->belongsTo(Entity::class, $this->getAttribute(self::PARENT_ID_COLUMN));
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function environnment(): ?BelongsTo
    {
        return $this->belongsTo(Environnement::class, 'env_id');
    }

    public function genericAssets()
    {
        return $this->hasMany(GenericAsset::class)->whereNull('deleted_at');
    }

    public function assets() : ?HasMany
    {
        return $this->hasMany(Asset::class);
    }

    public function children() : ?HasMany
    {
        return $this->hasMany(Entity::class, self::PARENT_ID_COLUMN)->with('children');
    }
}
