<?php

namespace App\Core\Models\Measure;

use App\Core\Models\AbstractModel;
use App\Core\Models\Asset\Asset;
use App\Core\Models\Device\Device;
use App\Core\Models\Entity\Entity;
use App\Core\Models\FeatureMeasure\FeatureMeasure;
use App\Core\Models\Features\Features;
use App\Core\Models\GenericAsset\GenericAsset;
use App\Core\Models\Point\Point;
use App\Models\GenericPoint\GenericPoint;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Measure extends AbstractModel
{
    public const TABLE = 'measurements';

    protected $table = self::TABLE;

    public const STATUS_COLUMN = 'status';
    public const CONFIG_ID_COLUMN = 'config_id';
    public const IS_GENERIC_COLUMN = 'is_generic';
    public const GENERIC_ASSET_ID_COLUMN = 'generic_asset_id';
    public const POINT_ID_COLUMN = 'point_id';
    public const DEVICE_ID_COLUMN = 'device_id';
    public const ASSET_ID_COLUMN = 'asset_id';
    public const ENTITY_ID_COLUMN = 'entity_id';

    protected $guarded = [];

    public function getStatus(): string
    {
        return $this->getAttribute(self::STATUS_COLUMN);
    }

    public function getConfigId(): ?int
    {
        return $this->getAttribute(self::CONFIG_ID_COLUMN);
    }


    public function getIsGeneric(): bool
    {
        return $this->getAttribute(self::IS_GENERIC_COLUMN);
    }

    public function getGenericAssetId(): ?int
    {
        return $this->getAttribute(self::GENERIC_ASSET_ID_COLUMN);
    }

    public function getPointId(): ?int
    {
        return $this->getAttribute(self::POINT_ID_COLUMN);
    }

    public function getDeviceId(): ?int
    {
        return $this->getAttribute(self::DEVICE_ID_COLUMN);
    }

    public function getAssetId(): ?int
    {
        return $this->getAttribute(self::ASSET_ID_COLUMN);
    }

    public  function getEntityId() : ?int
    {
        return $this->getAttribute(self::ENTITY_ID_COLUMN);
    }

    public function device() : ?BelongsTo
    {
        return $this->belongsTo(Device::class);
    }
    public function entity() : ?BelongsTo
    {
        return $this->belongsTo(Entity::class);
    }

    public function point() : ?BelongsTo
    {
        return $this->belongsTo(Point::class);
    }

    public function genericPoint() : ?BelongsTo
    {
        return $this->belongsTo(GenericPoint::class);
    }
    public function genericAsset() : ?BelongsTo
    {
        return $this->belongsTo(GenericAsset::class);
    }

    public function asset() : ?BelongsTo
    {
        return $this->belongsTo(Asset::class);
    }
    public function featureMeasurement()
    {
        return $this->hasMany(FeatureMeasure::class)->with('feature');
    }
    
    public function features() : ?BelongsToMany
    {
        return $this->belongsToMany(Features::class,'feature_measurement','measure_id', 'feature_id')->withPivot('value');
    }
}
