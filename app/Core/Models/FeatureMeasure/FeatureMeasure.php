<?php

namespace App\Core\Models\FeatureMeasure;

use App\Core\Models\AbstractModel;
use App\Core\Models\Features\Features;
use App\Core\Models\Measure\Measure;

class FeatureMeasure extends AbstractModel
{
    public const TABLE = 'feature_measurement';
    protected $table = self::TABLE;
    protected $guarded = [];

    public const VALUE_COLUMN = 'value';
    public const FEATURE_ID_COLUMN = 'feature_id';
    public const MEASURE_ID_COLUMN = 'measure_id';
    public const DELTA_COLUMN = 'delta';

    public const POWER = "Power(kW)";
    public const ENERGY = "Energy";
    public const PRODUCTION = "Production";
    public const RUNNING = "Running";
    public const RUNNING_TIME = "runningTime";

    public const PRODUCTION_ID = 49;

    public function getValue(): float
    {
        return $this->getAttribute(self::VALUE_COLUMN);
    }

    public function getFeatureId(): ?int
    {
        return $this->getAttribute(self::FEATURE_ID_COLUMN);
    }

    public function getMeasureId(): ?int
    {
        return $this->getAttribute(self::MEASURE_ID_COLUMN);
    }

    public function getDelta(): ?float
    {
        return $this->getAttribute(self::DELTA_COLUMN);
    }

    public function feature()
    {
        return $this->belongsTo(Features::class, 'feature_id');
    }

    public function measurement()
    {
        return $this->belongsTo(Measure::class, 'measure_id');
    }
}
