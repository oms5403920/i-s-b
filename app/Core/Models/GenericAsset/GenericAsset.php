<?php

namespace App\Core\Models\GenericAsset;

use App\Core\Models\AbstractModel;
use App\Core\Models\Diagram\Diagram;
use App\Core\Models\Entity\Entity;
use App\Core\Models\ExtraField\ExtraField;
use App\Core\Models\Family\Family;
use App\Core\Models\Group\Group;
use App\Core\Models\Measure\Measure;
use App\Core\Models\Point\Point;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class GenericAsset extends AbstractModel
{
    public const TABLE = 'generic_assets';

    protected $table = self::TABLE;

    protected $guarded = [];

    public const NAME_COLUMN = 'name';
    public const REF_COLUMN = 'ref';
    public const ENTITY_ID_COLUMN = 'entity_id';
    public const FAMILY_ID_COLUMN = 'family_id';
    public const DIAGRAM_ID_COLUMN = 'diagram_id';
    public const GROUP_ID_COLUMN = 'group_id';
    public const PHOTO_COLUMN = 'photo';
    public const CLASS_COLUMN = 'class';
    public const TYPE_COLUMN = 'type';
    public const ENERGY_TYPE_COLUMN = 'energy_type';
    public const STATUS_COLUMN = 'status';
    public const LONGITUDE_COLUMN = 'longitude';
    public const LATITUDE_COLUMN = 'latitude';
    public const LAST_MEASURE_ID = 'last_measure_id';
    public const LAST_MEASURE_CREATED_AT = 'last_measure_created_at';
    public const WORKING_HOURS = 'working_hours';

    public const CONSUMED = 0;
    public const PRODUCED = 1;


    public function getName() : string
{
    return $this->getAttribute(self::NAME_COLUMN);
}

    public function getRef() : string
    {
        return $this->getAttribute(self::REF_COLUMN);
    }

    public function getEntityId() : int
    {
        return $this->getAttribute(self::ENTITY_ID_COLUMN);
    }

    public function getFamilyId() : int
    {
        return $this->getAttribute(self::FAMILY_ID_COLUMN);
    }

    public function getDiagramId() : ?int
    {
        return $this->getAttribute(self::DIAGRAM_ID_COLUMN);
    }

    public function getGroupId() : int
    {
        return $this->getAttribute(self::GROUP_ID_COLUMN);
    }

    public function getPhoto() : ?string
    {
        return $this->getAttribute(self::PHOTO_COLUMN);
    }

    public function getClass() : ?string
    {
        return $this->getAttribute(self::CLASS_COLUMN);
    }

    public function getType() : ?string
    {
        return $this->getAttribute(self::TYPE_COLUMN);
    }

    public function getEnergyType() : ?int
    {
        return $this->getAttribute(self::ENERGY_TYPE_COLUMN);
    }

    public function getStatus() : ?int
    {
        return $this->getAttribute(self::STATUS_COLUMN);
    }

    public function getLongitude() : ?string
    {
        return $this->getAttribute(self::LONGITUDE_COLUMN);
    }

    public function getLatitude() : ?string
    {
        return $this->getAttribute(self::LATITUDE_COLUMN);
    }

    public function getLastMeasureId() : ?int
    {
        return $this->getAttribute(self::LAST_MEASURE_ID);
    }

    public function getLastMeasureCreatedAt()
    {
        return $this->getAttribute(self::LAST_MEASURE_CREATED_AT);
    }

    public function getWorkingHours()
    {
        return $this->getAttribute(self::WORKING_HOURS);
    }

    public function entity() : BelongsTo
    {
        return $this->belongsTo(Entity::class);
    }
    public function group() : BelongsTo
    {
        return $this->belongsTo(Group::class);
    }
    public function diagram() : ?BelongsTo
    {
        return $this->belongsTo(Diagram::class);
    }
    public function points() : ?HasMany
    {
        return $this->hasMany(Point::class);
    }
    public function extraFields() : ?BelongsToMany
    {
        return $this->belongsToMany(Extrafield::class, 'genericassetsextrafields', 'generic_asset_id', 'extrafield_id');
    }

    public function entityWithParent() : BelongsTo
    {
        return $this->belongsTo(Entity::class)->with('topLevelParent');;
    }

    public function getFirstConsummationMeasure(
        string $calculHour,
        ?string $startDate = null,
        ?string $endDate = null,
        ?string $per = null,
        bool $isLast = false
    ): ?Measure {
        $relatedPoint = $this->points()->first();

        if ($relatedPoint) {
            list($startDate, $endDate) = $relatedPoint->dateRange($startDate, $endDate, $per, $calculHour);

            return $relatedPoint->getConsummationMeasure(
                $calculHour,
                $startDate,
                $endDate,
                $per,
                $isLast
            );
        }

        return null;
    }

    public function getProductionMeasure(
        string $calculHour,
        ?string $startDate = null,
        ?string $endDate = null,
        ?string $per = null,
        bool $isLast = false
    ): ?Measure {
        $relatedPoint = $this->points()->first();

        if ($relatedPoint) {
            list($startDate, $endDate) = $relatedPoint->dateRange($startDate, $endDate, $per, $calculHour);

            return $relatedPoint->getProductionMeasure(
                $calculHour,
                $startDate,
                $endDate,
                $per,
                $isLast
            );
        }

        return null;
    }

    public function point() : ?HasOne
    {
        return $this->hasOne(Point::class);
    }

    public function family() : BelongsTo
    {
        return $this->belongsTo(Family::class);
    }
}
