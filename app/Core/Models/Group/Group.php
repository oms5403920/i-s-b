<?php

namespace App\Core\Models\Group;

use App\Core\Models\AbstractModel;

class Group extends AbstractModel
{
    public const Table = 'groups';
    protected $table = self::Table;

    public const NAME_COLUMN = 'name';

    public function getName(): string
    {
        return $this->getAttribute(self::NAME_COLUMN);
    }
}
