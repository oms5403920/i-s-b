<?php

namespace App\Core\Models\Features;

use App\Core\Models\AbstractModel;

class Features extends AbstractModel
{
    public const TABLE = 'features';
    protected $table = self::TABLE;

    public const NAME_COLUMN = 'name';
    public const DESCRIPTION_COLUMN = 'description';
    public const UNIT_COLUMN = 'unit';
    public const GRANDEUR_ID_COLUMN = 'grandeur_id';
    public const TAGS_COLUMN = 'tags';

    public const TARIF = 'Tarif';
    public const PRODUCTION = 'Production';

    public function getName(): string
    {
        return $this->getAttribute(self::NAME_COLUMN);
    }

    public function getDescription(): ?string
    {
        return $this->getAttribute(self::DESCRIPTION_COLUMN);
    }

    public function getUnit(): ?string
    {
        return $this->getAttribute(self::UNIT_COLUMN);
    }

    public function getGrandeurId(): ?int
    {
        return $this->getAttribute(self::GRANDEUR_ID_COLUMN);
    }

    public function getTags(): ?string
    {
        return $this->getAttribute(self::TAGS_COLUMN);
    }
}
