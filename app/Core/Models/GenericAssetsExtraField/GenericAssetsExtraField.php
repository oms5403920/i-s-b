<?php

namespace App\Core\Models\GenericAssetsExtraField;

use App\Core\Models\AbstractModel;

class GenericAssetsExtraField extends AbstractModel
{
    public const TABLE = 'asset_generic_extrafield';

    protected $table = self::TABLE;

    public const GENERIC_ASSETS_ID_COLUMN = 'generic_asset_id';
    public const EXTRA_FIELD_COLUMN = 'extrafield_id';
    public const VALUE_COLUMN = 'value';

    protected $guarded = [];

    public function getGenericAssetId(): string
    {
        return $this->getAttribute(self::GENERIC_ASSETS_ID_COLUMN);
    }

    public function getExtraFeildId(): string
    {
        return $this->getAttribute(self::EXTRA_FIELD_COLUMN);
    }

    public function getvalue(): string
    {
        return $this->getAttribute(self::VALUE_COLUMN);
    }
}
