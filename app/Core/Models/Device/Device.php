<?php

namespace App\Core\Models\Device;

use App\Core\Models\AbstractModel;
use App\Core\Models\Gate\Gate;
use App\Core\Models\Product\Product;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Device extends AbstractModel
{
    public const TABLE = 'devices';

    protected $table = self::TABLE;

    public const REF_COLUMN = 'ref';
    public const MAC_COLUMN = 'mac';
    public const DATE_ETALONNAGE_COLUMN = 'date_etalonnage';
    public const DATE_SERVICE_COLUMN = 'date_service';
    public const DUREE_VIE_COLUMN = 'duree_vie';
    public const FREQUENCE_ETALONNAGE_COLUMN = 'frequence_etalonnage';
    public const PRODUCT_ID_COLUMN = 'product_id';
    public const GATE_ID_COLUMN = 'gate_id';
    public const STATUS_COLUMN = 'status';

    public function getRef(): string
    {
        return $this->getAttribute(self::REF_COLUMN);
    }

    public function getMac(): string
    {
        return $this->getAttribute(self::MAC_COLUMN);
    }

    public function getDateEtalonnage()
    {
        return $this->getAttribute(self::DATE_ETALONNAGE_COLUMN);
    }

    public function getDateService()
    {
        return $this->getAttribute(self::DATE_SERVICE_COLUMN);
    }

    public function getDureeVie(): ?string
    {
        return $this->getAttribute(self::DUREE_VIE_COLUMN);
    }

    public function getFrequenceEtalonnage(): int
    {
        return $this->getAttribute(self::FREQUENCE_ETALONNAGE_COLUMN);
    }

    public function getProductId(): ?int
    {
        return $this->getAttribute(self::PRODUCT_ID_COLUMN);
    }

    public function getGateId(): ?int
    {
        return $this->getAttribute(self::GATE_ID_COLUMN);
    }

    public function getStatus(): int
    {
        return $this->getAttribute(self::STATUS_COLUMN);
    }

    public function product(): ?BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function gate(): ?BelongsTo
    {
        return $this->belongsTo(Gate::class);
    }
}
