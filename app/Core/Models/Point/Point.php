<?php

namespace App\Core\Models\Point;

use App\Core\Models\AbstractModel;
use App\Core\Models\GenericAsset\GenericAsset;
use App\Core\Models\Measure\Measure;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Core\Utilities\DateRangeUtility;
class Point extends AbstractModel
{
    public const TABLE = 'measurement_points';
    protected $table = self::TABLE;

    protected $guarded = [];

    public const NAME_COLUMN = 'name';
    public const TYPE_COLUMN = 'type';
    public const EXTERNAL_COLUMN = 'external';
    public const DEVICE_ID_COLUMN = 'device_id';
    public const ASSET_ID_COLUMN = 'asset_id';
    public const POSITION_ID_COLUMN = 'position_id';
    public const COMPONENT_ID_COLUMN = 'component_id';
    public const CONFIG_ID_COLUMN = 'config_id';
    public const GENERIC_ASSET_ID_COLUMN = 'generic_asset_id';
    public const STATUS_COLUMN = 'status';
    public const LAST_MEASURE_CREATED_AT = 'last_measure_created_at';
    public const LAST_MEASURE_ID = 'last_measure_id';

    public function getName(): string
    {
        return $this->getAttribute(self::NAME_COLUMN);
    }

    public function getType(): string
    {
        return $this->getAttribute(self::TYPE_COLUMN);
    }

    public function getExternal(): ?string
    {
        return $this->getAttribute(self::EXTERNAL_COLUMN);
    }

    public function getDeviceId(): ?int
    {
        return $this->getAttribute(self::DEVICE_ID_COLUMN);
    }

    public function getAssetId(): ?int
    {
        return $this->getAttribute(self::ASSET_ID_COLUMN);
    }

    public function getPositionId(): ?int
    {
        return $this->getAttribute(self::POSITION_ID_COLUMN);
    }

    public function getComponentId(): ?int
    {
        return $this->getAttribute(self::COMPONENT_ID_COLUMN);
    }

    public function getConfigId(): ?int
    {
        return $this->getAttribute(self::CONFIG_ID_COLUMN);
    }

    public function getGenericAssetId(): ?int
    {
        return $this->getAttribute(self::GENERIC_ASSET_ID_COLUMN);
    }

    public function getStatus(): ?string
    {
        return $this->getAttribute(self::STATUS_COLUMN);
    }

    public function getLastMeasureCreatedAt()
    {
        return $this->getAttribute(self::LAST_MEASURE_CREATED_AT);
    }

    public function getLastMeasureId(): ?int
    {
        return $this->getAttribute(self::LAST_MEASURE_ID);
    }

    public function genericAsset(): ?BelongsTo
    {
        return $this->belongsTo(GenericAsset::class);
    }
    public function measures() : ?HasMany
    {
        return $this->hasMany(Measure::class);
    }

    public function getConsummationMeasure(
        string $calculHour,
        ?string $startDate = null,
        ?string $endDate = null,
        ?string $per = null,
        bool $isLast = false
    ): ?Measure {
        if ($startDate === null || $endDate === null) {
            list($startDate, $endDate) = DateRangeUtility::calculateRange($startDate, $endDate, $per, $calculHour);
        }
    
        $query = Measure::select('measurement_points.id as point_id', 'feature_measurement.value',
            'generic_assets.energy_type as type')
            ->leftJoin('measurement_points', 'measurement_points.id', 'measurements.point_id')
            ->leftJoin('generic_assets', 'generic_assets.id', 'measurement_points.generic_asset_id')
            ->leftJoin('feature_measurement', 'feature_measurement.measure_id', 'measurements.id')
            ->where('feature_measurement.feature_id', 29)
            ->where('generic_assets.id', $this->getGenericAssetId())
            ->whereNull('measurements.deleted_at');
    
        if ($startDate === null || $endDate === null) {
            $query->whereBetween('measurements.created_at', [
                Carbon::now()->hour($calculHour)->minute(0)->second(0)->toDateTimeString(),
                Carbon::now()->toDateTimeString()
            ]);
        } else {
            if ($startDate === $endDate) {
                $query->whereDate('measurements.created_at', $startDate);
            } else {
                $query->whereBetween('measurements.created_at', [$startDate, $endDate]);
            }
        }

        $query->orderBy('measurements.created_at', $isLast ? 'desc' : 'asc');
        return $query->first();
    }
    

    public function getProductionMeasure(
        string $calculHour,
        ?string $startDate = null,
        ?string $endDate = null,
        ?string $per = null,
        bool $isLast = false
    ): ?Measure {
        list($startDate, $endDate) = DateRangeUtility::calculateRange($startDate, $endDate, $per, $calculHour);
        list($hour, $minute) = explode(':', $calculHour);
        return Measure::select('measurement_points.id as point_id', 'feature_measurement.value',
        'generic_assets.energy_type as type')
            ->leftJoin('measurement_points', 'measurement_points.id', 'measurements.point_id')
            ->leftJoin('generic_assets', 'generic_assets.id', 'measurement_points.generic_asset_id')
            ->leftJoin('feature_measurement', 'feature_measurement.measure_id', 'measurements.id')
            ->where('feature_measurement.feature_id', 49)
            ->where('generic_assets.id', $this->getGenericAssetId())
            ->whereNull('measurements.deleted_at')
            ->when(!$per, function ($query) use ($hour, $minute) {
                return $query->whereBetween('measurements.created_at', [
                    Carbon::now()->startOfMonth()->setHour((int)$hour)->setMinute((int)$minute)->toDateTimeString(),
                    Carbon::now()->toDateTimeString()
                ]);
            }, function ($query) use ($startDate, $endDate) {
                return $query->when($startDate === $endDate, function ($query) use ($startDate) {
                    return $query->whereDate('measurements.created_at', $startDate);
                }, function ($query) use ($startDate, $endDate) {
                    return $query->whereBetween('measurements.created_at', [$startDate, $endDate]);
                });
            })
            ->orderBy('measurements.created_at', $isLast ? 'desc' : 'asc')
            ->first();
    }
}
