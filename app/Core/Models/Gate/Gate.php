<?php

namespace App\Core\Models\Gate;

use App\Core\Models\AbstractModel;
use App\Core\Models\Entity\Entity;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Gate extends AbstractModel
{
    public const TABLE = 'gates';

    protected $table = self::TABLE;

    public const ID_COLUMN = 'id';
    public const NAME_COLUMN = 'name';
    public const REF_COLUMN = 'ref';
    public const PROVIDER_COLUMN = 'provider';
    public const TRANSMISSION_COLUMN = 'transmission';
    public const LAST_COMMUNICATION_COLUMN = 'last_communication';
    public const ENTITY_ID_COLUMN = 'entity_id';

    public function getName(): string
    {
        return $this->getAttribute(self::NAME_COLUMN);
    }

    public function getRef(): string
    {
        return $this->getAttribute(self::REF_COLUMN);
    }

    public function getProvider(): string
    {
        return $this->getAttribute(self::PROVIDER_COLUMN);
    }

    public function getTransmission(): string
    {
        return $this->getAttribute(self::TRANSMISSION_COLUMN);
    }

    public function getLastCommunication(): ?string
    {
        return $this->getAttribute(self::LAST_COMMUNICATION_COLUMN);
    }

    public function getEntityId(): ?int
    {
        return $this->getAttribute(self::ENTITY_ID_COLUMN);
    }

    public function entity(): ?BelongsTo
    {
        return $this->belongsTo(Entity::class);
    }
}
