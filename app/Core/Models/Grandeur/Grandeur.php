<?php

namespace App\Core\Models\Grandeur;

use App\Core\Models\AbstractModel;

class Grandeur extends AbstractModel
{
    public const TABLE = 'grandeurs';
    
    protected $table = self::TABLE;

    public const NAME_COLUMN = 'name';
    public const IMAGE_COLUMN = 'image';

    public function getName() : string
    {
        return $this->getAttribute(self::NAME_COLUMN);
    }

    public function getImage() : string
    {
        return $this->getAttribute(self::IMAGE_COLUMN);
    }
}
