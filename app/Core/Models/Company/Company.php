<?php

namespace App\Core\Models\Company;

use App\Core\Models\AbstractModel;
use App\Energy\Models\Tarif\Tarif;

class Company extends AbstractModel
{
    public const TABLE = 'companies';

    protected $table = self::TABLE;

    protected $guarded = [];

    public const NAME_COLUMN = 'name';

    public function getName(): string
    {
        return $this->getAttribute(self::NAME_COLUMN);
    }

    public function tarifs()
    {
        return $this->hasMany(Tarif::class);
    }
}
