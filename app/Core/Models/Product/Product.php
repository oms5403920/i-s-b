<?php

namespace App\Core\Models\Product;

use App\Core\Models\AbstractModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends AbstractModel
{
    public const TABLE = 'products';

    protected $table = self::TABLE;

    public const NAME_COLUMN = 'name';
    public const TRANSMISSION_COLUMN = 'transmition';
    public const POWER_COLUMN = 'power';
    public const SUPPLIER_ID_COLUMN = 'supplier_id';

    public function getName(): string
    {
        return $this->getAttribute(self::NAME_COLUMN);
    }

    public function getTransmission(): string
    {
        return $this->getAttribute(self::TRANSMISSION_COLUMN);
    }

    public function getPower(): string
    {
        return $this->getAttribute(self::POWER_COLUMN);
    }

    public function getSupplierId(): ?int
    {
        return $this->getAttribute(self::SUPPLIER_ID_COLUMN);
    }

    public function supplier() : ?BelongsTo
    {
        return $this->belongsTo(Supplier::class);
    }
}