<?php

namespace App\Core\Repositories\GenericAssetsExtraField;

use App\Core\Models\GenericAssetsExtraField\GenericAssetsExtraField;
use App\Core\Repositories\Repository;
use Illuminate\Support\Arr;

class GenericAssetsExtraFieldRepository extends Repository
{
    public function getByGenericAssetIdAndFieldId(int $genericAssetId, int $fieldId): ?GenericAssetsExtraField
    {
        return GenericAssetsExtraField::query()
            ->where(GenericAssetsExtraField::GENERIC_ASSETS_ID_COLUMN, $genericAssetId)
            ->where(GenericAssetsExtraField::EXTRA_FIELD_COLUMN, $fieldId)
            ->first();
    }

    public function create(array $attributes) : GenericAssetsExtraField
    {
        return GenericAssetsExtraField::create([
            GenericAssetsExtraField::GENERIC_ASSETS_ID_COLUMN => Arr::get($attributes, GenericAssetsExtraField::GENERIC_ASSETS_ID_COLUMN),
            GenericAssetsExtraField::EXTRA_FIELD_COLUMN => Arr::get($attributes, GenericAssetsExtraField::EXTRA_FIELD_COLUMN),
            GenericAssetsExtraField::VALUE_COLUMN => Arr::get($attributes, GenericAssetsExtraField::VALUE_COLUMN)
        ]);
    }
}
