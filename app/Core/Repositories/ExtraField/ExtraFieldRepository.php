<?php

namespace App\Core\Repositories\ExtraField;

use App\Core\Models\ExtraField\ExtraField;
use App\Core\Repositories\Repository;

class ExtraFieldRepository extends Repository
{
    public function findByName(string $name): ?ExtraField
    {
        return ExtraField::query()
            ->where(ExtraField::NAME_COLUMN, $name)
            ->first();
    }
}
