<?php

namespace App\Core\Repositories\User;

use App\Core\Repositories\Repository;
use App\Core\Models\User\PasswordResetToken;

class PasswordResetRepository extends Repository
{
    public function findByToken(string $token): ?PasswordResetToken
    {
        return PasswordResetToken::query()
            ->where(PasswordResetToken::TOKEN_COLUMN, $token)
            ->first();
    }
}
