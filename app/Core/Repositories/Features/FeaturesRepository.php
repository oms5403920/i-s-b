<?php

namespace App\Core\Repositories\Features;

use App\Core\Models\Features\Features;
use App\Core\Repositories\Repository;

class FeaturesRepository extends Repository
{
    public function getByName($name) : ?Features {
        return Features::where(Features::NAME_COLUMN,$name)->first();
    }
}
