<?php

namespace App\Core\Repositories\Measure;

use App\Core\Models\FeatureMeasure\FeatureMeasure;
use App\Core\Models\Features\Features;
use App\Core\Models\Measure\Measure;
use App\Core\Repositories\Repository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class MeasureRepository extends Repository
{
    public function firstMeasureForEachPoint(
        int $pointId,
        string $calculHour,
        ?string $startDate = null,
        ?string $endDate = null,
        ?string $per = 'month',
        bool $energyCompteur = false
    ): ?Collection {
        return $this->getMeasurements($pointId, $calculHour, $startDate, $endDate, $per, 'asc', 'MIN', $energyCompteur);
    }


    public function lastMeasureForEachPoint(
        int $pointId,
        string $calculHour,
        ?string $startDate = null,
        ?string $endDate = null,
        ?string $per = 'month',
        bool $energyCompteur = false
    ): ?Collection {
        return $this->getMeasurements($pointId, $calculHour, $startDate, $endDate, $per, 'desc', 'MAX', $energyCompteur);
    }
    public function getGenericAssetMeasurements(
        array $assetId,
        string $calculHour,
        ?string $startDate = null,
        ?string $endDate = null
    ): ?Collection {
        list($hour, $minute) = explode(':', $calculHour);
        $query = Measure::whereIn(Measure::GENERIC_ASSET_ID_COLUMN, $assetId)
            ->whereNull('measurements.deleted_at')
            ->select('generic_assets.energy_type as type', 'measurements.id', 'measurements.created_at as date', 'feature_measurement.value', 'generic_assets.name', 'features.unit', 'generic_assets.id as assetId')
            ->join('generic_assets', 'generic_assets.id', 'measurements.generic_asset_id')
            ->join('feature_measurement', 'feature_measurement.measure_id', 'measurements.id')
            ->join('features', 'features.id', '=', 'feature_measurement.feature_id')
            ->where('features.name', 'Power(kW)');

            $endDate = Carbon::parse($endDate)->setTime(23, 59, 59);

        if ($startDate && $endDate) {
            if ($startDate === $endDate) {
                $query->whereDate('measurements.created_at', Carbon::parse($startDate)->hour($hour)->minute($minute)->second(0)->toDateTimeString());
            } else {
                $query->whereBetween('measurements.created_at', [Carbon::parse($startDate)->hour($hour)->minute($minute)->second(0)->toDateTimeString(),
                Carbon::parse($endDate)->endOfDay()->toDateTimeString()]);
            }
        } else {
            $startDate = Carbon::now()->hour($calculHour)->minute(0)->second(0)->toDateTimeString();
            $query->where('measurements.created_at', '>=', $startDate);
            $query->orderBy('measurements.created_at', 'DESC');
            $query->limit(20);
        }

        return $query->get();
    }


    public function create(int $poinId, int $genericAssetId, int $entityId, string $dateMeasure): ?Measure
    {
        $convertedDate = Carbon::parse($dateMeasure);
        return Measure::create(
            [
                "point_id" => $poinId,
                "generic_asset_id" => $genericAssetId,
                "status" => 0,
                "is_generic" => 1,
                "entity_id" => $entityId,
                "created_at" => $convertedDate,
                "updated_at" => $convertedDate
            ]
        );
    }

    function getById(int $id): Measure
    {
        return Measure::find($id);
    }


    function getBeforeLastMeasure(int $gAssetId): ?Measure
    {
        return Measure::where(Measure::GENERIC_ASSET_ID_COLUMN, $gAssetId)
            ->orderBy(Measure::CREATED_AT, 'desc')
            ->skip(1)
            ->take(1)
            ->first();
    }

    public function firstConsummationMeasureForEachPoint(int $pointId,
    string $calculHour,
    ?string $startDate = null,
    ?string $endDate = null,
    ?string $per = null,
    bool $subDays = false): ?Measure {
        return $this->getConsummationMeasure($pointId, $calculHour, $startDate, $endDate, $per, $subDays);
    }

    public function lastConsummationMeasureForEachPoint(int $pointId,
    string $calculHour,
    ?string $startDate = null,
    ?string $endDate = null,
    ?string $per = null,
    bool $subDays = false): ?Measure {
        return $this->getConsummationMeasure($pointId, $calculHour, $startDate, $endDate, $per, $subDays, true);
    }

    private function dateRange(?string $startDate,
    ?string $endDate,
    ?string $per,
    string $calculHour,
    bool $subDays = false)
    {
        $WEEK = 'week';
        $MONTH = 'month';
        $YEAR = 'year';
        $DAY = 'day';
        list($hour, $minute) = explode(':', $calculHour);
        if ($per && is_null($startDate) && is_null($endDate)) {
            switch ($per) {
                case $WEEK:
                    if ($subDays) {
                        $startDate = Carbon::now()->subDays(7)->hour($hour)->minute($minute)->toDateTimeString();
                        $endDate = Carbon::now()->hour($hour)->minute($minute)->toDateTimeString();
                    } else {
                        $startDate = Carbon::now()->startOfWeek()->hour($hour)->minute($minute)->toDateTimeString();
                        $endDate = Carbon::now()->endOfWeek()->addDay()->hour($hour)->minute($minute)->toDateTimeString();
                    }
                    break;
                case $MONTH:
                    $startDate = Carbon::now()->startOfMonth()->hour($calculHour)->toDateTimeString();
                    $endDate = Carbon::now()->endOfMonth()->addDay()->hour($hour)->minute($minute)->toDateTimeString();
                    break;
                case $YEAR:
                    $startDate = Carbon::now()->startOfYear()->hour($calculHour)->toDateTimeString();
                    $endDate = Carbon::now()->endOfYear()->addDay()->hour($hour)->minute($minute)->toDateTimeString();
                    break;
                case $DAY:
                    $startDate = Carbon::now()->hour($calculHour)->minute(0)->second(0)->toDateTimeString();
                    $endDate = Carbon::parse($startDate)->addDay()->hour($hour)->minute($minute)->toDateTimeString();
                    break;
            }
        }
        else{
            $startDate = Carbon::parse($startDate)->hour($hour)->minute($minute)->second(0)->toDateTimeString();
            $endDate = Carbon::parse($endDate)->hour($hour)->minute($minute)->second(0)->toDateTimeString();
        }

        return [$startDate, $endDate];
    }

    private function getMeasurements(
        int $pointId,
        string $calculHour,
        ?string $startDate = null,
        ?string $endDate = null,
        ?string $per = 'month',
        string $orderBy = 'asc',
        string $aggregate = 'MIN',
        bool $energyCompteur = false
    ): ?Collection {
        $ENERGY = 'Energy';
        $MEASUREMENTS_CREATED_AT = 'measurements.created_at';
        $MEASUREMENTS_ID = 'measurements.id';
        $featureName = $energyCompteur ? 'energyCompteur' : $ENERGY;
        list($hour, $minute) = explode(':', $calculHour);
        $query = Measure::query()
        ->where(Measure::POINT_ID_COLUMN, $pointId)
        ->whereNull('measurements.deleted_at')
        ->whereHas('featureMeasurement.feature', function ($featureQuery) use ($featureName) {
            $featureQuery->where('name', $featureName);
        });
        // $query = Measure::where(Measure::POINT_ID_COLUMN, $pointId)
        //     ->whereNull('measurements.deleted_at')
        //     ->whereHas('features', function ($featureMeasurementQuery) use ($ENERGY) {
        //             $featureMeasurementQuery->where('name', $ENERGY);
        //     });
        if ($per) {
            switch ($per) {
                case 'week':
                    $startDate = Carbon::now()->startOfYear()->hour($hour)->minute($minute)->second(0)->toDateTimeString();
                    $endDate = Carbon::now()->addDay()->hour($hour)->minute($minute)->second(0)->toDateTimeString();
                    break;
                case 'month':
                    $startDate = Carbon::now()->startOfYear()->hour($hour)->minute($minute)->second(0)->toDateTimeString();
                    $endDate = Carbon::now()->endOfYear()->addDay()->hour($hour)->minute($minute)->second(0)->toDateTimeString();
                    break;
                case 'year':
                    break;
                case 'day':
                    $startDate = Carbon::now()->startOfMonth()->hour($hour)->minute($minute)->second(0)->toDateTimeString();
                    $endDate = Carbon::now()->addDay()->hour($hour)->minute($minute)->second(0)->toDateTimeString();
                    break;
                default:
                    break;
            }
        }
       // $endDate = Carbon::parse($endDate)->setTime(23, 59, 59);
        if ($startDate && $endDate) {
            if ($startDate === $endDate) {
                $query->whereDate($MEASUREMENTS_CREATED_AT, $startDate);
            } else {
                $query->whereBetween($MEASUREMENTS_CREATED_AT, [Carbon::parse($startDate), Carbon::parse($endDate)]);
            }
        }
        $query->whereTime($MEASUREMENTS_CREATED_AT, '>=', Carbon::now()->setTime($hour, $minute, 0)->toTimeString());
        if (in_array($per, ['year', 'month', 'day', 'week'])) {
            $subquery = clone $query;
            if ($per === 'month') {
                $subquery->selectRaw($aggregate . '(' . $MEASUREMENTS_ID . ') as first_measurement_id')
                    ->groupBy(DB::raw('YEAR(' . $MEASUREMENTS_CREATED_AT . ')'),
                        DB::raw('MONTH(' . $MEASUREMENTS_CREATED_AT . ')'));
            } elseif ($per === 'day') {
                $subquery->selectRaw($aggregate . '(' . $MEASUREMENTS_ID . ') as first_measurement_id')
                    ->groupBy(DB::raw('DATE(' . $MEASUREMENTS_CREATED_AT . ')'));
            } elseif ($per === 'week') {
                $subquery->selectRaw($aggregate . '(' . $MEASUREMENTS_ID . ') as first_measurement_id')
                    ->groupBy(DB::raw('WEEK(' . $MEASUREMENTS_CREATED_AT . ', 1)'),
                        DB::raw('YEAR(' . $MEASUREMENTS_CREATED_AT . ')'), 'measurements.point_id');
                        $subquery->whereTime($MEASUREMENTS_CREATED_AT, '>=', Carbon::now()->setTime($hour, $minute, 0)->toTimeString());
            } elseif($per === 'year')
            {
                $subquery->selectRaw($aggregate . '(' . $MEASUREMENTS_ID . ') as first_measurement_id')
                ->groupBy(DB::raw('YEAR(' . $MEASUREMENTS_CREATED_AT . ')'));
            }
            $query->joinSub($subquery, 'first_measurements', function ($join) {
                $join->on('measurements.id', '=', 'first_measurements.first_measurement_id');
            });
        }
        return $query->orderBy($MEASUREMENTS_CREATED_AT, $orderBy)->get();
    }

    private function getConsummationMeasure(
        int $pointId,
        string $calculHour,
        ?string $startDate = null,
        ?string $endDate = null,
        ?string $per = null,
        bool $subDays = false,
        bool $isLast = false
    ): ?Measure {
        $ENERGY = 'Energy';
        $MEASUREMENTS_CREATED_AT = 'measurements.created_at';
        
        $query = Measure::query()
            ->select('measurements.*', 'feature_measurement.value as feature_value')
            ->join('feature_measurement', 'feature_measurement.measure_id', 'measurements.id') 
            ->join('features', 'features.id', 'feature_measurement.feature_id')
            ->where('measurements.' . Measure::POINT_ID_COLUMN, $pointId)
            ->whereNull('measurements.deleted_at')
            ->where('features.name', $ENERGY);
    
        list($startDate, $endDate) = $this->dateRange($startDate, $endDate, $per, $calculHour, $subDays);
    
        if (!isset($per)) {
            $startDate = Carbon::now()->startOfMonth()->hour($calculHour)->toDateTimeString();
            $endDate = Carbon::now()->toDateTimeString();
        }
    
        if ($startDate && $endDate) {
            if ($startDate === $endDate) {
                $query->whereDate($MEASUREMENTS_CREATED_AT, $startDate);
            } else {
                $query->whereBetween($MEASUREMENTS_CREATED_AT, [Carbon::parse($startDate), Carbon::parse($endDate)]);
            }
        }
    
        $orderByDirection = $isLast ? 'desc' : 'asc';
        $query->orderBy($MEASUREMENTS_CREATED_AT, $orderByDirection);
    
        return $query->first();
    }
    
    public function createForGenericAsset(int $gAssetId,int $entityId, Carbon $measure_created_at): ?Measure
    {
        return Measure::create(
            [
                "point_id" => null,
                "generic_asset_id" => $gAssetId,
                "status" => 0,
                "is_generic" => 1,
                "entity_id" => $entityId,
                "created_at" => $measure_created_at,
                "updated_at" => $measure_created_at
            ]
        );
    }

    public function createForEntity(int $entityId, Carbon $measure_created_at): ?Measure
    {
        return Measure::create(
            [
                "point_id" => null,
                "generic_asset_id" => null,
                "status" => 0,
                "is_generic" => 1,
                "entity_id" => $entityId,
                "created_at" => $measure_created_at,
                "updated_at" => $measure_created_at
            ]
        );
    }

    public function getGenericAssetsRunningMeasurements(array $genericAssetId, string $calculHour, ?string $startDate = null, ?string $endDate = null): ?Collection
    {
        $RUNNING = FeatureMeasure::RUNNING;
        list($hour, $minute) = explode(':', $calculHour);
        $query = Measure::whereIn('measurements.generic_asset_id', $genericAssetId)
            ->whereNull('measurements.point_id')
            ->whereNotNull('measurements.entity_id')
            ->select('generic_assets.energy_type as type', 'measurements.id', 'measurements.created_at as date', 'feature_measurement.value', 'generic_assets.name', 'features.unit')
            ->join('generic_assets', 'generic_assets.id', 'measurements.generic_asset_id')
            ->join('feature_measurement', 'feature_measurement.measure_id', 'measurements.id')
            ->join('features', 'features.id', '=', 'feature_measurement.feature_id')
            ->where('features.name', $RUNNING);

        if ($startDate && $endDate) {
            $startDateTime = Carbon::parse($startDate)->hour($hour)->minute($minute)->second(0);
            $endDateTime = Carbon::parse($endDate)->setTime(23, 59, 59);

            $query->where(function ($query) use ($startDateTime, $endDateTime) {
                $query->whereDate('measurements.created_at', $startDateTime)
                    ->orWhereBetween('measurements.created_at', [$startDateTime, $endDateTime]);
            });
        } else {
            $startDateTime = Carbon::now()->hour($hour)->minute($minute)->second(0);
            $query->where('measurements.created_at', '>=', $startDateTime);
        }

        return $query->get();
    }


    public function isTarifMeasureExist(int $genericAssetId, string $startHour, string $finHour) : bool
    {
        list($dHour, $dMinute, $dSecond) = explode(':', $startHour);
        list($fHour, $fMinute, $fSecond) = explode(':', $finHour);
        $isGreater = $finHour > $startHour;
        if($isGreater)
        {
            $startDate = Carbon::now()->hour($dHour)->minute($dMinute)->second($dSecond)->toDateTimeString();
            $endDate = Carbon::now()->hour($fHour)->minute($fMinute)->second($fSecond)->toDateTimeString();
        }
        else{
            $startDate = Carbon::now()->subDay()->hour($fHour)->minute($fMinute)->second($fSecond)->toDateTimeString();
            $endDate = Carbon::now()->subDay()->hour($dHour)->minute($dMinute)->second($dSecond)->toDateTimeString();
        }
        return Measure::where(Measure::GENERIC_ASSET_ID_COLUMN, $genericAssetId)
            ->whereNull('measurements.deleted_at')
            ->whereBetween('created_at', [$startDate, $endDate])
            ->whereHas('featureMeasurement', function ($featureMeasurementQuery) {
                $featureMeasurementQuery->whereHas('feature', function ($featureQuery) {
                    $featureQuery->where('name', Features::TARIF);
                });
            })
            ->exists();
    }

    public function getFirstAndLastEnergyMeasures(int $genericAssetId, string $heureDebut, string $heureFin): ?array {
        $heureDebut = Carbon::parse($heureDebut);
        $heureFin = Carbon::parse($heureFin);
        $ENERGY = 'Energy';
    
        if ($heureDebut->greaterThan($heureFin)) {
            $heureDebut->subDay();
        }
    
        $query = Measure::where(Measure::GENERIC_ASSET_ID_COLUMN, $genericAssetId)
            ->where('created_at', '>=', $heureDebut)
            ->where('created_at', '<=', $heureFin)
            ->whereHas('featureMeasurement.feature', function ($featureQuery) use ($ENERGY) {
                $featureQuery->where('name', $ENERGY);
            });
    
        $firstMeasurement = clone $query;
        $lastMeasurement = clone $query;
    
        $firstMeasurement = $firstMeasurement->orderBy('created_at', 'asc')->first();
        $lastMeasurement = $lastMeasurement->orderBy('created_at', 'desc')->first();
        return [
            'first_energy_measure' => $firstMeasurement,
            'last_energy_measure' => $lastMeasurement,
        ];
    }
    

    public function getGenericAssetMeasurementsOfEachDay(
        array $gAssetsIds,
        string $calculHour,
        ?string $startDate = null,
        ?string $endDate = null
    ): ?array {
        $hour = Carbon::now()->setTimeFromTimeString($calculHour);
        $formatedHour = $hour->format('H:i:s');
        $startDate = $startDate ?? now()->subDays(7);
        $endDate = $endDate ?? now()->toDateString();
        $results = DB::table('generic_assets AS ga')
                    ->select('FM.measurement_date' ,'FM.first_measurement_value', 'LM.last_measurement_value')
                    ->selectRaw('(LM.last_measurement_value - FM.first_measurement_value) AS measurement_difference')
                    ->from(function ($query) use ($gAssetsIds, $startDate, $endDate, $formatedHour) {
                        $query->select('ga.id AS asset_id', 'ga.name AS asset_name', DB::raw('DATE(m1.created_at) AS measurement_date'))
                            ->selectRaw('MIN(m1.created_at) AS first_measurement_time, MIN(fm.value) AS first_measurement_value')
                            ->from('generic_assets AS ga')
                            ->leftJoin('measurement_points AS mp', 'mp.generic_asset_id', '=', 'ga.id')
                            ->leftJoin('measurements AS m1', 'mp.id', '=', 'm1.point_id')
                            ->leftJoin('feature_measurement AS fm', 'fm.measure_id', '=', 'm1.id')
                            ->whereIn('ga.id', $gAssetsIds)
                                ->where('fm.feature_id', 29)
                            ->where('ga.energy_type', 0)
                            ->whereRaw("TIME(m1.created_at) >= '$formatedHour'")
                            ->whereDate('m1.created_at', '>=', $startDate)
                            ->whereDate('m1.created_at', '<=', $endDate)
                            ->groupBy('asset_id', 'asset_name', 'measurement_date')
                            ->havingRaw('first_measurement_time IS NOT NULL');
                    }, 'FM')
                    ->joinSub(function ($query) use ($gAssetsIds, $startDate, $endDate) {
                        $query->select('ga.id AS asset_id', 'ga.name AS asset_name', DB::raw('DATE(m1.created_at) AS measurement_date'))
                            ->selectRaw('MAX(m1.created_at) AS last_measurement_time, MAX(m1.id) AS measure_id, MAX(fm.measure_id) AS fm_id')
                            ->selectRaw("SUBSTRING_INDEX(GROUP_CONCAT(fm.value ORDER BY m1.created_at DESC SEPARATOR '|'), '|', 1) AS last_measurement_value")
                            ->from('generic_assets AS ga')
                            ->leftJoin('measurement_points AS mp', 'mp.generic_asset_id', '=', 'ga.id')
                            ->leftJoin('measurements AS m1', 'mp.id', '=', 'm1.point_id')
                            ->leftJoin('feature_measurement AS fm', 'fm.measure_id', '=', 'm1.id')
                            ->whereIn('ga.id', $gAssetsIds)
                            ->where('fm.feature_id', 29)
                            ->where('ga.energy_type', 0)
                            ->whereDate('m1.created_at', '>=', $startDate)
                            ->whereDate('m1.created_at', '<=', $endDate)
                            ->groupBy('asset_id', 'asset_name', 'measurement_date')
                            ->havingRaw('last_measurement_time IS NOT NULL');
                    }, 'LM', 'FM.asset_id', '=', 'LM.asset_id')
                    ->whereColumn('FM.measurement_date', '=', 'LM.measurement_date')
                    ->orderByDesc('LM.last_measurement_time')
                    ->get();
                $sumByDate = $results->groupBy('measurement_date')->map(function ($group) {
                    return [
                        'date' => $group->first()->measurement_date,
                        'value' => $group->sum('measurement_difference'),
                    ];
                })->values()->toArray();
                return $sumByDate;
    }

    public function getLastMeasure(int $pointId) : ?object
    {
        return Measure::where(Measure::POINT_ID_COLUMN, $pointId)
            ->with('featureMeasurement')
            ->latest()
            ->first();
    }
}
