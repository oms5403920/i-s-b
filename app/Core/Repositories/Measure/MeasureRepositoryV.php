<?php

namespace App\Core\Repositories\Measure;

use App\Core\Models\FeatureMeasure\FeatureMeasure;
use App\Core\Models\Features\Features;
use App\Core\Models\Measure\Measure;
use App\Core\Repositories\Repository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class MeasureRepositoryV extends Repository
{





    
    public function firstConsummationMeasureForEachPoint(array $assetIds,
    int $calculHour,
    ?string $startDate = null,
    ?string $endDate = null,
    ?string $per = null): ?Measure {
        return $this->getConsummationMeasure($assetIds, $calculHour, $startDate, $endDate, $per);
    }
    
    public function lastConsummationMeasureForEachPoint(array $assetIds,
    int $calculHour,
    ?string $startDate = null,
    ?string $endDate = null,
    ?string $per = null): ?Measure {
        return $this->getConsummationMeasure($assetIds, $calculHour, $startDate, $endDate, $per, true);
    }

    private function dateRange(?string $startDate,
    ?string $endDate,
    ?string $per,
    int $calculHour)
    {
        $WEEK = 'week';
        $MONTH = 'month';
        $YEAR = 'year';
        $DAY = 'day';
        if ($per) {
            switch ($per) {
                case $WEEK:
                    $startDate = Carbon::now()->startOfWeek()->hour($calculHour)->toDateTimeString();
                    $endDate = Carbon::now()->endOfWeek()->toDateTimeString();
                    break;
                case $MONTH:
                    $startDate = Carbon::now()->startOfMonth()->hour($calculHour)->toDateTimeString();
                    $endDate = Carbon::now()->endOfMonth()->toDateTimeString();
                    break;
                case $YEAR:
                    $startDate = Carbon::now()->startOfYear()->hour($calculHour)->toDateTimeString();
                    $endDate = Carbon::now()->endOfYear()->toDateTimeString();
                    break;
                case $DAY:
                    $startDate = Carbon::now()->hour($calculHour)->minute(0)->second(0)->toDateTimeString();
                    $endDate = Carbon::parse($startDate)->addDay()->toDateTimeString();
                    break;
            }
        }

        return [$startDate, $endDate];
    }

    private function getConsummationMeasure(
        array $assetIds,
        int $calculHour,
        ?string $startDate = null,
        ?string $endDate = null,
        ?string $per = null,
        bool $isLast = false
    ): ?Measure {
        $ENERGY = 'Energy';
        $MEASUREMENTS_CREATED_AT = 'measurements.created_at';  
        $query = Measure::query()
        ->select('measurements.id as measure_id', 'measurement_points.id as point_id',
            'measurements.entity_id', 'measurement_points.generic_asset_id as asset_id',
            'generic_assets.energy_type as type')
            ->whereHas('featureMeasurement.feature', function ($featureQuery) use ($ENERGY) {
                $featureQuery->where('name', $ENERGY);
            })
            ->leftJoin('measurement_points', 'measurement_points.id', 'measurements.point_id')
            ->leftJoin('generic_assets', 'generic_assets.id', 'measurement_points.generic_asset_id')
            ->whereIn('generic_assets.id', $assetIds)
            ->whereNull('measurements.deleted_at');
    
        list($startDate, $endDate) = $this->dateRange($startDate, $endDate, $per, $calculHour);
    
        if (!isset($per)) {
            $startDate = Carbon::now()->startOfMonth()->hour($calculHour)->toDateTimeString();
            $endDate = Carbon::now()->toDateTimeString();
        }
    
        if ($startDate && $endDate) {
            if ($startDate === $endDate) {
                $query->whereDate($MEASUREMENTS_CREATED_AT, $startDate);
            } else {
                $query->whereBetween($MEASUREMENTS_CREATED_AT, [Carbon::parse($startDate), Carbon::parse($endDate)]);
            }
        }
        
        $orderByDirection = $isLast ? 'desc' : 'asc';
        $query->orderBy('measurements.created_at', $orderByDirection);
    
        return $query->first();
    }
        


    
}
