<?php

namespace App\Core\Repositories\EntityUser;

use App\Core\Repositories\Repository;
use App\Core\Models\EntityUser\EntityUser;
use Illuminate\Database\Eloquent\Collection;

class EntityUserRepository extends Repository
{
    public function getByUserId(int $userId): ?Collection
    {
        return EntityUser::where(EntityUser::USER_ID_COLUMN, $userId)->get();
    }
}
