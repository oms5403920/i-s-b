<?php

namespace App\Core\Repositories\Entity;

use App\Core\Models\Entity\Entity;
use App\Core\Models\Measure\Measure;
use App\Core\Models\User\User;
use App\Core\Repositories\Repository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class EntityRepository extends Repository
{
    public function getById(int $id): ?Entity
    {
        return Entity::where(Entity::ID_COLUMN, $id)->select('id', 'name', 'ref', 'parent_id')
            ->whereNull('deleted_at')->first();
    }

    public function getAllByIds(array $ids): ?Collection
    {
        return Entity::whereIn(Entity::ID_COLUMN, $ids)->select('id', 'name', 'ref')
            ->get();
    }

    public function getSubEntitiesById(int $id): ?Collection
    {
        return Entity::where(Entity::PARENT_ID_COLUMN, $id)->whereNull('deleted_at')->get();
    }

    public function update(string $id, array $attributes): bool
    {
        $attributes = Arr::only(
            $attributes,
            [
                Entity::NAME_COLUMN,
                Entity::REF_COLUMN,
                Entity::LONGITUDE_COLUMN,
                Entity::LATITUDE_COLUMN,
                Entity::TYPE_COLUMN,
                Entity::PARENT_ID_COLUMN,
                Entity::ENV_ID_COLUMN,
                Entity::SHAPE_COLUMN,
                Entity::LAST_MEASURE_CREATED_AT,
            ]
        );

        return Entity::query()
                ->where(Entity::ID_COLUMN, $id)
                ->update($attributes) > 0;
    }

    public function LastMeasure(int $entityId) : ?Measure
    {
        return  Measure::where('entity_id', $entityId)
            ->where('point_id',null)
            ->where('generic_asset_id',null)
            ->whereHas('features', function ($query) {
                $query->where('name', 'runningTime');
            })
            ->orderByDesc('created_at')
            ->first();
    }

    public function getLastRunningTimeEntity(int $entityId) : ?float
    {
        $lastRunningTime = Measure::where('entity_id', $entityId)
            ->where('point_id',null)
            ->where('generic_asset_id',null)
            ->whereHas('features', function ($query) {
                $query->where('name', 'runningTime');
            })
            ->with(['features' => function ($query) {
                $query->where('name', 'runningTime')->latest();
            }])
            ->orderByDesc('created_at')
            ->first();

        if ($lastRunningTime && $lastRunningTime->features->isNotEmpty()) {
            return $lastRunningTime->features->first()->pivot->value;
        }

        return null;
    }

    public function getTopLevelParent(int $entityId, ?int $parentEntityId = null): ?Entity
    {
        $entity = Entity::find($entityId);
        if ($entity !== null) {
            if ($parentEntityId !== null) {
                $parent = $entity;
                while ($parent->parent_id !== null && $parent->id !== $parentEntityId) {
                    $previousParent = $parent;
                    $parent = Entity::find($parent->parent_id);
                    if ($previousParent->getParentId() === $parentEntityId) {
                        return $previousParent;
                    }
                }

                return null;
            } else {
                $parent = $entity;
                while ($parent->parent_id !== null) {
                    $parent = Entity::find($parent->parent_id);
                }
                return $parent;
            }
        }
    }


public function getChildIds(int $entityId) : ?array
{
    return DB::table('entities')
        ->select(Entity::ID_COLUMN)
        ->where(Entity::PARENT_ID_COLUMN, $entityId)
        ->get()
        ->pluck(Entity::ID_COLUMN)
        ->toArray();
}

    public function getUserTree(User $user) : array
    {
        $result = Entity::with([
            'genericAssets',
            'genericAssets.point',
            'children.genericAssets',
            'children.genericAssets.point',
            'assets',
            'assets.children',
            'assets.children.points',
            'children',
            'children.assets',
            'children.assets.children',
            'children.assets.children.points'
        ])->get();

        $result->transform(function ($entity) {
            $entity->setAttribute('e_id', $entity->id);
            if ($entity->children) {
                $entity->children->each(function ($child) {
                    if ($child->assets) {
                        $child->assets->each(function ($asset) {
                            $asset->setAttribute('a_id', $asset->id);
                            if ($asset->children) {
                                $asset->children->each(function ($sub_asset) {
                                    $sub_asset->setAttribute('sa_id', $sub_asset->id);
                                    if ($sub_asset->points) {
                                        $sub_asset->points->each(function ($point) {
                                            $point->setAttribute('point_id', $point->id);
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
            return $entity;
        });
        return $result->toArray();
    }
}
