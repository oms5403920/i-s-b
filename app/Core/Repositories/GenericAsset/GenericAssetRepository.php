<?php

namespace App\Core\Repositories\GenericAsset;

use App\Core\Models\Entity\Entity;
use App\Core\Models\Family\Family;
use App\Core\Models\FeatureMeasure\FeatureMeasure;
use App\Core\Models\Features\Features;
use App\Core\Models\GenericAsset\GenericAsset;
use App\Core\Models\Group\Group;
use App\Core\Models\Measure\Measure;
use App\Core\Repositories\Repository;
use App\Core\Utilities\DateRangeUtility;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class GenericAssetRepository extends Repository
{
    public function getAllByEntityId(int $entityId): ?Collection
    {
        return GenericAsset::where(GenericAsset::ENTITY_ID_COLUMN, $entityId)
            ->whereNull('deleted_at')
            ->with(['points' => function ($query) {
                $query->select('id');
            }])->get();
    }

    public function getById(int $id): ?GenericAsset
    {
        return GenericAsset::where(GenericAsset::ID_COLUMN, $id)
            ->whereNull('deleted_at')->first();
    }

    public function update(string $id, array $attributes): bool
    {
        $attributes = Arr::only(
            $attributes,
            [
                GenericAsset::NAME_COLUMN,
                GenericAsset::REF_COLUMN,
                GenericAsset::ENTITY_ID_COLUMN,
                GenericAsset::FAMILY_ID_COLUMN,
                GenericAsset::DIAGRAM_ID_COLUMN,
                GenericAsset::GROUP_ID_COLUMN,
                GenericAsset::PHOTO_COLUMN,
                GenericAsset::CLASS_COLUMN,
                GenericAsset::TYPE_COLUMN,
                GenericAsset::ENERGY_TYPE_COLUMN,
                GenericAsset::STATUS_COLUMN,
                GenericAsset::LONGITUDE_COLUMN,
                GenericAsset::LATITUDE_COLUMN,
                GenericAsset::LAST_MEASURE_ID,
                GenericAsset::LAST_MEASURE_CREATED_AT,
                GenericAsset::WORKING_HOURS
            ]
        );

        return GenericAsset::query()
                ->where(GenericAsset::ID_COLUMN, $id)
                ->update($attributes) > 0;
    }

    public function getLastRunningTimeAsset(int $gAssetId) : ?float
    {
        $lastRunningTime = Measure::where('generic_asset_id', $gAssetId)
            ->where('point_id',null)
            ->whereNotNull('entity_id')
            ->whereHas('features', function ($query) {
                $query->where('name', 'runningTime');
            })
            ->with(['features' => function ($query) {
                $query->where('name', 'runningTime')->latest();
            }])
            ->orderByDesc('created_at')
            ->first();

        if ($lastRunningTime && $lastRunningTime->features->isNotEmpty()) {
            return $lastRunningTime->features->first()->pivot->value;
        }

        return null;
    }

    public function checkRunningStatus(array $genericAssetIds) : ?bool
    {
        $lastMeasures = Measure::whereIn('id', function ($query) use ($genericAssetIds) {
                $query->selectRaw('MAX(id)')
                    ->from('measurements')
                    ->whereIn('generic_asset_id', $genericAssetIds)
                    ->whereNull('point_id')
                    ->groupBy('generic_asset_id');
            })
            ->get();
        $featureRunning =  Features::where(Features::NAME_COLUMN,'Running')->first();

        if($featureRunning){
            $state_of_lastMeasures = [];
            foreach ($lastMeasures as $lastMeasure) {
                $featureMeasureRunning = FeatureMeasure::where(FeatureMeasure::MEASURE_ID_COLUMN,$lastMeasure->id)->where(FeatureMeasure::FEATURE_ID_COLUMN,$featureRunning->id)->first();

                $value = $featureMeasureRunning->getValue();

                $state_of_lastMeasures[] = intval($value);
            }

            if (in_array(1, $state_of_lastMeasures, false)) {
                return false;
            }else{
                return true;
            }
        }
    }

    public function LastMeasure(int $gAssetId) : ?Measure
    {
        return  Measure::where('generic_asset_id', $gAssetId)
            ->where('point_id',null)
            ->whereNotNull('entity_id')
            ->whereHas('features', function ($query) {
                $query->where('name', 'runningTime');
            })
            ->orderByDesc('created_at')
            ->first();
    }

    public function getAllByEntityIdAndEnergyType(int $entityId, int $energyType): ?Collection
    {
        return GenericAsset::where(GenericAsset::ENTITY_ID_COLUMN, $entityId)
            ->where('energy_type', $energyType)
            ->whereNull('deleted_at')
            ->select(
                GenericAsset::ID_COLUMN,
                GenericAsset::NAME_COLUMN,
                GenericAsset::REF_COLUMN,
                GenericAsset::ENTITY_ID_COLUMN,
                GenericAsset::ENERGY_TYPE_COLUMN
            )
            ->get();
    }

    public function getByIds(array $ids): Collection
    {
        return GenericAsset::whereIn('id', $ids)->get();
    }

    public function getAllByEntityIdWithConsummation(
        int $entityId,
        string $calculHour,
        ?string $startDate,
        ?string $endDate,
        ?string $per
    ): ?Collection {
        $genericAssets = GenericAsset::where(GenericAsset::ENTITY_ID_COLUMN, $entityId)
            ->whereNull('deleted_at')
            ->select(
                GenericAsset::ID_COLUMN,
                GenericAsset::NAME_COLUMN,
                GenericAsset::ENTITY_ID_COLUMN,
                GenericAsset::ENERGY_TYPE_COLUMN
            )->get();

        $genericAssets->each(function ($genericAsset) use ($calculHour, $startDate, $endDate, $per) {
            $consummationMeasures = [];
            $genericAsset->points->each(function ($point) use ($calculHour, $startDate, $endDate, $per, &$consummationMeasures) {
                $firstConsummationMeasure = $point->getConsummationMeasure(
                    $calculHour,
                    $startDate,
                    $endDate,
                    $per,
                    false
                );

                $lastConsummationMeasure = $point->getConsummationMeasure(
                    $calculHour,
                    $startDate,
                    $endDate,
                    $per,
                    true
                );

                $productionMeasure = $point->getProductionMeasure(
                    $calculHour,
                    $startDate,
                    $endDate,
                    $per,
                    true
                );
                $consummationMeasures[] = [
                    'firstConsummationMeasure' => $firstConsummationMeasure,
                    'lastConsummationMeasure' => $lastConsummationMeasure,
                    'productionMeasure' => $productionMeasure
                ];
            });
            $genericAsset->consummationMeasures = $consummationMeasures;
        });

        return $genericAssets;
    }

    public function getAllByEntityIds(array $entityId): ?Collection
    {
        return GenericAsset::whereIn(GenericAsset::ENTITY_ID_COLUMN, $entityId)
            ->whereNull('deleted_at')
            ->select(
                GenericAsset::ID_COLUMN,
                GenericAsset::NAME_COLUMN,
                GenericAsset::ENTITY_ID_COLUMN,
                GenericAsset::ENERGY_TYPE_COLUMN
            )->get();
    }

    public function getFirstAndLastConsumptionMeasures(array $entityIds,
    string $calculHour,
    ?string $startDate = null,
    ?string $endDate = null,
    ?string $per = null) : ?array
    {
        $hour = Carbon::now()->setTimeFromTimeString($calculHour);
        $formatedHour = $hour->format('H:i:s');
        if ($startDate === null || $endDate === null) {
            list($startDate, $endDate) = DateRangeUtility::calculateRange($startDate, $endDate, $per, $calculHour);
        }
        $results = DB::table(function ($subquery) use ($formatedHour, $startDate, $endDate, $entityIds) {
            $subquery
                ->select('ga.id as g_asset_id', DB::raw('MIN(m.created_at) as first_measure_date'), DB::raw('MIN(fm.value) as first_measure_value'), 'ga.energy_type as energyType', 'ga.entity_id as entity')
                ->from('generic_assets as ga')
                ->leftJoin('measurement_points as mp', 'mp.generic_asset_id', 'ga.id')
                ->leftJoin('measurements as m', 'm.point_id', 'mp.id')
                ->leftJoin('feature_measurement as fm', 'fm.measure_id', 'm.id')
                ->whereIn('ga.entity_id', $entityIds)
                ->where('ga.energy_type', 0)
                ->where('fm.feature_id', 29)
                ->whereBetween('m.created_at', [$startDate, $endDate])
                ->whereTime('m.created_at', '>=', $formatedHour)
                ->groupBy('ga.id');
        }, 'm1')
        ->leftJoin(DB::raw('(
            SELECT
                ga.id as g_asset_id,
                MAX(m.created_at) as last_measure_date,
                MAX(fm.value) as last_measure_value,
                ga.energy_type as energyType,
                ga.entity_id as entity
            FROM generic_assets as ga
            LEFT JOIN measurement_points as mp ON mp.generic_asset_id = ga.id
            LEFT JOIN measurements as m ON m.point_id = mp.id
            LEFT JOIN feature_measurement as fm ON fm.measure_id = m.id
            WHERE ga.entity_id IN (' . implode(',', $entityIds) . ')
            AND ga.energy_type = 0
            AND fm.feature_id = 29
            AND m.created_at BETWEEN "' . $startDate . '" AND "' . $endDate . '"
            GROUP BY ga.id
        ) as m2'), 'm1.g_asset_id', 'm2.g_asset_id')
        ->select('m1.first_measure_value', 'm2.last_measure_value', 'm1.first_measure_date', 'm2.last_measure_date', 'm1.g_asset_id', DB::raw('(m2.last_measure_value - m1.first_measure_value) as consumedEnergy'), 'm1.entity')
        ->orderBy('m1.g_asset_id')
        ->get();
        $resultArray = $results->toArray();
        $resultEntityIds = array_column($resultArray, 'entity');
        $notExistedEntities = array_diff($entityIds, $resultEntityIds);
        $newEntities = [];
        foreach ($notExistedEntities as $entity) {
            $newEntity = (object) [
                'first_measure_value' => 0.0,
                'last_measure_value' => 0.0,
                'first_measure_date' => null,
                'last_measure_date' => null,
                'g_asset_id' => null,
                'consumedEnergy' => 0.0,
                'entity' => $entity,
            ];
            $newEntities[] = $newEntity;
        }
        $finalResultArray = array_merge($resultArray, $newEntities);

        return $finalResultArray;
    }

    public function getFirstAndLastProducedMeasures(array $entityIds,
    string $calculHour,
    ?string $startDate = null,
    ?string $endDate = null,
    ?string $per = null) : ?array
    {
        $hour = Carbon::now()->setTimeFromTimeString($calculHour);
        $formatedHour = $hour->format('H:i:s');
        if ($startDate === null || $endDate === null) {
            list($startDate, $endDate) = DateRangeUtility::calculateRange($startDate, $endDate, $per, $calculHour);
        }
        $results = DB::table(function ($subquery) use ($formatedHour, $startDate, $endDate, $entityIds) {
            $subquery
                ->select('ga.id as g_asset_id', DB::raw('MIN(m.created_at) as first_measure_date'), DB::raw('MIN(fm.value) as first_measure_value'), 'ga.energy_type as energyType', 'ga.entity_id as entity')
                ->from('generic_assets as ga')
                ->leftJoin('measurement_points as mp', 'mp.generic_asset_id', '=', 'ga.id')
                ->leftJoin('measurements as m', 'm.point_id', '=', 'mp.id')
                ->leftJoin('feature_measurement as fm', 'fm.measure_id', '=', 'm.id')
                ->whereIn('ga.entity_id', $entityIds)
                ->where('ga.energy_type', 1)
                ->where('fm.feature_id', 29)
                ->whereBetween('m.created_at', [$startDate, $endDate])
                ->whereTime('m.created_at', '>=', $formatedHour)
                ->groupBy('ga.id');
        }, 'm1')
        ->leftJoin(DB::raw('(
            SELECT
                ga.id as g_asset_id,
                MAX(m.created_at) as last_measure_date,
                MAX(fm.value) as last_measure_value,
                ga.energy_type as energyType,
                ga.entity_id as entity
            FROM generic_assets as ga
            LEFT JOIN measurement_points as mp ON mp.generic_asset_id = ga.id
            LEFT JOIN measurements as m ON m.point_id = mp.id
            LEFT JOIN feature_measurement as fm ON fm.measure_id = m.id
            WHERE ga.entity_id IN (' . implode(',', $entityIds) . ')
            AND ga.energy_type = 1
            AND fm.feature_id = 29
            AND m.created_at BETWEEN "' . $startDate . '" AND "' . $endDate . '"
            AND TIME(m.created_at) <= "'. $formatedHour . '"
            GROUP BY ga.id
        ) as m2'), 'm1.g_asset_id', '=', 'm2.g_asset_id')
        ->select('m1.first_measure_value', 'm2.last_measure_value', 'm1.first_measure_date', 'm2.last_measure_date', 'm1.g_asset_id', DB::raw('(m2.last_measure_value - m1.first_measure_value) as consumedEnergy'), 'm1.entity')
        ->orderBy('m1.g_asset_id')
        ->get();

        return $results->toArray();
    }



    public function getAllByEntityIdsWithConsummation(
        array $entityIds,
        string $calculHour,
        ?string $startDate,
        ?string $endDate,
        ?string $per
    ): ?Collection {
        $genericAssets = GenericAsset::whereIn(GenericAsset::ENTITY_ID_COLUMN, $entityIds)
            ->whereNull('deleted_at')
            ->select(
                GenericAsset::ID_COLUMN,
                GenericAsset::NAME_COLUMN,
                GenericAsset::ENTITY_ID_COLUMN,
                GenericAsset::ENERGY_TYPE_COLUMN
            )->get();
            $genericAssets->each(function ($genericAsset) use ($calculHour, $startDate, $endDate, $per) {
                $consummationMeasures = [];
                $point = $genericAsset->point;

                $firstConsummationMeasure = $point->getConsummationMeasure(
                    $calculHour,
                    $startDate,
                    $endDate,
                    $per,
                    false
                );

                $lastConsummationMeasure = $point->getConsummationMeasure(
                    $calculHour,
                    $startDate,
                    $endDate,
                    $per,
                    true
                );

                $productionMeasure = $point->getProductionMeasure(
                    $calculHour,
                    $startDate,
                    $endDate,
                    $per,
                    true
                );

                $consummationMeasures[] = [
                    'firstConsummationMeasure' => $firstConsummationMeasure,
                    'lastConsummationMeasure' => $lastConsummationMeasure,
                    'productionMeasure' => $productionMeasure
                ];

                $genericAsset->consummationMeasures = $consummationMeasures;
            });

            return $genericAssets;
    }

    public function create(array $attributes) : GenericAsset
    {
        return GenericAsset::create([
            GenericAsset::NAME_COLUMN => Arr::get($attributes, GenericAsset::NAME_COLUMN),
            GenericAsset::REF_COLUMN => Arr::get($attributes, GenericAsset::REF_COLUMN),
            GenericAsset::ENTITY_ID_COLUMN => Arr::get($attributes, GenericAsset::ENTITY_ID_COLUMN),
            GenericAsset::FAMILY_ID_COLUMN => Arr::get($attributes, GenericAsset::FAMILY_ID_COLUMN),
            GenericAsset::GROUP_ID_COLUMN => Arr::get($attributes, GenericAsset::GROUP_ID_COLUMN),
            GenericAsset::DIAGRAM_ID_COLUMN => Arr::get($attributes, GenericAsset::DIAGRAM_ID_COLUMN),
            GenericAsset::CLASS_COLUMN => Arr::get($attributes, GenericAsset::CLASS_COLUMN),
            GenericAsset::ENERGY_TYPE_COLUMN => Arr::get($attributes, GenericAsset::ENERGY_TYPE_COLUMN),
            GenericAsset::TYPE_COLUMN => Arr::get($attributes, GenericAsset::TYPE_COLUMN),
            GenericAsset::LONGITUDE_COLUMN => Arr::get($attributes, GenericAsset::LONGITUDE_COLUMN),
            GenericAsset::LATITUDE_COLUMN => Arr::get($attributes, GenericAsset::LATITUDE_COLUMN),
            GenericAsset::PHOTO_COLUMN => Arr::get($attributes, GenericAsset::PHOTO_COLUMN),
        ]);
    }

    public function getRelatedData() : array
    {
        $entities = Entity::select('id', 'name')->get();
        $families = Family::select('id', 'name')->get();
        $groups = Group::select('id', 'name')->get();

        return [
            'entities' => $entities,
            'families' => $families,
            'groups' => $groups,
        ];
    }

    public function getAll() : ?Collection
    {
        return GenericAsset::whereNull('deleted_at')
            ->get();
    }
}
