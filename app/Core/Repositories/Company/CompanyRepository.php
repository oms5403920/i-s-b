<?php

namespace App\Core\Repositories\Company;

use App\Core\Models\Company\Company;
use App\Core\Repositories\Repository;

class CompanyRepository extends Repository
{
    public function findById(int $id) : ?Company
    {
        return Company::where('id', $id)->whereNull('deleted_at')->first();
    }
}
