<?php

namespace App\Core\Repositories\FeatureMeasure;

use App\Core\Models\FeatureMeasure\FeatureMeasure;
use App\Core\Repositories\Repository;
use App\Core\Utilities\DateRangeUtility;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

class FeatureMeasureRepository extends Repository
{
    public function getValueByFeature(int $measureId, string $featureName): ?FeatureMeasure
    {
        return FeatureMeasure::where('feature_measurement.measure_id', $measureId)
            ->join('features', 'features.id', 'feature_measurement.feature_id')
            ->where('features.name', $featureName)
            ->first();
    }

    public function getByEntityId(int $entityId, string $per, string $calculHour): ?FeatureMeasure
    {
        $startDate = null;
        $endDate = null;
        list($startDate, $endDate) = DateRangeUtility::calculateRange($startDate, $endDate, $per, $calculHour);
        $query = FeatureMeasure::whereHas('measurement', function ($query) use ($entityId, $startDate, $endDate) {
            $query->where('entity_id', $entityId)
                ->whereBetween('created_at', [$startDate, $endDate]);
        })
        ->whereHas('feature', function ($query) {
            $query->where('name', FeatureMeasure::PRODUCTION);
        });
        return $query->latest()->first();
    }

    public function create(int $measureId, float $featureValue, int $featureId,Carbon $measure_created_at): void
    {
        FeatureMeasure::create(
            [
                "value" => $featureValue,
                "feature_id" => $featureId,
                "measure_id" => $measureId,
                "created_at" => $measure_created_at,
                "updated_at" => $measure_created_at,
            ]
        );
    }

    public function getFeatureMeasure(int $measureId, int $featureId): ?FeatureMeasure
    {
        return FeatureMeasure::where(FeatureMeasure::MEASURE_ID_COLUMN, $measureId)
            ->where(FeatureMeasure::FEATURE_ID_COLUMN, $featureId)
            ->first();
    }

    public function getWorkingHourOfGnericAssets(array $genericAssetIds, int $featureId, string $calculHour, string $per): ?array
    {
        list($hour, $minute) = explode(':', $calculHour);
        $startDate = Carbon::now();
        $endDate = Carbon::now()->hour($hour)->minute($minute);
        switch ($per) {
            case 'day':
                $now = Carbon::now();
                $startDateValue = $now->copy()->hour($hour)->minute($minute)->second(0);
                if ($now->hour < $hour || ($now->hour == $hour && $now->minute < $minute)) {
                    $startDateValue->subDay();
                }
                $startDate = $startDateValue->toDateTimeString();
                $endDate = $startDateValue->copy()->addDay()->toDateTimeString();
                break;
    
            case 'week':
                $startDate->startOfWeek()->hour($hour)->minute($minute)->toDateTimeString();
                $endDate->endOfWeek()->hour($hour)->minute($minute)->toDateTimeString();
                break;
    
            case 'month':
                $startDate->startOfMonth()->hour($hour)->minute($minute)->toDateTimeString();
                $endDate->endOfMonth()->addDay(1)->hour($hour)->minute($minute)->toDateTimeString();
                break;
    
            case 'year':
                $startDate->startOfYear()->hour($hour)->minute($minute)->toDateTimeString();
                $endDate->endOfYear()->addDay(1)->hour($hour)->minute($minute)->toDateTimeString();
                break;
        } 
        $maxQuery = FeatureMeasure::whereIn('measure_id', function ($query) use ($genericAssetIds, $featureId, $startDate, $endDate) {
            $query->select('m.id')
            ->from('measurements as m')
            ->whereIn('m.generic_asset_id', $genericAssetIds)
            ->whereNull('m.point_id')
            ->whereNotNull('m.entity_id')
            ->whereBetween('m.created_at', [$startDate, $endDate])
            ->whereExists(function ($subquery) use ($featureId) {
                $subquery->selectRaw(1)
                ->from('feature_measurement as sub_fm')
                ->whereColumn('sub_fm.measure_id', 'm.id')
                ->where('sub_fm.feature_id', $featureId);
            });
        })
        ->where('feature_id', $featureId)
        ->orderByDesc('value')
        ->select('measure_id', 'feature_measurement.created_at', 'feature_measurement.value')
        ->first();
        
        $minQuery = FeatureMeasure::whereIn('measure_id', function ($query) use ($genericAssetIds, $featureId, $startDate, $endDate) {
            $query->select('m.id')
                ->from('measurements as m')
                ->whereIn('m.generic_asset_id', $genericAssetIds)
                ->whereNull('m.point_id')
                ->whereNotNull('m.entity_id')
                ->whereBetween('m.created_at', [$startDate, $endDate])
                ->whereExists(function ($subquery) use ($featureId) {
                    $subquery->selectRaw(1)
                        ->from('feature_measurement as sub_fm')
                        ->whereColumn('sub_fm.measure_id', 'm.id')
                        ->where('sub_fm.feature_id', $featureId);
                });
        })
        ->where('feature_id', $featureId)
        ->orderBy('value', 'asc')
        ->select('measure_id', 'feature_measurement.created_at', 'feature_measurement.value')
        ->first();
    
        return [$maxQuery, $minQuery];
    }
    
    public function getWorkingHourOfEntity(int $entityId, int $featureId, string $calculHour, string $per) : ?array
    {
        list($hour, $minute) = explode(':', $calculHour);
        $startDate = Carbon::now();
        $endDate = Carbon::now()->hour($hour)->minute($minute);
        switch ($per) {
            case 'day':
                $now = Carbon::now();
                $startDateValue = $now->copy()->hour($hour)->minute($minute)->second(0);
                if ($now->hour < $hour || ($now->hour == $hour && $now->minute < $minute)) {
                    $startDateValue->subDay();
                }
                $startDate = $startDateValue->toDateTimeString();
                $endDate = $startDateValue->copy()->addDay()->toDateTimeString();
                break;
    
            case 'week':
                $startDate->subDays(7)->hour($hour)->minute($minute)->toDateTimeString();
                break;
    
            case 'month':
                $startDate->startOfMonth()->hour($hour)->minute($minute)->toDateTimeString();
                $endDate->endOfMonth()->addDay(1)->hour($hour)->minute($minute)->toDateTimeString();
                break;
    
            case 'year':
                $startDate->startOfYear()->hour($hour)->minute($minute)->toDateTimeString();
                $endDate->endOfYear()->addDay(1)->hour($hour)->minute($minute)->toDateTimeString();
                break;
        }
        $maxQuery = FeatureMeasure::whereIn('measure_id', function ($query) use ($entityId, $featureId, $startDate, $endDate) {
            $query->select('m.id')
                ->from('measurements as m')
                ->where('m.entity_id', $entityId)
                ->whereNull('m.generic_asset_id')
                ->whereNull('m.point_id')
                ->whereBetween('m.created_at', [$startDate, $endDate])
                ->whereExists(function ($subquery) use ($featureId) {
                    $subquery->selectRaw(1)
                        ->from('feature_measurement as sub_fm')
                        ->whereColumn('sub_fm.measure_id', 'm.id')
                        ->where('sub_fm.feature_id', $featureId);
                });
        })
        ->where('feature_id', $featureId)
        ->orderByDesc('value')
        ->limit(1)
        ->select('measure_id', 'feature_measurement.created_at', 'feature_measurement.value')
        ->first();
        
        $minQuery = FeatureMeasure::whereIn('measure_id', function ($query) use ($entityId, $featureId, $startDate, $endDate) {
            $query->select('m.id')
                ->from('measurements as m')
                ->where('m.entity_id', $entityId)
                ->whereNull('m.generic_asset_id')
                ->whereNull('m.point_id')
                ->whereBetween('m.created_at', [$startDate, $endDate])
                ->whereExists(function ($subquery) use ($featureId) {
                    $subquery->selectRaw(1)
                        ->from('feature_measurement as sub_fm')
                        ->whereColumn('sub_fm.measure_id', 'm.id')
                        ->where('sub_fm.feature_id', $featureId);
                });
        })
        ->where('feature_id', $featureId)
        ->orderBy('value', 'asc')
        ->limit(1)
        ->select('measure_id', 'feature_measurement.created_at', 'feature_measurement.value')
        ->first();
        
        return [$maxQuery, $minQuery];
        
    }

     public function getProductionByEntityIds(array $entityIds, string $per, string $calculHour): ?Collection
    {
        $startDate = null;
        $endDate = null;
        list($startDate, $endDate) = DateRangeUtility::calculateRange($startDate, $endDate, $per, $calculHour);
        $query = FeatureMeasure::query()->whereHas('measurement', function ($query) use ($entityIds, $startDate, $endDate) {
            $query->whereIn('entity_id', $entityIds)
                ->whereBetween('created_at', [$startDate, $endDate]);
        })
            ->whereHas('feature', function ($query) {
                $query->where('name', FeatureMeasure::PRODUCTION);
            });
            $query->select(FeatureMeasure::VALUE_COLUMN);
            return $query->get();
    }

    public function updateProductionValue(string $measureId, array $attributes): bool
    {
        $attributes = Arr::only(
            $attributes,
            [
                FeatureMeasure::VALUE_COLUMN,
            ]
        );
        return FeatureMeasure::query()
                ->where(FeatureMeasure::MEASURE_ID_COLUMN, $measureId)
                ->where(FeatureMeasure::FEATURE_ID_COLUMN, FeatureMeasure::PRODUCTION_ID)
                ->update($attributes) > 0;
    }
}