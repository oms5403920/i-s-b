<?php

namespace App\Core\Repositories\Point;

use App\Core\Models\Point\Point;
use App\Core\Repositories\Repository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

class PointRepository extends Repository
{
    public function getAllByGenericAsset(int $genericAssetId): ?Collection
    {
        return Point::where(Point::GENERIC_ASSET_ID_COLUMN, $genericAssetId)
            ->whereNull('deleted_at')
            ->get();
    }

    public function getById(int $Id): ?Point
    {
        return Point::where(Point::ID_COLUMN, $Id)
            ->whereNull('deleted_at')
            ->first();
    }

    public function create($attributes) : Point
    {
        return Point::create([
            Point::NAME_COLUMN => Arr::get($attributes, Point::NAME_COLUMN),
            Point::TYPE_COLUMN => Arr::get($attributes, Point::TYPE_COLUMN),
            Point::GENERIC_ASSET_ID_COLUMN => Arr::get($attributes, Point::GENERIC_ASSET_ID_COLUMN),
            Point::DEVICE_ID_COLUMN => Arr::get($attributes, Point::DEVICE_ID_COLUMN)
        ]);
    }

    public function getAllByDeviceId(int $deviceId): ?Collection
    {
        return Point::where(Point::DEVICE_ID_COLUMN, $deviceId)->get();
    }
}
