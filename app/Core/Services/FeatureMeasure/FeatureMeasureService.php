<?php

namespace App\Core\Services\FeatureMeasure;

use App\Core\Managers\FeatureMeasure\FeatureMeasureManager;
use App\Core\Models\FeatureMeasure\FeatureMeasure;
use App\Core\Services\Features\FeaturesService;
use App\Core\Services\Service;
use Carbon\Carbon;

class FeatureMeasureService extends Service
{
    public function __construct(
        private FeatureMeasureManager $featureMeasureManager,
        private FeaturesService $featuresService
    ) {
        parent::__construct();
    }

    public function create(int $measureId, float $featureValue, int $featureId,Carbon $measure_created_at): void
    {
        $this->featureMeasureManager->create($measureId, $featureValue, $featureId,$measure_created_at);
    }

    public function createFeatureMeasure($measureId, array $request,Carbon $measure_created_at): void
    {
        $powerFeature = $this->featuresService->getByName('Power(kW)');
        $energyFeature = $this->featuresService->getByName('Energy');
        $energyFeatureCompteur = $this->featuresService->getByName('energyCompteur');
        $power = 'Puissance_Active_Actuelle';
        if (array_key_exists($power, $request)) {
            $featureValue = $request[$power];
            $this->create($measureId, $featureValue, $powerFeature->getId(),$measure_created_at);
        }

        $energy = "Energie_Active_Actuelle";
        if (array_key_exists($energy, $request)) {
            $featureValue = $request[$energy];
            $this->create($measureId, $featureValue, $energyFeature->getId(),$measure_created_at);
        }

        $energyCompteur = "energyCompteur";
        if (array_key_exists($energyCompteur, $request)) {
            $featureValue = $request[$energyCompteur];
            $this->create($measureId, $featureValue, $energyFeatureCompteur->getId(),$measure_created_at);
        }
    }

    public function getFeatureMeasure(int $measureId, int $featureId): ?FeatureMeasure
    {
        return $this->featureMeasureManager->getFeatureMeasure($measureId, $featureId);
    }

    public function updateProductionValue(string $measureId, array $attributes): bool
    {
        return $this->featureMeasureManager->updateProductionValue($measureId, $attributes);
    }
}
