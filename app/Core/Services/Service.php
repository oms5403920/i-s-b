<?php

namespace App\Core\Services;

use App\Core\Log\AppLogger;

abstract class Service
{
    protected AppLogger $appLogger;

    public function __construct()
    {
        $this->appLogger = app(AppLogger::class);
    }
}
