<?php

namespace App\Core\Services\GenericAssetsExtraField;

use App\Core\Managers\GenericAssetsExtraField\GenericAssetsExtraFieldManager;
use App\Core\Models\GenericAssetsExtraField\GenericAssetsExtraField;
use App\Core\Services\Service;

class GenericAssetsExtraFieldService extends Service
{
    public function __construct(
        private GenericAssetsExtraFieldManager $genericAssetsExtraFieldManager
    ) {
        parent::__construct();
    }

    public function getByGenericAssetIdAndFieldId(int $genericAssetId, int $fieldId): ?GenericAssetsExtraField
    {
        return $this->genericAssetsExtraFieldManager->getByGenericAssetIdAndFieldId($genericAssetId, $fieldId);
    }

    public function create(array $attributes)
    {
        return $this->genericAssetsExtraFieldManager->create($attributes);
    }
}
