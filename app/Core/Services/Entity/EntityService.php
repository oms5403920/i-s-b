<?php

namespace App\Core\Services\Entity;

use App\Core\Models\Entity\Entity;
use App\Core\Services\Service;
use App\Core\Log\LogParametersList;
use App\Core\Managers\Entity\EntityManager;
use App\Core\Models\Measure\Measure;
use App\Core\Models\User\User;
use App\Core\Services\EntityUser\EntityUserService;
use App\Core\Services\Feature\FeatureList;
use Illuminate\Database\Eloquent\Collection;

class EntityService extends Service
{
    public function __construct(
        private EntityManager $entityManager,
        private EntityUserService $entityUserService
    ) {
        parent::__construct();
    }

    public function getAllByUserId(int $userId): ?Collection
    {
        $userEntities = $this->entityUserService->getByUserId($userId)->pluck('entity_id')->toArray();
        $entities = $this->entityManager->getAllByIds($userEntities);
        $this->appLogger->info(
            'finding user entities',
            [
                LogParametersList::FEATURE => FeatureList::ENTITY,
                LogParametersList::USER_ID => $userId,
            ]
        );

        return $entities;
    }

    public function getById(int $id): ?Entity
    {
        $entity = $this->entityManager->getById($id);
        $this->appLogger->info(
            'finding entity by id',
            [
                LogParametersList::FEATURE   => FeatureList::ENTITY,
                LogParametersList::ENTITY_ID => $id,
            ]
        );

        return $entity;
    }

    public function getSubEntitiesById(int $id): ?Collection
    {
        $entityChildrens = $this->entityManager->getSubEntitiesById($id);
        $this->appLogger->info(
            'finding entity childrens ',
            [
                LogParametersList::FEATURE    => FeatureList::ENTITY,
                LogParametersList::SUBFEATURE => FeatureList::ENTITY_CHILDRENS,
                LogParametersList::ENTITY_ID  => $id,
            ]
        );

        return $entityChildrens;
    }

    public function update(Entity $entity, array $attributes): bool
    {
        return $this->entityManager->update($entity, $attributes);
    }

    public function LastMeasure(int $entityId) : ?Measure
    {
       return $this->entityManager->LastMeasure($entityId);
    }

    public function getLastRunningTimeEntity(int $entityId) : ?float
    {
       return $this->entityManager->getLastRunningTimeEntity($entityId);
    }

    public function getTopLevelParent(int $entityId, ?int $parentEntityId = null) : ?Entity
    {
        return $this->entityManager->getTopLevelParent($entityId, $parentEntityId);
    }

    public function getDescendantIds(array $entityIds) : array
    {
        $descendantIds = [];

        foreach ($entityIds as $entityId) {
            $queue = [$entityId];

            while (!empty($queue)) {
                $entityId = array_shift($queue);
                $childIds = $this->entityManager->getChildIds($entityId);

                $queue = array_merge($queue, $childIds);
                $descendantIds = array_merge($descendantIds, $childIds);
            }
        }

        return $descendantIds;
    }

    public function getAllByIds(array $ids) : ?Collection
    {
        return $this->entityManager->getAllByIds($ids);
    }

    public function getUserTree(User $user) : array
    {
        return $this->entityManager->getUserTree($user);
    }
}
