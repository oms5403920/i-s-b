<?php

namespace App\Core\Services\Measure;

use App\Core\Managers\Measure\MeasureManager;
use App\Core\Models\GenericAsset\GenericAsset;
use App\Core\Models\Measure\Measure;
use App\Core\Services\Entity\EntityService;
use App\Core\Services\GenericAsset\GenericAssetService;
use App\Core\Services\Point\PointService;
use App\Core\Services\Service;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class MeasureService extends Service
{
    public function __construct(
        private MeasureManager $measureManager,
        private GenericAssetService $genericAssetService,
        private EntityService $entityService,
        private PointService $pointService
    ) {
        parent::__construct();
    }

    public function lastMeasureForEachPoint(
        array $pointIds,
        string $calculHour,
        ?string $startDate = null,
        ?string $endDate = null,
        ?string $per = 'month',
        bool $energyCompteur = false
    ): array {
        $lastMeasures = [];
        foreach ($pointIds as $point) {
            $point_id = $point['point_id'];
            $lastMeasurements = $this->measureManager->lastMeasureForEachPoint($point_id, $calculHour, $startDate, $endDate, $per, $energyCompteur);
            if (!$lastMeasurements->isEmpty()) {
                foreach($lastMeasurements as $lastMeasurement)
                {
                    $energyValue = $lastMeasurement->featureMeasurement->where('feature_id',29)->first()->getValue();
                    $energyUnit = $lastMeasurement->featureMeasurement->where('feature_id',29)->first()->feature->getUnit();
                    $lastMeasures[] =
                    [
                        'name'         => $point['name'],
                        'point_id'     => $point_id,
                        'measure_id'   => $lastMeasurement->getId(),
                        'type'         => $point['type'],
                        'parentEntity' => $point['entity'],
                        'entity'       => $this->entityService->getById($lastMeasurement->getEntityId())->getName(
                            ) ?? null,
                        'date'         => $lastMeasurement->getCreatedAt(),
                        'per' => $per ?? null,
                        'energyValue'  => $energyValue,
                        'energyUnit'   => $energyUnit
                    ];
                }
            }
        }

        return $lastMeasures;
    }

    public function firstMeasureForEachPoint(
        array $pointIds,
        string $calculHour,
        ?string $startDate = null,
        ?string $endDate = null,
        ?string $per = 'month',
        bool $energyCompteur = false
    ): array {
        $firstMeasures = [];
        foreach ($pointIds as $point) {
            $point_id = $point['point_id'];
            $firstMeasurements = $this->measureManager->firstMeasureForEachPoint($point_id, $calculHour, $startDate, $endDate, $per, $energyCompteur);
            if (!$firstMeasurements->isEmpty()) {
                foreach($firstMeasurements as $firstMeasurement)
                {
                    $energyValue = $firstMeasurement->featureMeasurement->where('feature_id',29)->first()->getValue();
                    $entity = $this->entityService->getById($firstMeasurement->getEntityId());
                    $firstMeasures[] =
                    [
                        'name'         => $point['name'],
                        'point_id'     => $point_id,
                        'measure_id'   => $firstMeasurement->getId(),
                        'type'         => $point['type'],
                        'parentEntity' => $point['entity'],
                        'entity'       => $entity->getName(
                            ) ?? null,
                         'date'         => $firstMeasurement->getCreatedAt(),
                        'per' => $per ?? 'null',
                        'energyValue' => $energyValue,
                    ];
                }
            }
        }
        return $firstMeasures;
    }


    public function getGenericAssetMeasurements(array $assetId, string $calculHour, ?string $startDate = null, ?string $endDate = null): array
    {
        if ($assetId !== null) {
            $measurements = $this->measureManager->getGenericAssetMeasurements($assetId, $calculHour, $startDate, $endDate)->toArray();
        }
        return $measurements;
    }


    public function create(int $pointId, $request): ?Measure
    {
        $point = $this->pointService->getById($pointId);
        $genericAsset = $this->genericAssetService->getById($point->getGenericAssetId());
        $timestamp = "timestamp";
        if (array_key_exists($timestamp, $request)) {
            $dateMeasure = $request[$timestamp];
            return $this->measureManager->create($pointId, $point->getGenericAssetId(), $genericAsset->getEntityId(),$dateMeasure);
        }
        return null;
    }

    public function getById(int $id): Measure
    {
        return $this->measureManager->getById($id);
    }

    public function getBeforeLastMeasure($gAssetId): ?Measure
    {
        return $this->measureManager->getBeforeLastMeasure($gAssetId);
    }

    public function getLastOrFirstConsummationMeasureForEachPoint(array $pointIds, string $calculHour, ?string $startDate = null, ?string $endDate = null, ?string $per = 'month', bool $getLast = true): array
    {
        $measures = [];
        $entityService = $this->entityService;

        foreach ($pointIds as $pointData) {
            $point_id = $pointData['point_id'];
            $measurement = $getLast ?
                $this->measureManager->lastConsummationMeasureForEachPoint($point_id, $calculHour, $startDate, $endDate, $per) :
                $this->measureManager->firstConsummationMeasureForEachPoint($point_id, $calculHour, $startDate, $endDate, $per);

            if ($measurement instanceof Measure) {
               $entity = $measurement->entity;
               $parentEntity = $entityService->getTopLevelParent($entity->getId())->getName() ?? null;
                $measures[] = [
                    'name'         => $pointData['name'],
                    'point_id'     => $point_id,
                    'measure_id'   => $measurement->getId(),
                    'type'         => $pointData['type'],
                    'parentEntity' => $parentEntity,
                    'entity'       => $entity->getName() ?? null,
                    'date'         => $measurement->getCreatedAt(),
                    'per'          => $per,
                    'value'        => $measurement->feature_value
                ];
            }
        }

        return $measures;
    }

    public function getLastOrFirstConsummationMeasureStatistics(array $pointIds, string $calculHour, ?string $startDate = null, ?string $endDate = null, ?string $per = 'month', bool $getLast = true): array
    {
        $measures = [];
        foreach ($pointIds as $pointData) {
            $point_id = $pointData['point_id'];
            $measurement = $getLast ?
                $this->measureManager->lastConsummationMeasureForEachPoint($point_id, $calculHour, $startDate, $endDate, $per, true) :
                $this->measureManager->firstConsummationMeasureForEachPoint($point_id, $calculHour, $startDate, $endDate, $per, true);

            if ($measurement instanceof Measure) {
                $measures[] = [
                    'name'         => $pointData['name'],
                    'point_id'     => $point_id,
                    'measure_id'   => $measurement->getId(),
                    'date'         => $measurement->getCreatedAt(),
                    'type'         => $pointData['type'],
                    'value'        => $measurement->feature_value
                ];
            }
        }
        return $measures;
    }

    public function createForGenericAsset(GenericAsset $gAsset, Carbon $measure_created_at): ?Measure
    {
        return $this->measureManager->createForGenericAsset($gAsset->getId(),$gAsset->getEntityId(),$measure_created_at);
    }

    public function createForEntity(int $entityId, Carbon $measure_created_at): ?Measure
    {
        return $this->measureManager->createForEntity($entityId, $measure_created_at);
    }

    public function getGenericAssetsRunningMeasurements(array $genericAssetId, string $calculHour,
        ?string $startDate = null, ?string $endDate = null) : array{
            if ($genericAssetId !== null) {
                $measurements = $this->measureManager->getGenericAssetsRunningMeasurements($genericAssetId, $calculHour, $startDate, $endDate)->toArray();
            }
            return $measurements;
        }

    public function isTarifMeasureExist(int $genericAssetId, string $startHour, string $endHour) : bool
    {
        return $this->measureManager->isTarifMeasureExist($genericAssetId, $startHour, $endHour);
    }

    public function getFirstAndLastEnergyMeasures(int $genericAssetId, string $heureDebut, string $heureFin) : ?array
    {
        return $this->measureManager->getFirstAndLastEnergyMeasures($genericAssetId, $heureDebut, $heureFin);
    }
    public function getGenericAssetMeasurementsOfEachDay(array $assetId, string $calculHour, ?string $startDate = null, ?string $endDate = null): array
    {
        $measurements = [];
        if ($assetId !== null) {
            $measurements = $this->measureManager->getGenericAssetMeasurementsOfEachDay($assetId, $calculHour, $startDate, $endDate);
        }
        return $measurements;
    }

    public function getLastMeasure(int $pointId) : ?object
    {
        return $this->measureManager->getLastMeasure($pointId);
    }
}
