<?php

namespace App\Core\Services\GenericAsset;

use App\Core\Managers\GenericAsset\GenericAssetManager;
use App\Core\Models\Entity\Entity;
use App\Core\Models\GenericAsset\GenericAsset;
use App\Core\Models\Measure\Measure;
use App\Core\Services\Entity\EntityService;
use App\Core\Services\Service;
use Illuminate\Database\Eloquent\Collection;

class GenericAssetService extends Service
{
    public function __construct(
        private GenericAssetManager $genericAssetManager,
        private EntityService $entityService
    ) {
        parent::__construct();
    }

    public function getAllByEntityId(int $entityId): ?Collection
    {
        return $this->genericAssetManager->getAllByEntityId($entityId);
    }
    public function getAllByEntityIdWithConsummation(int $entityId, string $calculHour, ?string $startDate, ?string $endDate, ?string $per): ?Collection
    {
        return $this->genericAssetManager->getAllByEntityIdWithConsummation($entityId, $calculHour, $startDate, $endDate, $per);
    }

    public function getById(int $id): ?GenericAsset
    {
        return $this->genericAssetManager->getById($id);
    }

    public function getGenericAssetsOfUserEntitiesWithConsummation(Collection $entities, array &$assetIds,int $calculHour, ?string $startDate = null, ?string $endDate = null, ?string $per = null): void
    {
        foreach ($entities as $entity) {
            $this->getGenericAssetsOfEntityWithConsummation($entity, $assetIds, $calculHour, $startDate, $endDate, $per);
        }
    }

    public function getGenericAssetsOfEntityWithConsummation($entity, &$assetIds,$calculHour, $startDate, $endDate, $per): void
    {
        $entityChildrens = $this->entityService->getSubEntitiesById($entity->getId());

        foreach ($entityChildrens as $child) {
            $this->getAssets($child, $assetIds, $calculHour, $startDate, $endDate, $per);
        }
    }

    private function getAssets($entity, &$assetIds,$calculHour, $startDate, $endDate, $per): void
    {
        $genericAssets = $this->getAllByEntityIdWithConsummation($entity->getId(), $calculHour, $startDate, $endDate, $per);

        if (!$genericAssets->isEmpty()) {
            foreach ($genericAssets as $asset) {
                if (!in_array($asset, $assetIds)) {
                    $assetIds[] = $asset;
                }
            }
        }

        $children = $this->entityService->getSubEntitiesById($entity->getId());
        foreach ($children as $child) {
            $this->getAssets($child, $assetIds,$calculHour, $startDate, $endDate, $per);
        }
    }


    public function getGenericAssetsOfUserEntities(Collection $entities, array &$assetIds): void
    {
        foreach ($entities as $entity) {
            $this->getGenericAssetsOfEntity($entity, $assetIds);
        }
    }

    public function getGenericAssetsOfEntity($entity, &$assetIds): void
    {
        $genericAssets = $this->getAllByEntityId($entity->getId());
        foreach ($genericAssets as $asset) {
            $assetId = $asset->getId();
            if (!in_array($assetId, array_column($assetIds, 'id'))) {
                $assetIds[] = [
                    'id' => $assetId,
                    'type' => $asset->getEnergyType(),
                    'entity' => $entity->getName()
                ];
            }
        }
        $entityChildrens = $this->entityService->getSubEntitiesById($entity->getId());
        foreach ($entityChildrens as $child) {
            $this->getGenericAssetsOfEntity($child, $assetIds);
        }
    }



    public function entityGenericAssets($entity): ?array
    {
        $assetIds = [];
        $genericAssets = $entity->genericAssets;
        if (!$genericAssets->isEmpty()) {
            foreach ($genericAssets as $asset) {
                $assetId = $asset->getId();
                if (!in_array($assetId, $assetIds)) {
                    $assetIds[] = [
                        'id' => $assetId,
                        'type' => $asset->getEnergyType(),
                        'entity' => $entity->getName()
                    ];
                }
            }
        }

        return $assetIds;
    }

    public function update(GenericAsset $gAsset, array $attributes): bool
    {
        return $this->genericAssetManager->update($gAsset, $attributes);
    }

    public function getLastRunningTimeAsset(int $gAssetId) : ?float
    {
       return $this->genericAssetManager->getLastRunningTimeAsset($gAssetId);
    }

    public function checkRunningStatus(array $genericAssetIds) : bool
    {
        return $this->genericAssetManager->checkRunningStatus($genericAssetIds);
    }

    public function LastMeasure(int $gAssetId) : ?Measure
    {
       return $this->genericAssetManager->LastMeasure($gAssetId);
    }

    public function getAllByEntityIdAndEnergyType(int $entityId, int $energyType): ?Collection
    {
        return $this->genericAssetManager->getAllByEntityIdAndEnergyType($entityId, $energyType);
    }

    public function getByIds(array $ids): Collection
    {
        return $this->genericAssetManager->getByIds($ids);
    }

    public function getFirstAndLastConsumptionMeasures(array $entityIds,  $assetIds, string $calculHour, ?string $startDate, ?string $endDate, ?string $per) : ?array
    {
        $descendantIds = $this->entityService->getDescendantIds($entityIds);
        $assetIds = $this->genericAssetManager->getFirstAndLastConsumptionMeasures($descendantIds, $calculHour, $startDate, $endDate, $per);
        return [
            'assets' => $assetIds,
            'entitiesIds' => $descendantIds
        ];
    }

    public function getFirstAndLastProducedMeasures(array $entityIds,  $assetIds, string $calculHour, ?string $startDate, ?string $endDate, ?string $per) : ?array
    {
        $descendantIds = $this->entityService->getDescendantIds($entityIds);
        $assetIds = $this->genericAssetManager->getFirstAndLastProducedMeasures($descendantIds, $calculHour, $startDate, $endDate, $per);
        return [
            'assets' => $assetIds,
            'entitiesIds' => $descendantIds
        ];
    }

    public function create(array $attributes) : GenericAsset
    {
        return $this->genericAssetManager->create($attributes);
    }

    public function getRelatedData() : array
    {
        return $this->genericAssetManager->getRelatedData();
    }

    public function getAll() : ?Collection
    {
        return $this->genericAssetManager->getAll();
    }

    public function getGenericAssetsOfUserEntitiesOptimize(Collection $entities, array &$assetIds): void
{
    $entityIds = $entities->pluck('id')->toArray();
    $descendantIds = $this->entityService->getDescendantIds($entityIds);
    $ids = array_merge($entityIds, $descendantIds);
    $entitiesWithAssets = Entity::with('genericAssets')->find($ids);

    foreach ($entitiesWithAssets as $entity) {
        foreach ($entity->genericAssets as $asset) {
            $assetId = $asset->getId();

            if (!in_array($assetId, array_column($assetIds, 'id'))) {
                $assetIds[] = [
                    'id' => $assetId,
                    'type' => $asset->getEnergyType(),
                    'entity' => $entity->name,
                ];
            }
        }
    }
}
}
