<?php

namespace App\Core\Services\Features;

use App\Core\Managers\Features\FeaturesManager;
use App\Core\Models\Features\Features;
use App\Core\Services\Service;

class FeaturesService extends Service
{
    public function __construct(
        private FeaturesManager $featuresManager
    ) {
        parent::__construct();
    }

    public function getByName(string $name): ?Features
    {
        return $this->featuresManager->getByName($name);
    }
}
