<?php

namespace App\Core\Services\EntityUser;

use App\Core\Services\Service;
use App\Core\Log\LogParametersList;
use App\Core\Managers\EntityUser\EntityUserManager;
use App\Core\Services\Feature\FeatureList;
use Illuminate\Database\Eloquent\Collection;

class EntityUserService extends Service
{
    public function __construct(
        private EntityUserManager $entityUserManager,
    ) {
        parent::__construct();
    }

    public function getByUserId(int $userId): ?Collection
    {
        $entities = $this->entityUserManager->getByUserId($userId);
        $this->appLogger->info(
            'finding user entities',
            [
                LogParametersList::FEATURE => FeatureList::ACCOUNT_USERS,
                LogParametersList::USER_ID => $userId,
            ]
        );

        return $entities;
    }
}
