<?php

namespace App\Core\Services\Point;

use App\Core\Models\GenericAsset\GenericAsset;
use App\Core\Models\Point\Point;
use App\Core\Services\Service;
use App\Core\Managers\Point\PointManager;
use App\Core\Services\GenericAsset\GenericAssetService;
use Illuminate\Database\Eloquent\Collection;

class PointService extends Service
{
    public function __construct(
        private PointManager $pointManager,
        private GenericAssetService $genericAssetService
    ) {
        parent::__construct();
    }

    public function getGenericAssetsPoints(array $assetIds): array
    {
        $pointIds = [];
        foreach ($assetIds as $asset) {
            $genericAsset = $this->genericAssetService->getById($asset['id']);
            if ($genericAsset instanceof GenericAsset) {
                $points = $genericAsset->points;
                foreach ($points as $point) {
                    $pointIds[] = [
                        'name'     => $genericAsset->getName(),
                        'point_id' => $point->getId(),
                        'type'     => $asset['type'] ?? $genericAsset->getEnergyType(),
                        'entity'   => $asset['entity'] ?? $genericAsset->entity->getName(),
                    ];
                }
            }
        }

        return $pointIds;
    }

    public function getGenericAssetsPointsStatistics(array $assetIds): array
    {
        $pointIds = [];
        foreach ($assetIds as $asset) {
            $genericAsset = $this->genericAssetService->getById($asset['id']);
            if ($genericAsset instanceof GenericAsset) {
                $points = $this->pointManager->getAllByGenericAsset($asset['id']);
                foreach ($points as $point) {
                    $pointIds[] = [
                        'name'     => $genericAsset->getName(),
                        'point_id' => $point->getId(),
                        'type'     => $asset['type'] ?? $genericAsset->getEnergyType(),
                    ];
                }
            }
        }

        return $pointIds;
    }

    public function getById(int $id) : ?Point
    {
        return $this->pointManager->getById($id);
    }

    public function create(array $attributes) : Point
    {
        return $this->pointManager->create($attributes);
    }

    public function getAllByDeviceId(int $deviceId) : ?Collection
    {
        return $this->pointManager->getAllByDeviceId($deviceId);
    }
}
