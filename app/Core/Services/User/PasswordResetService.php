<?php

namespace App\Core\Services\User;

use App\Core\Managers\User\PasswordResetManager;
use App\Core\Services\Service;
use App\Core\Models\User\PasswordResetToken;

class PasswordResetService extends Service
{
    public function __construct(
        private PasswordResetManager $passwordResetManager,
    ) {
        parent::__construct();
    }

    public function findByToken(string $token): ?PasswordResetToken
    {
        $passwordReset = $this->passwordResetManager->findByToken($token);
        if (!$passwordReset instanceof PasswordResetToken) {
            return null;
        }

        return $passwordReset;
    }
}
