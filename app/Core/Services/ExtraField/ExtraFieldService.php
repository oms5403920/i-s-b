<?php

namespace App\Core\Services\ExtraField;

use App\Core\Managers\ExtraField\ExtraFieldManager;
use App\Core\Models\ExtraField\ExtraField;
use App\Core\Models\GenericAsset\GenericAsset;
use App\Core\Models\Point\Point;
use App\Core\Services\Service;

class ExtraFieldService extends Service
{
    public function __construct(
        private ExtraFieldManager $extraFieldManager
    ) {
        parent::__construct();
    }

    public function findByName(string $name): ?ExtraField
    {
        return $this->extraFieldManager->findByName($name);
    }
}
