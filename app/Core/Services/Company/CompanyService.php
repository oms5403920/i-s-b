<?php

namespace App\Core\Services\Company;

use App\Core\Services\Service;
use App\Core\Managers\Company\CompanyManager;
use App\Core\Models\Company\Company;

class CompanyService extends Service
{
    public function __construct(
        private CompanyManager $companyManager
    ) {
        parent::__construct();
    }

    public function findById(int $id) : ?Company
    {
        return $this->companyManager->findById($id);
    }
}
