<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forecasts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('point_id');
            $table->string('magnitude');
            $table->mediumText('ds');
            $table->mediumText('y');
            $table->mediumText('y_lower');
            $table->mediumText('y_upper');
            $table->mediumText('metrics');
            $table->integer('days_to_alert')->nullable()->default(365);
            $table->integer('days_to_alarm')->nullable()->default(365);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forecasts');
    }
};
