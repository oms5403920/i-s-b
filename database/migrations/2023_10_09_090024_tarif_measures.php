<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tarif_measures', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tarif_id');
            $table->foreign('tarif_id')->references('id')->on('tarifs')->onDelete('cascade');
            $table->unsignedInteger('measure_id');
            $table->foreign('measure_id')->references('id')->on('measurements')->onDelete('cascade');
            $table->unsignedInteger('generic_asset_id');
            $table->foreign('generic_asset_id')->references('id')->on('generic_assets')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
