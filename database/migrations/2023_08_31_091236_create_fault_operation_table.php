<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fault_operation', function (Blueprint $table) {
            $table->increments('id');
            $table->softDeletes();
            $table->unsignedInteger('operation_id')->index('fault_operation_operation_id_foreign');
            $table->unsignedInteger('fault_id')->index('fault_operation_fault_id_foreign');
            //add foreign keys
            $table->foreign(['fault_id'])->references(['id'])->on('faults')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['operation_id'])->references(['id'])->on('operations')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fault_operation');
    }
};
