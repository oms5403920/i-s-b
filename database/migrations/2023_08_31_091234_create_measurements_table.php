<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurements', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('status')->index();
            $table->timestamp('created_at')->nullable()->index('measurements__created_at_desc_index');
            $table->timestamp('updated_at')->nullable();
            $table->unsignedInteger('point_id')->nullable()->index('measurements_grandeur_point_id_index');
            $table->unsignedInteger('device_id')->nullable();
            $table->unsignedInteger('config_id')->nullable()->index('measurements_config_id_foreign');
            $table->softDeletes();
            $table->unsignedInteger('asset_id')->nullable()->index();
            $table->boolean('is_generic')->default(false);
            $table->unsignedBigInteger('generic_asset_id')->nullable()->index();
            $table->unsignedBigInteger('generic_point_id')->nullable()->index();
            $table->unsignedInteger('entity_id')->unsigned()->nullable();
            //add foreign keys 
            $table->foreign(['asset_id'])->references(['id'])->on('assets')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['config_id'])->references(['id'])->on('acquisition_configurations')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['device_id'])->references(['id'])->on('devices')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['generic_asset_id'])->references(['id'])->on('generic_assets')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['generic_point_id'])->references(['id'])->on('generic_points')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['point_id'])->references(['id'])->on('measurement_points')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('entity_id')->references('id')->on('entities');
            //index's
            $table->index(['created_at', 'generic_asset_id'], 'measurements_created_at_generic_asset_id');
            $table->index(['point_id', 'created_at']);
            $table->index(['created_at']);
            $table->index(['created_at', 'point_id']);
            $table->index(['point_id', 'device_id', 'created_at']);
            $table->index(['device_id', 'created_at']);
            $table->index(['point_id', 'created_at'], 'measurements_point_id_grandeur_created_at_index');
            $table->index(['generic_asset_id', 'is_generic', 'created_at']);
            $table->index(['status', 'created_at']);
            $table->index(['created_at'], 'measurements_grandeur_created_at');
            $table->index(['deleted_at', 'created_at'], 'measurements_grandeur_deleted_at_created_at');
            $table->index(['created_at'], 'measurements_velocity_created_at');
            $table->index(['id', 'created_at'], 'measurements_id_created_at');
            $table->index(['id', 'point_id', 'created_at']);
            $table->index(['id', 'point_id']);
            $table->index(['point_id', 'created_at', 'deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measurements');
    }
};
