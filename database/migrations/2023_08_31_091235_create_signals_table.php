<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('description')->index();
            $table->string('unit')->nullable()->index();
            $table->unsignedInteger('grandeur_id')->nullable()->index();
            // add foreign key 
            $table->foreign(['grandeur_id'])->references(['id'])->on('grandeurs')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->timestamps();
            $table->softDeletes()->index();
            $table->string('x_unit_description')->nullable();
            $table->string('x_unit')->nullable();
            $table->integer('signal_type')->nullable();
            $table->string('signal_type_description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signals');
    }
};
