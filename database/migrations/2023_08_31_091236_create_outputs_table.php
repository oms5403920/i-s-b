<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outputs', function (Blueprint $table) {
            $table->text('mp_name')->nullable();
            $table->double('acceleration')->nullable();
            $table->double('velocity')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->text('moto_pompe')->nullable();
            $table->double('vel_amplitude_at_1_f0')->nullable();
            $table->double('vel_amplitude_at_2_f0')->nullable();
            $table->double('acc_amplitude_at_1_f0')->nullable();
            $table->double('acc_amplitude_at_2_f0')->nullable();
            $table->double('kurtosis')->nullable();
            $table->double('facteur_crete')->nullable();
            $table->double('facteur_k')->nullable();
            $table->double('vel_amp_at_fbpi')->nullable();
            $table->double('acc_amp_at_fbpi')->nullable();
            $table->double('vel_amp_at_fbpo')->nullable();
            $table->double('acc_amp_at_fbpo')->nullable();
            $table->double('vel_amp_at_fft')->nullable();
            $table->double('acc_amp_at_fft')->nullable();
            $table->double('vel_amp_at_fbs')->nullable();
            $table->double('acc_amp_at_fbs')->nullable();
            $table->double('vel_amp_at_2_freq_alimentation')->nullable();
            $table->double('acc_amp_at_2_freq_alimentation')->nullable();
            $table->boolean('balourd_indicator')->nullable();
            $table->boolean('desalignement_indicator')->nullable();
            $table->boolean('defaut_electrique_indicator')->nullable();
            $table->boolean('defaut_de_roulement_indicator')->nullable();
            $table->boolean('defaut_caviation')->nullable();
            $table->boolean('defaut_lubrification')->nullable();
            $table->double('slope_degree_by_win_of_vel_amplitude_at_1_f0')->nullable();
            $table->double('slope_degree_by_win_of_vel_amplitude_at_2_f0')->nullable();
            $table->double('slope_degree_by_win_of_acc_amplitude_at_1_f0')->nullable();
            $table->double('slope_degree_by_win_of_acc_amplitude_at_2_f0')->nullable();
            $table->double('slope_degree_by_win_of_kurtosis')->nullable();
            $table->double('slope_degree_by_win_of_facteur_crete')->nullable();
            $table->double('slope_degree_by_win_of_facteur_k')->nullable();
            $table->double('slope_degree_by_win_of_vel_amp_at_fbpi')->nullable();
            $table->double('slope_degree_by_win_of_acc_amp_at_fbpi')->nullable();
            $table->double('slope_degree_by_win_of_vel_amp_at_fbpo')->nullable();
            $table->double('slope_degree_by_win_of_acc_amp_at_fbpo')->nullable();
            $table->double('slope_degree_by_win_of_vel_amp_at_fft')->nullable();
            $table->double('slope_degree_by_win_of_acc_amp_at_fft')->nullable();
            $table->double('slope_degree_by_win_of_vel_amp_at_fbs')->nullable();
            $table->double('slope_degree_by_win_of_acc_amp_at_fbs')->nullable();
            $table->double('slope_degree_by_win_of_vel_amp_at_2_freq_alimentation')->nullable();
            $table->double('slope_degree_by_win_of_acc_amp_at_2_freq_alimentation')->nullable();
            $table->double('slope_degree_by_win_of_velocity')->nullable();
            $table->double('slope_degree_by_win_of_acceleration')->nullable();
            $table->double('deviation_by_win_of_vel_amplitude_at_1_f0')->nullable();
            $table->double('deviation_by_win_of_vel_amplitude_at_2_f0')->nullable();
            $table->double('deviation_by_win_of_acc_amplitude_at_1_f0')->nullable();
            $table->double('deviation_by_win_of_acc_amplitude_at_2_f0')->nullable();
            $table->double('deviation_by_win_of_kurtosis')->nullable();
            $table->double('deviation_by_win_of_facteur_crete')->nullable();
            $table->double('deviation_by_win_of_facteur_k')->nullable();
            $table->double('deviation_by_win_of_vel_amp_at_fbpi')->nullable();
            $table->double('deviation_by_win_of_acc_amp_at_fbpi')->nullable();
            $table->double('deviation_by_win_of_vel_amp_at_fbpo')->nullable();
            $table->double('deviation_by_win_of_acc_amp_at_fbpo')->nullable();
            $table->double('deviation_by_win_of_vel_amp_at_fft')->nullable();
            $table->double('deviation_by_win_of_acc_amp_at_fft')->nullable();
            $table->double('deviation_by_win_of_vel_amp_at_fbs')->nullable();
            $table->double('deviation_by_win_of_acc_amp_at_fbs')->nullable();
            $table->double('deviation_by_win_of_vel_amp_at_2_freq_alimentation')->nullable();
            $table->double('deviation_by_win_of_acc_amp_at_2_freq_alimentation')->nullable();
            $table->double('deviation_by_win_of_velocity')->nullable();
            $table->double('deviation_by_win_of_acceleration')->nullable();
            $table->text('gravity_level_iso')->nullable();
            $table->boolean('faulty_measure')->nullable();
            $table->boolean('low_vibration')->nullable();
            $table->boolean('missing_previous_observations')->nullable();
            $table->boolean('abnormal')->nullable();
            $table->bigInteger('balourd_indicator_ml')->nullable();
            $table->bigInteger('balourd_indicator_ml_proba')->nullable();
            $table->bigInteger('desalignement_indicator_ml')->nullable();
            $table->bigInteger('desalignement_indicator_ml_proba')->nullable();
            $table->bigInteger('defaut_de_roulement_indicator_ml')->nullable();
            $table->bigInteger('defaut_de_roulement_indicator_ml_proba')->nullable();
            $table->bigInteger('defaut_electrique_indicator_ml')->nullable();
            $table->bigInteger('defaut_electrique_indicator_ml_proba')->nullable();
            $table->bigInteger('defaut_lubrification_ml')->nullable();
            $table->bigInteger('defaut_lubrification_ml_proba')->nullable();
            $table->bigInteger('defaut_caviation_ml')->nullable();
            $table->bigInteger('defaut_caviation_ml_proba')->nullable();
            $table->boolean('balourd_indicator_consolidated')->nullable();
            $table->boolean('desalignement_indicator_consolidated')->nullable();
            $table->boolean('defaut_de_roulement_indicator_consolidated')->nullable();
            $table->boolean('defaut_electrique_indicator_consolidated')->nullable();
            $table->boolean('defaut_lubrification_consolidated')->nullable();
            $table->boolean('defaut_caviation_consolidated')->nullable();
            $table->double('desalignement_indicator_consolidated_proba')->nullable();
            $table->double('balourd_indicator_consolidated_proba')->nullable();
            $table->double('defaut_de_roulement_indicator_consolidated_proba')->nullable();
            $table->double('defaut_electrique_indicator_consolidated_proba')->nullable();
            $table->double('defaut_lubrification_consolidated_proba')->nullable();
            $table->double('defaut_caviation_consolidated_proba')->nullable();
            $table->text('desalignement_indicator_user')->nullable();
            $table->text('balourd_indicator_user')->nullable();
            $table->text('defaut_de_roulement_indicator_user')->nullable();
            $table->text('defaut_electrique_indicator_user')->nullable();
            $table->text('defaut_lubrification_user')->nullable();
            $table->text('defaut_caviation_user')->nullable();
            $table->double('severity_level')->nullable();
            $table->bigInteger('arret_subi')->nullable();
            $table->double('retour_utilisateur')->nullable();
            $table->bigInteger('status')->nullable();
            $table->double('acceleration_rms_without_peaks')->nullable();
            $table->double('low_energy_noise_ratio')->nullable();
            $table->double('deviation_by_win_of_acceleration_rms_without_peaks')->nullable();
            $table->double('slope_degree_by_win_of_acceleration_rms_without_peaks')->nullable();
            $table->bigInteger('temperature_abnormal')->nullable();
            $table->bigInteger('current_abnormal')->nullable();
            $table->text('n_of_days_until_shutdown')->nullable();
            $table->text('forecast_model_predictions')->nullable();
            $table->boolean('is_datacube_data')->nullable();
            $table->double('calculated_based_on_faulty_measure')->nullable();
            $table->double('calculated_based_on_low')->nullable();
            $table->bigInteger('id')->nullable();
            $table->dateTime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outputs');
    }
};
