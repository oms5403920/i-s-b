<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('asset_generic_extrafield', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('generic_asset_id')->index('asset_extrafield_generic_asset_id_foreign');
            $table->foreign(['generic_asset_id'])->references(['id'])->on('generic_assets')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->unsignedInteger('extrafield_id')->index('asset_extrafield_extrafield_id_foreign');
            $table->foreign(['extrafield_id'])->references(['id'])->on('extrafields')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->string('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('asset_generic_extrafield');
    }
};
