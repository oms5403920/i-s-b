<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recommendation_assets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fault');
            $table->string('cause')->nullable();
            $table->string('notes')->nullable();
            $table->date('started_at');
            $table->date('ended_at');
            $table->timestamps();
            $table->unsignedInteger('asset_id')->index('recommendation_assets_asset_id_foreign');
            $table->unsignedInteger('point_id')->index('recommendation_assets_point_id_foreign');
            $table->unsignedInteger('expert')->index('recommendation_assets_expert_foreign');
            //add foreign keys
            $table->foreign(['asset_id'])->references(['id'])->on('assets')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['expert'])->references(['id'])->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['point_id'])->references(['id'])->on('measurement_points')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->tinyInteger('status')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recommendation_assets');
    }
};
