<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cause_fault', function (Blueprint $table) {
            $table->increments('id');
            $table->softDeletes();
            $table->unsignedInteger('cause_id')->index('cause_fault_cause_id_foreign');
            $table->unsignedInteger('fault_id')->index('cause_fault_fault_id_foreign');
            // add foreign keys 
            $table->foreign(['cause_id'])->references(['id'])->on('causes')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['fault_id'])->references(['id'])->on('faults')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cause_fault');
    }
};
