<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_fault', function (Blueprint $table) {
            $table->increments('id');
            $table->softDeletes();
            $table->unsignedInteger('family_id')->index('family_fault_family_id_foreign');
            $table->unsignedInteger('fault_id')->index('family_fault_fault_id_foreign');
            //add foreign keys
            $table->foreign(['family_id'])->references(['id'])->on('families')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['fault_id'])->references(['id'])->on('faults')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_fault');
    }
};
