<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('predictions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('asset_id');
            $table->integer('measurement_id');
            $table->integer('timestamp');
            $table->string('model');
            $table->string('failure');
            $table->string('cause');
            $table->mediumText('failures_probability');
            $table->mediumText('causes_probability');
            $table->mediumText('predictions_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('predictions');
    }
};
