<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_group', function (Blueprint $table) {
            $table->increments('id');
            $table->double('warning');
            $table->double('alarm');
            $table->unsignedInteger('feature_id')->nullable()->index();
            $table->unsignedInteger('group_id')->nullable()->index();
            //add foreign keys
            $table->foreign(['feature_id'])->references(['id'])->on('features')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->foreign(['group_id'])->references(['id'])->on('groups')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->timestamps();
            $table->softDeletes();
            //index's
            $table->index(['group_id', 'feature_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feature_group');
    }
};
