<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extrafield_family', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('family_id')->index('extrafield_family_family_id_foreign');
            $table->unsignedInteger('extrafield_id')->index('extrafield_family_extrafield_id_foreign');
            //add foreign keys
            $table->foreign(['extrafield_id'])->references(['id'])->on('extrafields')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign(['family_id'])->references(['id'])->on('families')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extrafield_family');
    }
};
