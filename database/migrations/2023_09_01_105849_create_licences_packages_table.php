<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('licences_packages', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('package_id');
            $table->unsignedBigInteger('license_id');
            $table->bigInteger('value');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('package_id')->references('id')->on('packages')->onDelete('cascade');
            $table->foreign('license_id')->references('id')->on('licences')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('licences_packages');
    }
};
