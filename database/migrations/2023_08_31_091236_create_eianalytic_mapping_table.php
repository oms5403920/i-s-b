<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eianalytic_mapping', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('point_id')->nullable()->index('ind_point_id');
            $table->string('machine_code')->index('index4');
            $table->integer('point_index');
            $table->integer('axis');
            $table->integer('type')->default(0)->index('index7');
            $table->integer('idarea');
            $table->integer('idcompany');
            $table->integer('user_id')->nullable();
            $table->integer('database_type')->default(1);
            //add foreign keys
            $table->foreign(['point_id'], 'fk_point_id')->references(['id'])->on('measurement_points')->onUpdate('CASCADE')->onDelete('CASCADE');

            //index's
            $table->index(['point_id', 'point_index', 'machine_code', 'user_id', 'idcompany', 'idarea', 'type', 'axis'], 'index10');
            $table->index(['point_id'], 'index3');
            $table->index(['machine_code', 'point_index'], 'index5');
            $table->index(['machine_code', 'point_index', 'axis'], 'index6');
            $table->index(['user_id', 'idcompany', 'idarea'], 'index8');
            $table->index(['user_id', 'machine_code', 'idarea', 'idcompany'], 'index9');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eianalytic_mapping');
    }
};
