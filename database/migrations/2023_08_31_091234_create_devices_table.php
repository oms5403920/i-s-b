<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref')->index();
            $table->string('mac')->index();
            $table->date('date_etalonnage');
            $table->date('date_service');
            $table->string('duree_vie')->nullable();
            $table->integer('frequence_etalonnage');
            $table->timestamps();
            $table->unsignedInteger('product_id')->nullable()->index('devices_product_id_foreign');
            $table->unsignedInteger('gate_id')->nullable()->index('devices_gate_id_foreign');
            //add foreign keys
            $table->foreign(['gate_id'])->references(['id'])->on('gates')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['product_id'])->references(['id'])->on('products')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->softDeletes();
            $table->integer('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
};
