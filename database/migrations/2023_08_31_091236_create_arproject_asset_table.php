<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arproject_asset', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('device_id')->index('arproject_asset_device_id_foreign');
            $table->unsignedInteger('arproject_id')->index('arproject_asset_arproject_id_foreign');
            //add forign keys
            $table->foreign(['arproject_id'])->references(['id'])->on('arprojects')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['device_id'])->references(['id'])->on('assets')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arproject_asset');
    }
};
