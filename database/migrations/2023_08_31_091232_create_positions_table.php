<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('positions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number');
            $table->unsignedInteger('diagram_id')->index('positions_diagram_id_foreign');
            //add foreign keys
            $table->foreign(['diagram_id'])->references(['id'])->on('diagrams')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->double('x', 8, 8)->nullable();
            $table->double('y', 8, 8)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('positions');
    }
};
