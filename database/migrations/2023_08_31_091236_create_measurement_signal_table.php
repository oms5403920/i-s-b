<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurement_signal', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('value');
            $table->unsignedInteger('signal_id')->nullable()->index();
            $table->unsignedInteger('measure_id')->nullable()->index();
            //add foreign keys
            $table->foreign(['measure_id'])->references(['id'])->on('measurements')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->foreign(['signal_id'])->references(['id'])->on('signals')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->timestamps();
            $table->softDeletes();
            
            //index's
            $table->index(['signal_id', 'measure_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measurement_signal');
    }
};
