<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generic_points', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('generic_asset_id')->index('generic_points_generic_asset_id_foreign');
            $table->unsignedInteger('device_id')->nullable()->index('generic_points_device_id_foreign');
            //add foreign keys
            $table->foreign(['device_id'])->references(['id'])->on('devices')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['generic_asset_id'])->references(['id'])->on('generic_assets')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->timestamps();
            $table->string('type');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generic_points');
    }
};
