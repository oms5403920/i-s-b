<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_components', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference');
            $table->unsignedInteger('asset_id')->index('asset_component_asset_id_foreign');
            $table->unsignedInteger('component_id')->index('asset_component_component_id_foreign');
            //add foreign keys 
            $table->foreign(['asset_id'], 'asset_component_asset_id_foreign')->references(['id'])->on('assets')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['component_id'], 'asset_component_component_id_foreign')->references(['id'])->on('components')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_components');
    }
};
