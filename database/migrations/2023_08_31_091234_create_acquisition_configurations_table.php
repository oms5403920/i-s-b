<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acquisition_configurations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->double('sampling', 8, 2);
            $table->double('duration', 8, 2);
            $table->double('overlap', 8, 2);
            $table->double('min_acceleration', 8, 2);
            $table->double('max_acceleration', 8, 2);
            $table->double('min_velocity', 8, 2);
            $table->double('max_velocity', 8, 2);
            $table->double('resolution', 8, 8);
            $table->timestamps();
            $table->softDeletes();
            $table->double('resolution_acc', 8, 8)->nullable();
            $table->double('resolution_env', 8, 8)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acquisition_configurations');
    }
};
