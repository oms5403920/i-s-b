<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recommendations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fault');
            $table->string('cause');
            $table->string('notes')->nullable();
            $table->timestamps();
            $table->unsignedInteger('measure_id')->index('recommendations_measure_id_foreign');
            $table->unsignedInteger('expert')->index('recommendations_expert_foreign');
            // add foreign keys
            $table->foreign(['expert'])->references(['id'])->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['measure_id'])->references(['id'])->on('measurements')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->softDeletes();

            //index's
            $table->index(['id', 'deleted_at'], 'recommendations_id_deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recommendations');
    }
};
