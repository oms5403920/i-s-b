<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vibox_configurations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('frequency');
            $table->unsignedInteger('device_id')->index('vibox_configurations_device_id_foreign');
            $table->text('configuration');
            $table->timestamps();
            $table->dateTime('deployed_at')->nullable();
            $table->dateTime('last_communication')->nullable()->index();
            $table->text('version')->nullable();
            $table->boolean('deployed')->nullable()->index();
            //add foreign keys
            $table->foreign(['device_id'])->references(['id'])->on('devices')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vibox_configurations');
    }
};
