<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_grandeurs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('grandeur_id')->nullable()->index('product_grandeurs_grandeur_id_foreign');
            $table->unsignedInteger('product_id')->nullable()->index('product_grandeurs_product_id_foreign');
            //add foreign keys
            $table->foreign(['grandeur_id'])->references(['id'])->on('grandeurs')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->foreign(['product_id'])->references(['id'])->on('products')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_grandeurs');
    }
};
