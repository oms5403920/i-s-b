<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recommendations_faults_causes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('recommendation_id')->index('recommendations_faults_causes_recommendation_id_foreign');
            $table->unsignedInteger('fault_id')->index('recommendations_faults_causes_fault_id_foreign');
            $table->unsignedInteger('cause_id')->index('recommendations_faults_causes_cause_id_foreign');
            //add foreign keys
            $table->foreign(['cause_id'])->references(['id'])->on('causes')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['fault_id'])->references(['id'])->on('faults')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['recommendation_id'])->references(['id'])->on('recommendation_assets')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recommendations_faults_causes');
    }
};
