<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alarms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status');
            $table->string('grandeur');
            $table->dateTime('ended_at')->nullable()->index('alarms_ended_at');
            $table->timestamp('created_at')->nullable()->index('alarms_created_at');
            $table->timestamp('updated_at')->nullable();
            $table->unsignedInteger('asset_id')->nullable()->index('alarms_asset_id_foreign');
            $table->unsignedInteger('device_id')->nullable()->index('alarms_device_id_foreign');
            $table->unsignedInteger('measure_id')->nullable()->index('alarms_measure_id_foreign');
            $table->softDeletes();
            $table->integer('point_id')->nullable()->index('vbcncvbn');
            //add forign keys
            $table->foreign(['asset_id'])->references(['id'])->on('assets')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['device_id'])->references(['id'])->on('devices')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['measure_id'])->references(['id'])->on('measurements')->onUpdate('CASCADE')->onDelete('CASCADE');


            //add index's 
            $table->index(['created_at', 'ended_at'], 'alarms_created_at_ended_at');
            $table->index(['id', 'created_at', 'ended_at'], 'alarms_id_created_at_ended_at');
            $table->index(['id', 'grandeur', 'asset_id', 'point_id'], 'id');
            $table->index(['ended_at'], 'ended_at');
            $table->index(['grandeur', 'ended_at'], 'grandeur');
            $table->index(['status', 'grandeur', 'ended_at'], 'status');
            $table->index(['grandeur', 'asset_id'], 'grandeur_2');
            $table->index(['status', 'grandeur', 'ended_at', 'asset_id'], 'status_2');
            $table->index(['grandeur', 'asset_id', 'point_id'], 'grandeur_3');
            $table->index(['status', 'grandeur', 'asset_id'], 'status_3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alarms');
    }
};
