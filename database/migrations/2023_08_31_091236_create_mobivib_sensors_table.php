<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobivib_sensors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sensor_sn');
            $table->string('manufacturer')->nullable();
            $table->string('model')->nullable();
            $table->integer('channel_a')->nullable()->default(33000);
            $table->integer('channel_b')->nullable()->default(65000);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobivib_sensors');
    }
};
