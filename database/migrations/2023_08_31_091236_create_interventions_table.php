<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interventions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->date('date_intervention')->index();
            $table->string('status');
            $table->timestamps();
            $table->unsignedInteger('expert_id')->nullable()->index('interventions_expert_id_foreign');
            $table->unsignedInteger('validator_id')->nullable()->index('interventions_validator_id_foreign');
            $table->unsignedInteger('asset_id')->nullable()->index('interventions_asset_id_foreign');
            $table->unsignedInteger('operation_id')->nullable()->index('interventions_operation_id_foreign');

            //add foreign keys
            $table->foreign(['asset_id'])->references(['id'])->on('assets')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['expert_id'])->references(['id'])->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['operation_id'])->references(['id'])->on('operations')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['validator_id'])->references(['id'])->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interventions');
    }
};
