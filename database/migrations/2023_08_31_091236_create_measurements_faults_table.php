<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurements_faults', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('measure_id')->index('measurements_faults_measure_id_foreign');
            $table->unsignedInteger('fault_id')->index('measurements_faults_fault_id_foreign');
            //add foreign keys
            $table->foreign(['fault_id'])->references(['id'])->on('faults')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['measure_id'])->references(['id'])->on('measurements')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->integer('percentage');
            $table->integer('type')->default(1);
            $table->integer('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measurements_faults');
    }
};
