<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('company_params', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('param_id');
            $table->unsignedInteger('company_id');
            $table->string('value');
            $table->timestamps();
            $table->softDeletes(); 
            $table->foreign('param_id')->references('id')->on('params')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('company_params');
    }
};
