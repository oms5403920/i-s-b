<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_extrafield', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('asset_id')->index('asset_extrafield_asset_id_foreign');
            $table->unsignedInteger('extrafield_id')->index('asset_extrafield_extrafield_id_foreign');
            $table->string('value');
            //add foreign keys
            $table->foreign(['asset_id'])->references(['id'])->on('assets')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign(['extrafield_id'])->references(['id'])->on('extrafields')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_extrafield');
    }
};
