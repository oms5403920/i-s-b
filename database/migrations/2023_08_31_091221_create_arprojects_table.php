<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arprojects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type')->index();
            $table->unsignedInteger('asset_id')->index('arprojects_asset_id_foreign');
            // add forign keys 
            $table->foreign(['asset_id'])->references(['id'])->on('assets')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arprojects');
    }
};
