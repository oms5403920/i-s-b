<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('ref')->index();
            $table->double('latitude', 8, 2)->nullable();
            $table->double('longitude', 8, 2)->nullable();
            $table->smallInteger('type')->nullable()->index();
            $table->unsignedInteger('parent_id')->nullable()->index('entities_parent_id_foreign');
            $table->unsignedBigInteger('env_id')->nullable();
            //add foreign keys 
            $table->foreign(['parent_id'])->references(['id'])->on('entities')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('env_id')->references('id')->on('envirenements');
            $table->timestamps();
            $table->softDeletes();
            $table->text('shape')->nullable();
            $table->integer('status')->nullable()->index();
            $table->timestamp('last_measure_created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entities');
    }
};
