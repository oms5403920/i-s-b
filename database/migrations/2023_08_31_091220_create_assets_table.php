<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('ref')->index();
            $table->string('illustration')->nullable();
            $table->string('3d_model')->nullable();
            $table->unsignedInteger('diagram_id')->nullable()->index('assets_diagram_id_foreign');
            $table->double('mtbr', 8, 2)->nullable();
            $table->double('mttr', 8, 2)->nullable();
            $table->string('class')->nullable()->index();
            $table->smallInteger('type')->index();
            $table->string('brand')->nullable();
            $table->string('structure')->nullable();
            $table->string('SpeedPerimeter')->nullable();
            $table->string('powerRange')->nullable();
            $table->double('rpm', 8, 2)->nullable();
            $table->unsignedInteger('entity_id')->nullable()->index('assets_entity_id_foreign');
            $table->unsignedInteger('group_id')->nullable()->index('assets_group_id_foreign');
            $table->unsignedInteger('family_id')->nullable()->index('assets_family_id_foreign');
            $table->unsignedInteger('parent_id')->nullable()->index('assets_parent_id_foreign');
            $table->timestamps();
            $table->softDeletes()->index('deleted_at');
            $table->tinyInteger('status')->nullable();
            $table->integer('last_measure_id')->nullable()->index();
            //add foreign keys
            $table->foreign(['diagram_id'])->references(['id'])->on('diagrams')->onUpdate('NO ACTION')->onDelete('SET NULL');
            $table->foreign(['entity_id'])->references(['id'])->on('entities')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['family_id'])->references(['id'])->on('families')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['group_id'])->references(['id'])->on('groups')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['parent_id'])->references(['id'])->on('assets')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->timestamp('last_measure_created_at')->nullable();
            
            //index's
            $table->index(['class', 'type']);
            $table->index(['entity_id', 'deleted_at']);
            $table->index(['entity_id', 'parent_id', 'deleted_at']);
            $table->index(['ref', 'type']);
            $table->index(['id', 'deleted_at']);
            $table->index(['id', 'entity_id', 'deleted_at']);
            $table->index(['id', 'entity_id', 'parent_id', 'deleted_at']);
            $table->index(['id', 'parent_id', 'deleted_at'], 'assets_id_parent_id_deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
};
