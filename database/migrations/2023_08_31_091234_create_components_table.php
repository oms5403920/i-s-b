<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('components', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('bearing_id')->nullable()->index('components_bearing_id_foreign');
            $table->string('type')->index();
            $table->text('specs')->nullable();
            $table->string('label');
            $table->timestamps();
            $table->unsignedInteger('asset_id')->nullable()->index('components_asset_id_foreign');
            //add foreign keys 
            $table->foreign(['asset_id'])->references(['id'])->on('assets')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['bearing_id'])->references(['id'])->on('bearings')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('components');
    }
};
