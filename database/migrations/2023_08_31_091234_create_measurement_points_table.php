<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurement_points', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('type')->index();
            $table->string('external')->nullable();
            $table->timestamps();
            $table->unsignedInteger('asset_id')->nullable()->index('asset_id');
            $table->unsignedInteger('device_id')->nullable()->index('measurement_points_device_id_foreign');
            $table->unsignedInteger('component_id')->nullable()->index('measurement_points_component_id_foreign');
            $table->unsignedInteger('config_id')->nullable()->index('measurement_points_config_id_foreign');
            $table->softDeletes()->index('deleted_at');
            $table->unsignedInteger('position_id')->nullable()->index('measurement_points_position_id_foreign');
            $table->unsignedBigInteger('generic_asset_id')->nullable()->index('measurement_points_generic_asset_id_foreign');
            $table->integer('status')->nullable();
            $table->timestamp('last_measure_created_at')->nullable();
            $table->integer('last_measure_id')->nullable()->index();
            //add foreign keys 
            $table->foreign(['asset_id'])->references(['id'])->on('assets')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['component_id'])->references(['id'])->on('components')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['config_id'])->references(['id'])->on('acquisition_configurations')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['device_id'])->references(['id'])->on('devices')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['generic_asset_id'])->references(['id'])->on('generic_assets')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['position_id'])->references(['id'])->on('positions')->onUpdate('NO ACTION')->onDelete('SET NULL');

            //index's
            $table->index(['asset_id', 'deleted_at']);
            $table->index(['id', 'deleted_at']);
            $table->index(['id', 'device_id', 'type'], 'measurement_points_id_device_id_type');
            $table->index(['status', 'last_measure_created_at']);
            $table->index(['type', 'deleted_at'], 'measurement_points_type_deleted_at');
            $table->index(['type', 'device_id'], 'mps_type_device_id');
            $table->index(['type', 'device_id', 'deleted_at'], 'mps_type_device_id_deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measurement_points');
    }
};
