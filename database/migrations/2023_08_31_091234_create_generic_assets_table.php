<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generic_assets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
            $table->string('ref')->index();
            $table->unsignedInteger('entity_id')->index();
            $table->unsignedInteger('family_id')->index('generic_assets_family_id_foreign');
            $table->unsignedInteger('group_id')->index();
            $table->unsignedInteger('diagram_id')->nullable()->index('generic_assets_diagram_id_foreign');
            $table->string('photo')->nullable();
            $table->string('class')->nullable();
            $table->smallInteger('energy_type')->nullable()->index();
            $table->string('type')->nullable();
            $table->integer('status')->default(0)->index();
            $table->timestamps();
            $table->softDeletes()->index();
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
            $table->double('working_hours', 8, 2)->nullable();
            $table->integer('last_measure_id')->nullable()->index();
            //add foreign keys
            $table->foreign(['diagram_id'])->references(['id'])->on('diagrams')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['entity_id'])->references(['id'])->on('entities')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['family_id'])->references(['id'])->on('families')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['group_id'])->references(['id'])->on('groups')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->timestamp('last_measure_created_at')->nullable();

            //add index's
            $table->index(['entity_id', 'deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generic_assets');
    }
};
