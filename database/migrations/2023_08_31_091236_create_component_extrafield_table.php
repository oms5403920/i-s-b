<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('component_extrafield', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('component_id')->index('component_extrafield_component_id_foreign');
            $table->unsignedInteger('extrafield_id')->index('component_extrafield_extrafield_id_foreign');
            //add foreign keys
            $table->foreign(['component_id'])->references(['id'])->on('components')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign(['extrafield_id'])->references(['id'])->on('extrafields')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->string('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('component_extrafield');
    }
};
