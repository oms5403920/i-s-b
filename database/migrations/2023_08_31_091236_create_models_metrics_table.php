<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('models_metrics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('model_id');
            $table->integer('forecast_id')->nullable();
            $table->double('mse', 8, 2)->nullable();
            $table->double('mae', 8, 2)->nullable();
            $table->double('r2', 8, 2)->nullable();
            $table->double('accuracy', 8, 2)->nullable();
            $table->double('precision', 8, 2)->nullable();
            $table->double('recall', 8, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('models_metrics');
    }
};
