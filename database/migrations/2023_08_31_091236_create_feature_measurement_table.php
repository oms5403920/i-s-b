<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_measurement', function (Blueprint $table) {
            $table->increments('id');
            $table->double('value');
            $table->unsignedInteger('feature_id')->nullable()->index();
            $table->unsignedInteger('measure_id')->nullable()->index();
            $table->timestamps();
            $table->softDeletes();
            $table->double('delta', 8, 2)->nullable();
            //add foreign keys
            $table->foreign(['feature_id'])->references(['id'])->on('features')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->foreign(['measure_id'])->references(['id'])->on('measurements')->onUpdate('CASCADE')->onDelete('SET NULL');
            //index's
            $table->index(['feature_id', 'measure_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feature_measurement');
    }
};
