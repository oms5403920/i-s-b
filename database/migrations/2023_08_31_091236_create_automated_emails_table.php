<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('automated_emails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('label');
            $table->string('template');
            $table->text('to');
            $table->text('cc')->nullable();
            $table->text('hours')->nullable();
            $table->text('entities');
            $table->unsignedInteger('user_id')->nullable()->index('automated_emails_user_id_foreign');
            // add foreign key
            $table->foreign(['user_id'])->references(['id'])->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('automated_emails');
    }
};
