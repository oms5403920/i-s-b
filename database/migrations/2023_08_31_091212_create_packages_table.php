<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('max_users');
            $table->boolean('backup');
            $table->boolean('support');
            $table->integer('max_storage');
            $table->integer('max_devices');
            $table->boolean('mobile_notifications');
            $table->boolean('email_notifications');
            $table->boolean('reports');
            $table->boolean('dynamic_dashboard');
            $table->boolean('analytics');
            $table->boolean('augmented_reality');
            $table->boolean('analysis');
            $table->boolean('active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
};
