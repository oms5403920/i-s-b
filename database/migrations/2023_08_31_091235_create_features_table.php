<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('description')->nullable()->index();
            $table->string('unit')->nullable()->index();
            $table->unsignedInteger('grandeur_id')->nullable()->index();
            $table->timestamps();
            $table->softDeletes()->index();
            $table->string('tags')->nullable();
            //add foreign keys
            $table->foreign(['grandeur_id'])->references(['id'])->on('grandeurs')->onUpdate('CASCADE')->onDelete('SET NULL');

            //index's
            $table->index(['id', 'grandeur_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('features');
    }
};
