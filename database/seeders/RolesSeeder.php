<?php

namespace Database\Seeders;

use App\Core\Services\Auth\API\AuthService;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Config;


class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $defaultRoles = Config::get('app.default_roles');

        foreach ($defaultRoles as $roleName) {
            if (!Role::where('name', $roleName)->exists()) {
                Role::create(['name' => $roleName, 'guard_name' => AuthService::GUARD_NAME]);
            }
        }

    }
}
