<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EnvironnementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $evnirenements = ["monitoring", "energy"];
        foreach ($evnirenements as $env) {
            if (!DB::table('envirenements')->where('env_name', $env)->exists()) {
                DB::table('envirenements')->insert(
                    [
                        'env_name'   => $env,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ]
                );
            }
        }
    }
}
