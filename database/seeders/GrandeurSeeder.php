<?php

namespace Database\Seeders;

use App\Core\Models\Grandeur\Grandeur;
use Illuminate\Database\Seeder;

class GrandeurSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $parsedData = $this->readCSV(public_path('csvSeederFiles/grandeurs.csv'), ['delimiter' => ',']);
        $this->fill_table($parsedData);
    }

    protected function readCSV($csvFile, $array)
    {
        $file_handle = fopen($csvFile, 'r');
        $line_of_text = [];
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 0, $array['delimiter']);
        }
        fclose($file_handle);

        return $line_of_text;
    }

    protected function fill_table($parsedData)
    {
        $fp = fopen(public_path('csvSeederFiles/grandeurs.csv'), 'a');

        $iteration = 0;

        foreach ($parsedData as $key => $value) {
            if (is_array($value)) {
                echo 'seeding for: ' . $value[0] . "  $iteration\n";

                if (!Grandeur::where('name', trim($value[0]))->exists()) {
                    Grandeur::create(
                        [
                            'name'  => trim($value[0]),
                            'image' => trim($value[1]),
                        ]
                    );
                    $iteration++;
                }
            }
        }

        fclose($fp);
    }
}
