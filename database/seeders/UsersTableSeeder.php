<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Faker\Generator as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        DB::table('packages')->insert(
            [
                'name'                 => 'Elite',
                'max_users'            => 45,
                'max_storage'          => 2000,
                'max_devices'          => 2000,
                'mobile_notifications' => 1,
                'email_notifications'  => 1,
                'reports'              => 1,
                'dynamic_dashboard'    => 1,
                'analytics'            => 1,
                'augmented_reality'    => 1,
                'analysis'             => 1,
                'backup'               => 1,
                'support'              => 1,
                'active'               => 1,
            ]
        );

        DB::table('users')->insert(
            [
                'first_name'      => 'KARIM',
                'last_name'       => 'ELBAZ',
                'password'        => Hash::make('oms2019**'),
                'email'           => 'karim.elbaz@ocpgroup.ma',
                'phone'           => '06' . $faker->randomNumber(8),
                'address'         => $faker->address,
                'active'          => true,
                'type'            => 0,
                'company1'        => "OCP Maintenance Solutions",
                'country'         => 'Morocco',
                'package_id'      => 1,
                'last_connection' => '2021-02-05',
            ]
        );
        DB::table('users')->insert(
            [
                'first_name'      => 'ABDELHAQ',
                'last_name'       => 'AKRATE',
                'password'        => Hash::make('123456'),
                'email'           => 'abdelhaq.akrate@um6p.ma',
                'phone'           => '06' . $faker->randomNumber(8),
                'address'         => $faker->address,
                'active'          => true,
                'type'            => 0,
                'company1'        => "OCP Maintenance Solutions",
                'country'         => 'Morocco',
                'package_id'      => 1,
                'last_connection' => '2023-02-05',
            ]
        );
        DB::table('users')->insert(
            [
                'first_name'      => 'Elmehdi',
                'last_name'       => 'Larchaoui',
                'password'        => Hash::make('123456'),
                'email'           => 'Elmehdi.LARCHAOUIE@um6p.ma',
                'phone'           => '06' . $faker->randomNumber(8),
                'address'         => $faker->address,
                'active'          => true,
                'type'            => 0,
                'company1'        => "OCP Maintenance Solutions",
                'country'         => 'Morocco',
                'package_id'      => 1,
                'last_connection' => '2023-02-05',
            ]
        );
        DB::table('users')->insert(
            [
                'first_name'      => 'Othmane',
                'last_name'       => 'Rhazlani',
                'password'        => Hash::make('admin1234'),
                'email'           => 'othmane.rhazlani@um6p.ma',
                'phone'           => '06' . $faker->randomNumber(8),
                'address'         => $faker->address,
                'active'          => true,
                'type'            => 0,
                'company1'        => "OCP Maintenance Solutions",
                'country'         => 'Morocco',
                'package_id'      => 1,
                'last_connection' => '2023-02-05',
            ]
        );
        DB::table('users')->insert(
            [
                'first_name'      => 'Otmane',
                'last_name'       => 'Maacha',
                'password'        => Hash::make('admin1234'),
                'email'           => 'othmane.maacha@um6p.ma',
                'phone'           => '06' . $faker->randomNumber(8),
                'address'         => $faker->address,
                'active'          => true,
                'type'            => 0,
                'company1'        => "OCP Maintenance Solutions",
                'country'         => 'Morocco',
                'package_id'      => 1,
                'last_connection' => '2023-02-05',
            ]
        );
        /** Families inserts */
        DB::table('families')->insert(
            [
                'name' => 'AC MOTOR',
            ]
        );
        DB::table('families')->insert(
            [
                'name' => 'Agitator',
            ]
        );
        DB::table('families')->insert(
            [
                'name' => 'Alternator',
            ]
        );
        DB::table('families')->insert(
            [
                'name' => 'Belt Pulley',
            ]
        );
        DB::table('families')->insert(
            [
                'name' => 'Chain Drive',
            ]
        );
        DB::table('families')->insert(
            [
                'name' => 'Compressor',
            ]
        );
        DB::table('families')->insert(
            [
                'name' => 'Coupler',
            ]
        );
        DB::table('families')->insert(
            [
                'name' => 'Coupling',
            ]
        );
        DB::table('families')->insert(
            [
                'name' => 'Crusher',
            ]
        );
        DB::table('families')->insert(
            [
                'name' => 'DC Motor',
            ]
        );
        DB::table('families')->insert(
            [
                'name' => 'Drum Conveyor',
            ]
        );
        DB::table('families')->insert(
            [
                'name' => 'Fan',
            ]
        );
        DB::table('families')->insert(
            [
                'name' => 'Ferrule',
            ]
        );
        DB::table('families')->insert(
            [
                'name' => 'Gearbox',
            ]
        );
        DB::table('families')->insert(
            [
                'name' => 'Pump',
            ]
        );
        DB::table('families')->insert(
            [
                'name' => 'Turbine',
            ]
        );
        DB::table('families')->insert(
            [
                'name' => 'Other',
            ]
        );
        DB::table('suppliers')->insert(
            [
                'name' => 'OCP Maintenance Solutions',
            ]
        );
        DB::table('suppliers')->insert(
            [
                'name' => 'I-care',
            ]
        );
        DB::table('suppliers')->insert(
            [
                'name' => 'RisingHF',
            ]
        );
        DB::table('products')->insert(
            [
                'name'        => 'RHF1S001',
                'transmition' => 'Lora',
                'power'       => 'Batterie',
                'supplier_id' => 3,
            ]
        );
        DB::table('products')->insert(
            [
                'name'        => 'VIBOX',
                'transmition' => 'Hybrid',
                'power'       => 'Wired',
                'supplier_id' => 1,
            ]
        );
        DB::table('products')->insert(
            [
                'name'        => 'We-care',
                'transmition' => 'Wifi',
                'power'       => 'Batterie',
                'supplier_id' => 2,
            ]
        );
        DB::table('products')->insert(
            [
                'name'        => 'MOBIVIB',
                'transmition' => 'Wifi',
                'power'       => 'Batterie',
                'supplier_id' => 1,
            ]
        );
    }
}
