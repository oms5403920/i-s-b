<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Core\Models\Entity\Entity;
use App\Core\Models\GenericAsset\GenericAsset;
use App\Core\Models\Point\Point;
use App\Core\Models\User\User;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class canevasProvider1Seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //cette command alimente la base de données avec des informations du test à partir d\'un fichier EXCEL canevasP1
        $canevasP1 = public_path('canevasExcel/canevasP1.xlsx');

        // entities.
        $entities = Excel::toCollection(null, $canevasP1)->get(0);
        $skipFirstRow1 = true;
        foreach ($entities as $row) {
            if ($skipFirstRow1) {
                $skipFirstRow1 = false;
                continue;
            }
            $parentName = trim($row[2]);
            $parentEntity = null;

            if (!empty($parentName)) {
                $parentEntity = Entity::where('name', $parentName)->first();
            }

            Entity::create(
                [
                    'name'      => trim($row[0]),
                    'ref'       => trim($row[1]),
                    'parent_id' => $parentEntity?->id,
                    'env_id'    => trim($row[3]),
                ]
            );
        }


        // generic assets
        $gAssets = Excel::toCollection(null, $canevasP1)->get(1);
        $skipFirstRow2 = true;
        foreach ($gAssets as $row) {
            if ($skipFirstRow2) {
                $skipFirstRow2 = false;
                continue;
            }
            $entity = Entity::where('name', $row[1])->first();
            GenericAsset::create(
                [
                    'name'        => trim($row[0]),
                    'entity_id'   => $entity->id,
                    'family_id'   => intval($row[2]),
                    'group_id'    => intval($row[3]),
                    'energy_type' => intval($row[5]),
                    'class'       => trim($row[6]),
                    'ref'         => trim($row[7]),
                ]
            );
        }

        // measurements points
        $measurements_points = Excel::toCollection(null, $canevasP1)->get(2);
        $skipFirstRow3 = true;
        foreach ($measurements_points as $row) {
            if ($skipFirstRow3) {
                $skipFirstRow3 = false;
                continue;
            }
            $gAsset = GenericAsset::where('name', $row[2])->first();
            Point::create(
                [
                    'name'             => trim($row[0]),
                    'type'             => trim($row[1]),
                    'generic_asset_id' => $gAsset->id,
                ]
            );
        }

        // affectation des entities au utilisateurs
        $entities = Entity::all();
        $users = User::all();
        foreach ($entities as $entity) {
            foreach ($users as $user) {
                DB::table('entity_user')->insert(
                    [
                        'entity_id' => $entity->getId(),
                        'user_id'   => $user->getId(),
                    ]
                );
            }
        }
    }
}
