<?php

namespace Database\Seeders;

use App\Core\Models\Features\Features;
use Illuminate\Database\Seeder;

class FeaturesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $parsedData = $this->readCSV(public_path('csvSeederFiles/features.csv'), ['delimiter' => ',']);
        $this->fill_table($parsedData);
    }

    protected function readCSV($csvFile, $array)
    {
        $file_handle = fopen($csvFile, 'r');
        $line_of_text = [];
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 0, $array['delimiter']);
        }
        fclose($file_handle);

        return $line_of_text;
    }

    protected function fill_table($parsedData)
    {
        $fp = fopen(public_path('csvSeederFiles/features.csv'), 'a');

        $iteration = 0;

        foreach ($parsedData as $key => $value) {
            if (is_array($value)) {
                echo 'seeding for: ' . $value[0] . "  $iteration\n";

                if (!Features::where('name', trim($value[0]))->exists()) {
                    Features::create(
                        [
                            'name'        => trim($value[0]),
                            'description' => trim($value[1]),
                            'unit'        => trim($value[2]),
                            'grandeur_id' => trim($value[3]),
                            'tags'        => trim($value[4]),
                        ]
                    );
                    $iteration++;
                }
            }
        }

        fclose($fp);
    }
}
