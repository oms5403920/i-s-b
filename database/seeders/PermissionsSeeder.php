<?php

namespace Database\Seeders;

use App\Core\Models\User\User;
use App\Core\Services\Auth\API\AuthService;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Config;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $permissionsMap = Config::get('app.permissionsMap');
        foreach ($permissionsMap as $permissions) {
            foreach ($permissions as $permissionName) {
                if (!Permission::where('name', $permissionName[0])->exists()) {
                    Permission::create(['name' => $permissionName[0], 'guard_name' => AuthService::GUARD_NAME]);
                }
            }
        }

        $adminRole = Role::where('name', User::ADMIN_ROLE)->first();

        if ($adminRole) {
            $permissions = Permission::where('guard_name', AuthService::GUARD_NAME)->pluck('name')->all();
            $adminRole->syncPermissions($permissions);
        }

    }
}
